<?php
/**
 *
 * Drupal carbon module (calculations)
 *
 * Created on 18-Oct-2006
 *
 * @file
 * The calculations used to build carbon footprint.
 *
 * Different carbon_stamps have a different scope, some declare
 * emissions on one date, others declare emissions over a period
 * of time.
 *
 * Some sources of emissions e.g. from air travel need special
 * formulas for calculating resulting CO2 because fuel used in
 * aircraft takeoff has to be taken into consideration.
 */


/*
 * Describes the footprint results for a single account.
 *
 * used to pass results information between main functions
 * including the reporting functions and the CRAG settlement functions.
 */

class GroupResult
{
	var $account ;  // account
	var $group ;    // group name
	var $start ;
	var $end ;

	var $sector_kwh0 = array();
	var $sector_kwh1 = array() ;
	var $sector_co2 = array() ;
	var $grand_kwh0 = 0 ;
	var $grand_kwh1 = 0 ;
	var $grand_co2 = 0 ;
	var $C02_above_target = 0 ;
	var $emissions = array();
}

/**
 * Describes a single emission source e.g. a plane trip
 *
 */

class Emission
{
	var $start ; 		// stamp use for starting point
	var $end ; 			// stamp used for end point
	var $error ;
	var $notes ; 		// additional information to present to user

	var $select ;		  // array of stamps
	var $co2   ;		  // co2 emission
	var $kwh0  ; 	    // energy 0
	var $kwh1  ;	    // energy 1

	function Emission($select)
	{
		$this->select = $select;
	}
}


class CarbonStamp
{

}

/**
 * work out where to place the boundary between years
 * use months and days from any user preference
 * otherwise use Jan 1 in year of first stamp
 *
 * @param $dateRange  first and last enddate omn stamps
 * @param $firstDate  timestamp
 * @param $period     in months
 * @return unknown_type
 */
 function carbon_calculate_reporting_dates(&$dateRange, $firstDate, $period)
 {
  $earliestStampDate = $dateRange['first'];
  if (!empty($firstDate))
  {
    $y = date('Y', $firstDate);
    $m = date('m', $firstDate);
    $d = date('d', $firstDate);
  }
  else
  {
    $y = date('Y',$earliestStampDate);
    $m = 1 ;  // jan
    $d = 1 ;  // 1st
  }
  $earliestDate = mktime(0, 0, 0, $m, $d, $y);

  $dates = array();

  for ($i = 0 ; $firstdayinyear < $dateRange['last']; $i++)
  {
    if ($i > CARBON_MAX_CALCULATIONS)
    {
      drupal_set_message(t("Maximum number of batch of calculations (%calc) exceeded", array("%calc"=>CARBON_MAX_CALCULATIONS)), "error");
      break ;
    }
    $dates[] = $firstdayinyear = _add_months($earliestDate, $i * $period);
    $firstdayinyear_s = CarbonDate::asYYYYMMDD($firstdayinyear); // to help debug
  }

  return $dates;
}



/**
 * Create a set of results for a given period (usually a year)
 * based on on all the available carbon_stamps. Maintain
 * sector (e.g. home, tranport) and grand totals.
 *
 * @param $account passed to $results but only nid used
 * @param $stamps array of all carbon_stamps (with associated carbon_source fields)
 * @param $start first date the generated carbon footprint
 * @param $end last date of the generated carbon footprint
 *
 * @return a complex structure that has the carbon footprint for this period
 */


function carbon_calculate_total(&$account, &$stamps, $start, $end)
{
	$results = new GroupResult();
	$results->account = $account ;
	$results->start = $start ;
	$results->end = $end ;
	$results->emissions = _calculate($stamps, $start, $end);

	foreach ($results->emissions as $emission)
	{
		$sector = $emission->select[0]->sector ;
		if (isset($emission->kwh0))
		{
			//aacc
			if (!isset($results->sector_kwh0[$sector]))
			$results->sector_kwh0[$sector] = 0 ;

			$results->sector_kwh0[$sector] += $emission->kwh0 ;
			$results->grand_kwh0 += $emission->kwh0 ;
		}
		if (isset($emission->kwh1))
		{
			//aacc
			if (!isset($results->sector_kwh1[$sector]))
			$results->sector_kwh1[$sector] = 0 ;

			$results->sector_kwh1[$sector] += $emission->kwh1 ;
			$results->grand_kwh1 += $emission->kwh1 ;
		}
		if (isset($emission->co2))
		{
			if (!isset($results->sector_co2[$sector]))
			$results->sector_co2[$sector] = 0 ;

			$results->sector_co2[$sector] += $emission->co2 ;
			$results->grand_co2 += $emission->co2 ;
		}
		// need to execute this once.
		$results->enablekwh0 = $emission->select[0]->enablekwh0  &&  (0 < strlen(variable_get("carbon_energy_units_0", "")));
		$results->enablekwh1 = $emission->select[0]->enablekwh1  &&  (0 < strlen(variable_get("carbon_energy_units_1", "")));
		$results->enableco2 =  $emission->select[0]->enableco2 ;
	}
	return $results ;
}



/**
 * Aggregate all similar carbon stamps and build an array
 * of results of CO2 calculations.
 *
 * @param $ar array of all carbon_stamps (with associated carbon_source fields)
 * @param $start first date the generated carbon footprint
 * @param $end last date of the generated carbon footprint
 *
 * @return an array of emissions. Each result contains text and/or value.
 */


function _calculate(&$ar, $start, $stop)
{
	$dgroup = array();
	foreach ($ar as $node)
	{
		$dgroup[] = _concat_same_value($node);
	}

	// each key represents a group of readings
	// that can be considered identical.

	$keys = array_unique($dgroup);
	$emissions = array();
	foreach ($keys as $subgroup)
	{
		// pull out the first group and
		// select all the nodes that match that group

		$select = array();
		$column_notes = "" ;
		foreach ($ar as $node2)
		{
			if (_concat_same_value($node2) != $subgroup)
			continue ;
			$select[] = $node2;
			$node2->total = null ;
		}
		$fname = '_scope_'.$select[0]->scope;

		if (function_exists($fname))
		{
			$fname($emissions, $select, $start, $stop);
		}
		else
		{
			drupal_set_message("Invalid or missing function ". $fname, "error");
		}
	}
	return $emissions ;
}

/**
 * identify stamps that are identical so that they can be
 * group together.
 *
 * meter readings (scope == 1) are handled as a group, so stamps
 * with a different value should be group together.
 *
 * ja 2-jan-2009 use _concat() function
 */


function _concat_same_value($node)
{
	$hashcode = _concat($node);
	switch ($node->scope)
	{
		case 0 :
		case 2 :
			return $hashcode.'.'.$node->reading.'.'.$node->enddate.'.'.$node->startdate;
		case 1 : return $hashcode ;
	}
}

/**
 * identify stamps that are identical that can presented in a group on a form
 * and the only the field that needs to be changed is enddate and reading.
 *
 * ja 2-jan-2009 ignore adjustment and code fields. The side effect is that
 * the title alone of each meter reading determines which sequence of readings it
 * belongs to.
 */

function _concat($node){
	return $node->title.'.'.$node->scope.'.'.$node->sourceclass;
}


/**
 * todo check that no ; or : present
 */

function _evaluate($expression)
{
	$expression = trim($expression);
	$exp1 = substr($expression,0,1);
	if (strcmp($exp1,"=")==0)
	$expression = substr($expression,1);

	return eval('return '.$expression.';');
}



function _stamp_map_sorted_by_enddate($ar)
{
	$ar1 = array();
	foreach ($ar as $node)
	{
		$k = $node->enddate;
		while (isset($ar1[$k]))
		  $k++ ;
		$ar1[$node->enddate] = $node ;
	}
	ksort($ar1);
	return $ar1;
}


function _sort_by_valuedate($ar)
{
	$ar1 = array();
	foreach ($ar as $node)
	{
		$k = $node->valuedate;
		while (isset($ar1[$k]))
		$k++ ;
		$ar1[$node->valuedate] = $node ;
	}
	ksort($ar1);
	return array_values($ar1);
}




/**
 * There are two scenarios.
 * 1. the emissions occur on a single date (e.g. air travel)
 * 2. the emissions are spread over a month , year or more.
 *
 * @return false if no stamp falls outside date range
 */

function _scope_0(&$emissions, $select, $start, $stop)
{
	$emission = new Emission($select);
	_duplicate_check($emission->select);

	$stamp = $emission->select[0] ;
	if (empty($stamp->startdate) || $stamp->startdate == $stamp->enddate)
	{
		// this is the simple case where all the emissions
		// were released  on the same date
		// (take care with < and <= etc to include all stamps in
		// at least one accounting period (a year)
		//
    if ($stamp->enddate < $start) return false ;
    if ($stamp->enddate >= $stop) return false ;
		_invoke_calculate($emission, $stamp, 1);
	}
	else
	{
		// this is the case where the emissions spread across a long
		// period of time e.g. the carbon stamp might refer to emissions
		// from june 05 to june 06 and so would appear in two separate years.

		// explanation of below:
		// Let's say user inputs carbon stamp of 1000 Kg of CO2 over 10 years.
		// We want to allocate the 1000Kg over the ten years as evenly as possible.
		// So we calculate an adjustment which equals
		//    number of emitting days inside our period of interest (i.e between $start and $end) /
		//    total number of emitting days.

    if ($stamp->startdate <= $end) return false ;

		$begin = max($stamp->startdate, $start);
		$end = min($stamp->enddate, $stop);
		if ($end <= $begin) return false ;

		$adjust_for_period = ($end - $begin) / ($stamp->enddate - $stamp->startdate);
		_invoke_calculate($emission, $stamp, $adjust_for_period);

	}
	$emission->valuedate = $stamp->enddate ;

	$emissions[] = $emission ;
}

/**
 * meter readings
 *
 * Use linear interpolation to calculate meter readings for the
 * start and end times below then calculate emissions by creating
 * a scope==0 stamp and calculating emissions in the usual way.
 *
 * The resulting emission is added to the emissions array.
 *
 *
 * @param $emissions array of results of each calculation
 * @param $select  one of more scope==1 stamps to process
 * @param $start   start and
 * @param $end     end   time of the accounting period we are interested in
 * @return unknown_type
 */
function _scope_1(&$emissions, &$select, $start, $end)
{
  $meterStamps = _stamp_map_sorted_by_enddate($select);

  $usefulStamps = array();  // the stamps that we will use to calculate emissions

  // add the first real stamp or calculated estimate stamp
  // at the start of the period that we are interested in.

  $usefulStamps[] = $first = _estimate_meter_reading($meterStamps, $start) ;

  // add all the intermediate stamps
  foreach ($meterStamps as $stamp) // sorted by ascending date
  {
    if ($stamp->enddate <= $start)
      continue ;        // meter reading too early

    if ($stamp->enddate >= $end)
      continue ;        // meter reading too late

    $usefulStamps[] = $stamp ;
  }
  // add the last stamp of the period we are interested in
  $usefulStamps[] = _estimate_meter_reading($meterStamps, $end) ;

  // look for other stamps between $start and $end where the $sourcecode
  // or the sharing adjustment has been changed. If the adjustment
  // hasn't changed, remove them from the map. This doesn't make
  // any difference to the calculation but it is confusing to the
  // user to itemise emissions between each reading.

  $first = $second = null ;
  $usefulStampsMap = _stamp_map_sorted_by_enddate($usefulStamps);
  foreach ($usefulStampsMap as $stamp)
  {
    $date_s = _format_date_only($stamp->enddate);
    if (isset($first) && isset($second))
    {
      if (  ($stamp->adjustment == $second->adjustment)
         && ($stamp->sourcecode == $second->sourcecode))
      {
        unset($usefulStampsMap[$second->enddate]);   // remove the middle stamp
      }
    }
    $first = $second ; $second = $stamp ;
  }

  // gosh it's hard to delete array members.

  $usefulStamps = array() ;
  foreach($usefulStampsMap as $date => $stamp)
  {
    if (!empty($date))  $usefulStamps[] = $stamp ;
  }

  // there will only be two stamps here, unless sourcecode or
  // adjustment changes have been uncovered.

  foreach ($usefulStamps as $current)
  {
    if ($current == null)
      continue ;        // no estimate available

    if (empty($previous))   // skip first meter reading
    {
      $previous = $current ; continue ;
    }

//    $emissions = _create_emissions_record($previousStamp, $stamp);

    $stamp = clone($current);  /// use adjust and sourcecode from last reading
    $stamp->scope = 0 ;
    $stamp->reading = $current->reading - $previous->reading ;

    $e0 = new Emission($usefulStamps);
    $e0->start = $previous ;  $e0->end = $current ;
    $e0->notes = "" ;

    _invoke_calculate($e0, $stamp, 1) ;

    if ($e0->start->estimate)
    {
      $e0->notes .= $e0->start->notes ;
    }

    if ($e0->end->estimate)
    {
      $e0->notes .= $e0->end->notes ;
    }

    if (isset($e0->end->adjustment))
      if (_evaluate($e0->end->adjustment) != 1.0)
    {
      $e0->notes .= " Adjusted by ". $e0->end->adjustment. "." ;
    }

    $e0->valuedate = $e0->current->enddate;

    $emissions[] = $e0;
    $previous = $current ;
  }
}

/**
 * estimate a meter reading for a given date and return the value
 * in a new temporary carbon stamp
 *
 * @param $meter_readings  an array of carbon_stamps
 * @param $date the date of the desired new fillin stamp
 * @return calculated timestamp
 */

function _estimate_meter_reading($meter_readings, $date)
{
  $date_s = _format_date_only($date);
  $i = 0 ;
  foreach ($meter_readings as $mr) // assume meter readings are sorted by date
  {
    if ($mr->enddate == $date)
      return $mr ;   // got an actual reading for that date

    if ($mr->enddate > $date)
      break ;

    $i++;         // push pointer above all the earlier dates.
  }
  $i-- ;
  // now, $i is the index of the meter reading just below
  // where we want to calculate our estimate (and if there isn't
  // one -1.

  // special case, we want an estimate before the first meter reading
  // shift up and use the first two meter readings


  if ($i <= 0)
    $i++ ;

  // special case, we want to estimate after the last meter reading
  // shift down and use the last two meter readings

  if ($i >= count($meter_readings) - 1)
    $i-- ;

  // now pick out the meter readings we are going to use
  $meter_readings_array = array_values($meter_readings);
  $below = $above = null ;
  if ($i >= 0  && $i <= count($meter_readings) - 2)
  {
    $below = $meter_readings_array[$i];
    $above = $meter_readings_array[$i+1];
  }
  else
    return null ;

  $stamp = clone($above); // assume source code and adjustment of later stamp
  $stamp->estimate = true ;
  $stamp->enddate = $date ;

  // use linear interpolation to estimate value between the dates that we have

  $adjust = ($date - $below->enddate) / ($above->enddate - $below->enddate);

  // If the estimate required falls between 2 meter readings then  0.0 < $adjust < 1.0.

  // We try to provide estimates before the first or after the last meter reading,
  // however this is a big assumption as the person may have moved.

  // the margin of 1/2 below is equivalent to creating estimates up to month before
  // the first meter reading or 1 month after the last meter reading, if the
  // meter readings are 2 months apart.

  $margin = 1/2 ;

  if (   $adjust < -$margin     // we are estimating before the first meter reading
      || $adjust > 1+ $margin)    // we are estimating after the last meter reading
  {
    return null ;
  }


  $stamp->reading = $below->reading + $adjust * ($above->reading - $below->reading) ;

  $stamp->notes = "Using estimated reading of "
    . _format_float($stamp->reading) . " on " . _format_date_only($stamp->enddate)
    . " interpolated from "
    . _format_float($below->reading) . " read on " . _format_date_only($below->enddate)
    . " and "
    . _format_float($above->reading) . " read on " . _format_date_only($above->enddate)
    . ". ";

  return $stamp ;
}


/**
 * daily event
 * The difference betweeen a scope_0 and scope_2 event is that
 * the scope_2 event repeats the same level of emissions every
 * year and the emissions are not dispersed between start and
 * date.
 */


function _scope_2(&$emissions, &$select, $start, $stop)
{
  $emission = new Emission($select);

  _duplicate_check($emission->select);

  $stamp = $emission->select[0] ;

  if (empty($stamp->startdate))
  {
    if ($stamp->enddate > $start && $stamp->enddate <= $stop)
      $begin = $start ;
    else
      return false;
  }
  else
  {
    // find out the range of dates when this event occurred
    // and that fall inside the reporting period that we are
    // interested in.

    $begin = max($stamp->startdate, $start);
  }
  $end = min($stamp->enddate, $stop);

  if ($end <= $begin)
    return false ;
  // assuming annual repetition
  $adjust_for_repetition = ($end - $begin) / (60*60*24*365);
  _invoke_calculate($emission, $stamp, $adjust_for_repetition);
  $emission->valuedate = $end;

  $emissions[] = $emission;   // process only 1 stamp, ignore duplicates
}

/**
 * We had expected to be passed an array of just 1 carbon_stamp.
 * Log an error if there is more than one.
 * @param $ar an array of carbon stamps
 */


function _duplicate_check($ar)
{
  if (count($ar) > 1)
  {
    $text = "These identical carbon stamps treated as one:";
    foreach ($ar as $stamp)
    {
      $text .= " ".l("node ".$stamp->nid,"/node/".$stamp->nid);
    }
    drupal_set_message($text, "warning");
  }
}

/**
 *
 * At the moment the classname is simply mapped to a function name.
 *
 * @param $emission structure into which results are place
 * @param $node a carbon stamp
 */


function _invoke_calculate(&$emission, $node, $adjust)
{
  if (!isset($node->sourcecode))
  {
    $emission->sourceError = t("Unknown source code: %code", array('%code'=>$node->code)) ;
    return ;
  }
  if ($node->sourcestatus == 0)
  {
    $emission->sourceError = t("Source not or no longer published: %code", array('%code'=>$node->code)) ;
    return ;
  }

  if (empty($node->sourceclass))
  {
    $emission->sourceError = t("Source class not defined: %class", array('%class'=>$node->sourceclass)) ;
    return ;
  }

  $fn = '_'.$node->sourceclass ;
  if (!function_exists($fn))
  {
    $emission->sourceError = t("Calculator function not defined: ").$fn ;
    return ;
  }
  $fn($emission, $node, $adjust);
}

/**
 * The simple class calculates emissions with the formula
 *
 * emissions = r*m*a
 *
 * where r is the value spcified by the user
 *       m is the multiplier used to convert units of r
 *       a is the adjustment specified by the user
 *       which might be 48/52 to allow for holidays
 *       from a daily commute.
 *       (this can be a string and must be evaluated)
 *
 * to the corresponding CO2 value.
 *
 * @param $node a carbon stamp
 */

function _simple(&$emission, $node, $adjust)
{
  $emission->units = $node->reading * _evaluate($node->adjustment) * $adjust;
  $emission->co2 =  $emission->units * $node->toco2 ;
  $emission->kwh0 = $emission->units * $node->tokwh0 ;
  $emission->kwh1 = $emission->units * $node->tokwh1 ;
}

/**
 *  This formula comes from http://www.chooseclimate.org
 *

 Fuel per passenger (Data for B-747).

 Fuel = [7840 + 10.1 * (distance-250)] (*2 if return)
 (7840 kg take off-climb-descent, 10.1 kg/km cruising.
 Passengers, = 370 * [occupancy] (/1.5 if business)
 Greenhouse warming:
 CO2 = fuel * (44/12 * 156/184) (molecular masses)

 Total warming effect of CO2,Ozone (made by NOx), water vapour and
 contrails is about three times greater than effect of CO2 alone
 */


/**
 * $multiplier should only be used to convert distance from some
 * arbitrary unit to Km.
 **/

function _chooseclimate(&$emission, $node, $adjust)
{
  $model = array(
    "aircraft" => "B747-400",
    "fuel_for_takeoff" => 7840,
    "fuel_cruising_per_km" => 10.1,
    "climb_distance" => 250,
    "capacity" => 370,
    "occupancy" => .80,
    "fuel_conversion" => (44/12 * 156/184),
    "radiative_forcing_index" => 2.7
  );
  return _flight_calculate($emission, $node, $adjust, $model);
}

/**
 Climate care model

 This model is based on the information in a report written
 by the Environmnetal Change Institute for climate care
 and is provided on the climate care website.
 http://www.climatecare.org/_media/documents/pdf/Aviation_Emissions_&_Offsets.pdf

 the raw data refrred to on aircraft fuel consumption
 http://reports.eea.europa.eu/EMEPCORINAIR3/en/B851vs2.4.pdf

 my version of this with average fuel consumption added:
 http://cvs.drupal.org/viewcvs/ checkout /drupal/contributions/modules/carbon/air_fuel_use.html

 note the climate care assume 100% occupancy and that on long
 haul flights the 10% of fuel can be be attributed to freight.
 */

function _climatecare(&$emission, $node, $adjust)
{
  $distance_km = $node->reading * $node->toco2 ;

  // switch planes depending on distance

  $model =  $distance_km < 3500 ? _climatecareB737() : _climatecareB747();

  return _flight_calculate($emission, $node, $adjust, $model);
}

function _climatecareB747() {
  return array(
    "aircraft" => "B747-400",
    "fuel_for_takeoff" => 3402,
    "fuel_cruising_per_km" => 10.1,    // from average kg/mile in EU table
    "climb_distance" => 0,
    "fuel_conversion" => 3.15,         // http://www.climatecare.org/_media/documents/pdf/Aviation_Emissions_&_Offsets.pdf
    "capacity" => 400,                 // ditto
    "occupancy" => 1.00,               // ditto
    "radiative_forcing_index" => 2.0   // ditto & http://iacweb.ethz.ch/tradeoff/
  );
}

function _climatecareB737() {
  return array(
    "aircraft" => "B737-400",
    "fuel_for_takeoff" => 825.4,
    "fuel_cruising_per_km" => 3.01,    // from average kg/mile above
    "climb_distance" => 0,
    "fuel_conversion" => 3.15,         // http://www.climatecare.org/_media/documents/pdf/Aviation_Emissions_&_Offsets.pdf
    "capacity" => 180,                 // ditto
    "occupancy" => 1.00,               // ditto
    "radiative_forcing_index" => 2.0   // diito & http://iacweb.ethz.ch/tradeoff/
  );
}

function _climatecareA340() {
  return array(
    "aircraft" => "Airbus-A340",
    "fuel_for_takeoff" => 2019,
    "fuel_cruising_per_km" => 6.6,    // from average kg/mile in EU table
    "climb_distance" => 0,
    "fuel_conversion" => 3.15,         // http://www.climatecare.org/_media/documents/pdf/Aviation_Emissions_&_Offsets.pdf
    "capacity" => 295,                 // ditto
    "occupancy" => 1.00,               // ditto
    "radiative_forcing_index" => 2.0   // diito & http://iacweb.ethz.ch/tradeoff/
  );
}


/**
 * the choose climate model is basically a simple
 * first order polynomial and I have used it for
 * the climate care model as well.
 */


function _flight_calculate(&$emission, $node, $adjust, $model)
{
  $distance = $node->reading * $node->toco2 ;

  $flight_takeoff = $model["fuel_for_takeoff"] ;

  if ($distance > $model["climb_distance"])
  $flight_cruising += ($model["fuel_cruising_per_km"] * ($distance-$model["climb_distance"]));  // Kg

  $fuel = ($flight_takeoff + $flight_cruising)/($model["capacity"] * $model["occupancy"]) ;   //.Kg

  $co2 = $fuel * $model["fuel_conversion"];  // convert fuel to CO2

  // the radiative_forcing_index below (expected to be between
  // 1.8 and around 2.7) includes effect of the non CO2 gasses

  $co2_and_other_gasses = $co2 * $model["radiative_forcing_index"] ;

  $emission->notes =  sprintf("Assumptions: aircraft is a %s" .
            " which uses %s Kg of fuel on takeoff/landing, ".
            " and %s Kg/Km while cruising; ".
            " %s%% of %s seats occupied," .
            " radiative forcing index is %s." .
            " Calculation: CO2 per passenger is %2d Kg," .
            " all gasses (expressed as CO2 equivalent) is %2d Kg.",
  $model["aircraft"], $model["fuel_for_takeoff"],  $model["fuel_cruising_per_km"],
  100 * $model["occupancy"],$model["capacity"], $model["radiative_forcing_index"],
  $co2, $co2_and_other_gasses);

  $emission->units = $node->reading ;   // this was previously set !
  $emission->co2 = $co2_and_other_gasses * _evaluate($node->adjustment) * $adjust ; // Kg
}


/**
 * Create a printable carbon footprint for a given period (usually a year)
 * based on on all the available carbon_stamps.
 *
 * @param $results a structure containing all results of calculations
 * @param $json set to true if want table as raw data
 * @return a string containing a table that shows the carbon footprint.
 */


function carbon_calculate_present_results(&$group_result)
{
  $rows = array();
  $grand_total = 0 ;
  $numupdates = 0 ;
  $notes = array();
  $show_model = false ;


  // now go through the results and create a table
  // containing the results and then add the notes
  // to the end of the table.

  foreach ($group_result->emissions as $emission)
  {
    $result_as_text = null ;
    $column_notes = "" ;
    $showRow = false ;

    if (!empty($emission->notes))
    {
      $notes[] = $emission->notes;
      $column_notes =  " [".count($notes)."]" ;
    }

    if (isset($emission->sourceError) || isset($emission->error) || isset($emission->co2))
    {
      $showRow = true ;
    }
    if ($showRow)
    {
      $select = $emission->select ;
      $daterange = "" ;
      if ($select[0]->scope == 1)
      {
        if ($emission->start->enddate != $emission->end->enddate)
          $daterange = _format_date_only($emission->start->enddate)." and " ;
        $daterange .= _format_date_only($emission->end->enddate);
      }
      else
      {
        if (!empty($select[0]->startdate) && ($select[0]->startdate != $select[0]->enddate))
          $daterange = _format_date($select[0]->startdate)." to ";
        $daterange .= _format_date($select[0]->enddate);
        $emission->end = $select[0] ;
      }
      if (isset($emission->sourceError))
      {
        $units_col = $emission->sourceError ;
      }
      else   // ja 2-1-2008 replace select[0] with emission->end
      {
        if (isset($emission->units))
          $units = is_numeric($emission->units) ? _format_float($emission->units) : $emission->units ;
        else
          $units = $emission->end->amount ; // is this line used?

        $model = $show_model ? $emission->end->sourcemodel."/" : "" ;
        $units_col =  isset($units) ? $units." ".$emission->end->units." of " : "" ;

        $units_col .= l($model.$emission->end->sourcetitle, 'node/'.$emission->end->sourceid);
      }
      $row = array(
        $daterange,
        carbon_link_from_title($emission->end,$group_result->account,"")." ".$column_notes,
        $units_col);
      if ($group_result->enablekwh0)
        $row[] = _format_float($emission->kwh0);
      if ($group_result->enablekwh1)
        $row[] = _format_float($emission->kwh1);
      if ($group_result->enableco2)
        $row[] = empty($emission->error)  ? _format_float($emission->co2) : $emission->error;
        // really need to redirect to original page rather than hard coded page
      $row[] = carbon_access("update", $group_result->account) ?
         carbon_format_edit_ops($emission->end, "destination=node/".$group_result->account->nid) : ""  ;
        $rows[] = $row ;
      $numupdates++ ;
    }
  }
  if (!empty($numupdates))
  {
    $out = "";
    $row = array("total", null, null);

    if ($group_result->enablekwh0)
    $row[] = _format_float($group_result->grand_kwh0);

    if ($group_result->enablekwh1)
    $row[] = _format_float($group_result->grand_kwh1);

    if ($group_result->enableco2)
    $row[] = _format_float($group_result->grand_co2);

    // pass the footprint and see whether we are allowed to update
    // it rather than asking if we can crate a new stamp as the former
    // check makes sure we own the existing footprint.
    $row[] = "" ; // carbon_format_add_new_op($group_result->account, "destination=node/".$group_result->account->nid);
    $rows[] = $row ;

    $header = array(t("date"),t("title"),t("source"));
    if ($group_result->enablekwh0)
    $header[] = variable_get("carbon_energy_units_0", "");
    if ($group_result->enablekwh1)
    $header[] = variable_get("carbon_energy_units_1", "");
    if ($group_result->enableco2)
    $header[] = variable_get("carbon_units", "Kg of C02") ;
    $header[] = t("Operations");

    $out .= theme('table', $header, $rows);

    $i = 1 ; foreach ($notes as $note)
    {
      $out .= "<p class='link'>[".$i++."] ".$note."</p>" ;
    }
  }
  return $out ;
}


function carbon_calculate_empty_table($footprint)
{
  $row = array(t("There are no carbon stamps attached to this carbon account. <br/>" .
               "Select <i>Meters</i> or <i>Travel</i> tabs and add new carbon stamps."), null);
  $out .= "<h1></h1>".theme('table', array("","",""), array($row));
  return $out ;
}

/**
 * this routine borrowed from php.net
 * @param $amount value to print
 * @return unknown_type
 */

function _format_float($amount)
{
    $precision = 2 ;        // number of significant digits
    $logval = log10(abs($amount));
    $decimals = min(0, max(0, $precision - 1 - intval($logval)));
    $format = "%." . $decimals . "f";
    return sprintf($format, $amount);
}


?>
