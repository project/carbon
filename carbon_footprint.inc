<?php
/*
 *  
 * Created on 26-Feb-2007
 * 
 * @file 
 * quick client side calculator that provides a spreadsheet. 
 * 
 * THIS IS STILL IN DEVELOPMENT. IT IS NOT FINISHED.  
 */


/*

function mymodule_edit_record($record) {
  $output = t('This is my edit page!');
  $output .= drupal_get_form('mymodule_edit_record_form', $record);
  return $output;
}

function mymodule_edit_record_form($record) {
  $form['my_field'] = array(
    '#type' => 'textfield',
    '#title' => 'Record name',
    '#default_value' => $record->name,
  );
  return $form;
}


*/
function carbon_footprint()
{
  $fields = array(
   
    array("Gas consumption in hours", "therms",  3.4, 5.6, 7.8),
    array("electricity in hours", "Kwh",  3.4, 5.6, 7.8)
  ); 
    
  $path = drupal_get_path("module", "carbon");
  drupal_add_js($path . "/carbon.js", "module");
  drupal_add_css($path . "/carbon.css");

  $output = drupal_get_form("carbon_footprint_form", $fields);
  
  return $output ;          
}
 
 
 
 
function carbon_footprint_form($fieldblock)
{      
  $form = array(); 
  
  $r = 0 ; 
  foreach ($fieldblock as $fields)
  {

      $subform = array(    
        "#name" => "abcd", 
        "#type" => "fieldset", 
        "#title" => $fields[0],
        "#weight" => -1, 
        "#collapsible" => false,
        "#collapsed" => false
       );   

       $subform["choice"] = array(    
        "#type" => "radios", 
        "#options" => array("one","meter"), 
        "#default_value" => 1,   
 //       "#description" => t("Select type of carbon debit"),                
        "#attributes" => array("onchange" => "scope_change(this.value);")
       );       
             
       $subform["help"] = array(
      
        "#type" => "item",
        "#value" => $fields[0], 
        "#size" => 14,
        "#maxlength" => 20,         
        "#value" => "<div class='scope-1' style='white-space: normal'>" .
          t("Note that meter readings are subtracted from one another to calculate " .
          "consumption. You need to have at least two meter readings before this source of " .
          "emissions will be included in your carbon footprint.")."</div>",  
       );
       
       $subform["amount"] = array(
       
        "#value" => 0,      
        "#type" => "textfield",     
        "#size" => 8,
//        "#description" => "<div class='scope-0'>Enter the one off value</div>". 
  //                        "<div class='scope-1'>Enter the meter reading</div>", 
        "#maxlength" => 20,  
//        "#suffix" => "KWh of electricity"
       );

       $subform["units"] = array(
       
        "#value" => 0,      
        "#type" => "select",     
        "#options" => array("Kwh of Electricity", "Therms of gas"),   
        "#size" => 8,
//        "#description" => "<div class='scope-0'>Enter the one off value</div>". 
  //                        "<div class='scope-1'>Enter the meter reading</div>", 
        "#maxlength" => 20,  
//        "#suffix" => "KWh of electricity"
       );
       
       $subform["adjust"] = array(
//        "#prefix" => "adjustment",     
        "#value" => 0,      
        "#type" => "textfield",     
        "#size" => 8,
//        "#description" => "<div class='scope-0'>Enter the one off value</div>". 
  //                        "<div class='scope-1'>Enter the meter reading</div>", 
        "#maxlength" => 20  
       );

       $subform["co2"] = array(
      
        "#type" => "textfield",     
        "#size" => 14,
        "#readonly" => true,
        "#maxlength" => 20 
       );
       $subform["toCo2"] = array(
      
        "#type" => "hidden",     
        "#value" => 1444
       );
       $form["my".$r++] = $subform ;      
  }
  return $form ;   
} 
  


function theme_carbon_footprint_form($form)
{
  $header = array(t("amount"),t("select units"),t("apply adjustment"), t("total CO2"));
  $rows = array();

  foreach ($form as $rowname => $row)
  {
//    if (empty($row)) continue ; 
  //  if (!is_array($row))
    //  continue ;
    if (substr($rowname,0,1) == "#")
      continue ; 
    $colsTop = array();  $colsBottom = array(); 
    foreach ($row as $colname => $col)  
    {       
      if (substr($colname,0,1) == "#")
        continue ; 
 
      if ($col["#type"] == "hidden")
        continue ; 

      if ($colname == "help")
      {
        
         $colsTop[] = (array('data' => drupal_render($col), 'colspan' => count($header)-1));                   
      }
      else 
      if ($colname == "choice")
      {
         $colsTop[] = drupal_render($col); 
      }
      else
      {
        $text = drupal_render($col);
        if (strspn($text, "no-array ") != 0)
        {
          $colsBottom[] = "yuk ".$rowname." ".$colname ; 
        }
        else
          $colsBottom[] = /* $rowname." ".$colname." ".$col["type"]." ". */ $text ;
        
//      unset($col);
      }         
    }
    $rows[] = $colsTop ; 
    if (count($colsBottom))
      $rows[] = $header ;
    $rows[] = array("data" => $colsBottom, "valign"=>"top") ;  
    unset($form[$rowname]);


  }
  $output = theme("table", null, $rows);

  $output .= drupal_render($form);

  return $output; 
}




function form_render($col)
{
  if (is_array($col)) return $col[0];
  return $col ;   
  
}



/**
 * Add clickable link to words.
 */
function click_info_make($text, $words) {
  foreach ($words as $word => $message) {
    $text = str_replace ($word, '<span class="clickInfo" title="' . $message . '">' . $word . '</span>', $text);
  }
  return $text;
}


?>