 *  
 * These js functions dump objects below #main 
 * 
 */

Drupal.behaviors.carbon_debug = function () {
{  
  fieldset = $('#footer').append(
		 "<fieldset class='collapsible collapsed' id='debugPanel'>" +
		 "<legend>ajax log</legend>" +
		 "<div id='debugLog'></div>" +
		 "</fieldset>");
  addJQueryForm();
  Drupal.behaviors.collapse(fieldset);
  $('div#nojavascript').remove();
  log("carbon_debug initialised and logging");
}};  



/** 
 * Put up a box into which jQuery requests can be processed
 * @param none
 * @return null
 */

function addJQueryForm()
{   
  formHTML = 
	'<form id="jQueryEval">' +
    '<input type="submit" maxlength="30" name="jquery" value="evaluate jQuery" />' + 
    '<input type="text" maxlength="30" name="query" class="queryText" description="try #jQueryEval"/>' +
    '</form>';

  $('#debugPanel').append("<p>" + formHTML + "</p>").submit( function() { return handleJQueryRequest() } ); 
}


function handleJQueryRequest()
{
  theQuery = $("form#jQueryEval > input.queryText").attr("value");
  res = $(theQuery);
//  for (var item in res)
  logJQueryObject(theQuery, res);
  return false ; 	
}



/**
 * used to report Ajax transfer error
 * @param msg
 * @return
 */

function handleError(msg)
{
  alert( "js Error, data got: " + msg);
}


// logging functions. There should be no naming conflicts as 
// all the functions are controlled by the carbon noudule (apart 
// from drupal here). 

function log(text)
{
  var div = document.getElementById("debugLog");
//  text.replace(/\n/, "<BR/>")
  if (div == null) 
    alert(Date().substr(16,8) + " #debugLog has been wiped out! " + text.substr(0,80) ); 	
  else 
    div.innerHTML += "<br/>" + Date().substr(16,8) + " " + text ; 	
}


function logObject(obj)
{
  logObject2("", obj);
}

function logObject2(prefix, obj)
{
  log(dumpObj(prefix, obj,"",0));
}


var MAX_DUMP_DEPTH = 2;

function dumpObj(name, obj, indent, depth) 
{
  var output = "" ; 

  if (depth > MAX_DUMP_DEPTH) {
    return output ; 
  }
  if (typeof obj == "object") {
    var child = null;

    indent += "&nbsp;&nbsp;&nbsp;&nbsp;";
    for (var item in obj)
    {
      try {
        child = obj[item];
        output += dumpObj(name + "." + item, child, indent, depth + 1);
      } catch (e) {
        output += "<Unable to Evaluate" + item + ">";
      }
    }
    return output;
  } 
  else 
  {
    if (typeof obj == "string")
    {
      output += indent + name + ":s: " + obj + "<br/>";
    } 
    if (typeof obj == "boolean")
    {
      output += indent + name + ":b: " + obj + "<br/>";
    }
    if (typeof obj == "number")
    {
      output += indent + name + ":n: " + obj + "<br/>";
    }
  }
  return output ;
}


function logJQueryObject(prefix, a)
{
  if (a == undefined) 
  { 
	log("logJQueryObject: too few arguments");  return ;
  }
  logObject2(prefix, a);
}


// js debug functions

//pre-submit callback 
function showRequest(formData, jqForm, options) { 
  // formData is an array; here we use $.param to convert it to a string to display it 
  // but the form plugin does this for you automatically when it submits the data 
  var queryString = $.param(formData); 

  // jqForm is a jQuery object encapsulating the form element.  To access the 
  // DOM element for the form do this: 
  // var formElement = jqForm[0]; 
  log('About to submit: ' + queryString); 
  log('About to URL: ' + options.url);  
  // here we could return false to prevent the form from being submitted; 
  // returning anything other than false will allow the form submit to continue 
  return true; 
} 


//post-submit callback 
function showResponse(responseText, statusText)  { 
  // for normal html responses, the first argument to the success callback 
  // is the XMLHttpRequest object's responseText property 

  // if the ajaxForm method was passed an Options Object with the dataType 
  // property set to 'xml' then the first argument to the success callback 
  // is the XMLHttpRequest object's responseXML property 

  // if the ajaxForm method was passed an Options Object with the dataType 
  // property set to 'json' then the first argument to the success callback 
  // is the json data object returned by the server 

  alert('status: ' + statusText + '\n\nresponseText: \n' + responseText.substr + 
      '\n\nThe output div should have already been updated with the responseText.'); 
} 