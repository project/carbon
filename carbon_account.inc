<?php
/**
 *
 * Drupal carbon account module
 *
 * Created on 18-Oct-2006
 *
 * @file
 * definition of carbon_account node. Each carbon_account
 * node contains settings such as choice of preferred carbon
 * model and privacy settings that control the presentation of
 * a carbon footprint.
 */



/**
 * Implementation of hook_access().
 *
 * Defer to carbon_access because it is easier to check
 * access for all carbon types in one place.
 */


function carbon_account_access($op, $node)
{
  return carbon_access($op, $node);
}


/**
 * Return an array of protection options
 * @return array
 */
function _get_protection_options() {
  return array(t("private"), t("share with others in my group"),
               t("share with all logged in users"), t("public"));
}


/**
 * Implementation of hook_form().
 */


function carbon_account_form(&$node, &$param) {
  global $user;

  $form = array();

  $form["account"] = array(
    "#type" => "fieldset",
    "#title" => "Carbon settings",
    "#weight" => -2,
    "#collapsible" => TRUE,
    "#collapsed" => FALSE);

  $form["account"]["title"] = array(
    "#type" => "textfield",
    "#title" => t("Title of this Account"),
    "#required" => TRUE,
    "#default_value" => empty($node->title) ? $node->name : $node->title,
    "#size" => 20,
    "#maxlength" => 40,
    "#weight" => -1,
    "#description" => t("Your name or someone else's name, name of a building etc. It can be different from your username."));

  if (variable_get("carbon_use_postal_code", 1))

    $form["account"]["postcode"] = array(
    "#type" => "textfield",
    "#title" => t("Postcode"),
    "#required" => FALSE,
    "#default_value" => empty($node->postcode) ? '' : $node->postcode,
    "#size" => 10,
    "#maxlength" => 20,
    "#weight" => 1,
    "#description" => t("Either some or all of your postcode or zipcode. Interesting to others " .
            "if you share your carbon account."));

  if (variable_get("carbon_use_organization", 1)
  ||  user_access("administer accounts")
  ||  user_access("administer carbon nodes"))

    $form["account"]["organization"] = array(
    "#type" => "textfield",
    "#title" => t("Local Group"),
    "#default_value" => empty($node->organization) ? '' : $node->organization,
    "#size" => 20,
    "#maxlength" => 20,
    "#weight" => 2,
    "#description" => t("Name of any local group, leave blank if none."),
    "#required" => FALSE);

  $model_map = CarbonSources::getDefault()->getModelMap();
  $model_keys = array_keys($model_map);
  if (variable_get("carbon_use_model_selection", 1))

    $form["account"]["model_index"] = array(
    "#type" => "select",
    "#title" => t("Preferred emissions model"),
    "#required" => FALSE,
    "#options" => array_values($model_map),
    "#default_value" => isset($node->model) ? array_search($node->model, $model_keys) : 0 ,
    "#description" => "Select the preferred model that you want to use to calculate the CO2 emissions." .
    " Not all the models support all possible <a href='?q=carbon/source'>carbon " .
    " sources</a>; in which case substitutes will be made. Leave as -no preference- if you are not sure.",
    "#weight" => 3);

  $form["account"]["protection"] = array(
    "#type" => "select",
    "#title" => t("Privacy"),
    "#required" => FALSE,
    "#default_value" => !isset($node->protection) ? 2 : $node->protection,
    "#description" => t("Who can see this carbon footprint?"),
    "#options" => _get_protection_options(),
    "#weight" => 4);


  $form["account"]["firstdate_s"] = array(
    "#type" => "textfield",
    "#title" => t("First day of reporting period"),
    "#required" => FALSE,
    "#size" => 20,
    "#maxlength" => 20,
    "#default_value" => empty($node->firstdate)  ? "" : _format_date($node->firstdate),
    "#weight" => 5,
    "#description" =>
               t("Format: %time. Can be left blank. The latest 12 months of " .
                "carbon stamps are used to calculate your footprint which will be " .
                "displayed alongside others in charts et cetera (assuming your privacy " .
                "settings allow this). You can override that date " .
                "by setting this field to the end of the period you want to use " .
                "and you can change the length of the period below. ",
                array("%time" => _format_date_only(time()))));

  $form["account"]["period"] = array(
    "#type" => "textfield",
    "#title" => t("Reporting period in months"),
    "#required" => FALSE,
    "#size" => 2,
    "#maxlength" => 20,
    "#default_value" => empty($node->period) ? 12 : $node->period,
    "#weight" => 6,
    "#description" =>
                t("The length of the reporting period in months."));

  $e0 = variable_get("carbon_energy_units_0", "");
  if (!empty($e0))
    $form["account"]["enablekwh0"] = array(
    "#type" => "checkbox",
    "#title" => t("Display !s in results", array("!s" => $e0)),
    "#required" => false,
    "#default_value" => isset($node->enablekwh0)? $node->enablekwh0 : false,
    "#weight" => 10);

  $e1 = variable_get("carbon_energy_units_1", "");
  if (!empty($e1))
    $form["account"]["enablekwh1"] = array(
    "#type" => "checkbox",
    "#title" => t("Display !s in results", array("!s" =>$e1)),
    "#required" => false,
    "#default_value" => isset($node->enablekwh1)? $node->enablekwh1 : false,
    "#weight" => 11);

  $form["account"]["enableco2"] = array(
    "#type" => "checkbox",
    "#title" => t("Display CO2 emissions in results"),
    "#required" => false,
    "#default_value" => isset($node->enableco2)? $node->enableco2 : true,
    "#weight" => 12);

  $form['#submit'] = array('carbon_account_submit');

  return $form;
}

/**
 * Validate our forms
 * @param string form id
 * @param array form values
 */
function carbon_account_validate($form_id, $form_values)
{

}

/**
 * Implementation of hook_view, add our node specific information
 * @param node object to display
 * @param boolean is this a teaser or full node?
 * @param boolean is this displaying on its own page
 */
function carbon_account_view(&$node, $teaser = FALSE, $page = FALSE)
{
  $node->content["account_view"] = array("#weight"=>0, "#value" => carbon_view($node, 'view'));
  return $node ;
}


/**
 * Implemenation of hook_load
 * @param node object to load additional information for
 * @return object with carbon fields
 */
function carbon_account_load($node)  {
  global $user ;
  $query = db_query("SELECT f.*, s.uid as friends_uid " .
    "FROM {carbon_account} f " .
    "LEFT  JOIN {carbon_account_share} s ON  f.nid = s.nid " .
    "WHERE f.nid = %d ",
    $node->nid);

  // most user will not be sharing records. However if they are
  // we pick up their friends uid's while loading the footprint
  $first_record = null ;
  while ($record = db_fetch_object($query))
  {
    if (!isset($first_record))
      $first_record = $record ;
      $first_record->friends = array();

    if (isset($friends_uid))
      $first_record->friends[] = $friends_uid ;
  }
  return $first_record;
}

/**
 * This function is declared in carbon_account_form and called prior
 * to node_save.
 *
 * @param $form
 * @param $form_state   discovered that there is a ['values'] field.
 *
 */
function carbon_account_submit(&$form, &$form_state)
{
  $model_keys = array_keys(CarbonSources::getDefault()->getModelMap());

  $f = &$form_state['values'];

  if (!empty($f['firstdate_s']))
  {
    $f['firstdate'] = strtotime($f['firstdate_s']) ;
  }

  if (isset($f['model_index']))
  {
    $f['model'] = $model_keys[$f['model_index']] ;
  }
}



/**
 * Implementation of hook_insert, which saves carbon-specific information
 * into the carbon table
 * @param node object
 */

function carbon_account_insert(&$node)
{
  db_query("INSERT INTO {carbon_account} (postcode, organization, model, firstdate, period, " .
    " enablekwh0, kwh0, enablekwh1, kwh1, enableco2, co2, " .
    " filing_status, protection, nid) " .
    " VALUES ('%s', '%s', '%s', %d,  %d, %d, %d, %d, %d, %d, %d, %d, %d, %d)",
  $node->postcode, $node->organization, $node->model, $node->firstdate, $node->period,
  $node->enablekwh0, $node->kwh0, $node->enablekwh1, $node->kwh1, $node->enableco2, $node->co2,
  $node->filing_status, $node->protection,$node->nid);
  drupal_set_message(t("To retrieve your carbon account on a later " .
        "date click on <em>my account</em>."));
}

/**
 * Implementation of hook_update, which saves updated todo-specific
 * information into the carbon_account table
 *
 * @param node object
 */
function carbon_account_update(&$node)
{
  $result = db_query("UPDATE {carbon_account} " .
        " SET postcode='%s', organization='%s', model='%s', firstdate=%d, period=%d, " .
        " enablekwh0=%d, kwh0=%d, enablekwh1=%d, kwh1=%d, enableco2=%d, co2=%d, " .
        " filing_status=%d, protection=%d WHERE nid=%d",
  $node->postcode, $node->organization, $node->model, $node->firstdate, $node->period,
  $node->enablekwh0, $node->kwh0, $node->enablekwh1, $node->kwh1, $node->enableco2, $node->co2,
  $node->filing_status, $node->protection,$node->nid);
  return $result ;
}


/**
 * Implementation of hook_delete().
 */
function carbon_account_delete($node) {
  db_query("DELETE FROM {carbon_stamp_account} WHERE fid = %d", $node->nid);
  db_query("DELETE FROM {carbon_account_share} WHERE nid = %d", $node->nid);
  db_query("DELETE FROM {carbon_account}       WHERE nid = %d", $node->nid);
}


?>
