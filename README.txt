
Carbon Drupal module

author : john ackers 


Features
-----------------------------
Allows drupal users to create and maintain personal carbon 
accounts. By entering utility meter readings, gasoline/petrol 
consumption, daily commutes and plane trips etc., users get an 
immediate indication of CO2 emissions. Measuring one's own 
footprint is a big shock and awakening for many people - it is 
not just the [insert your country] government that's at fault. 


Installation
-----------------------------

1.  Download the package into your site's '/modules' directory.
2.  Enable the 'Carbon' modules at 'Administer >> Site Building >> Modules'.
3.  Go to Administer >> Content Management >> Content Types and uncheck the  
    'promoted to front page' box on all the carbon entities.
4.  Configure permissions at 'Administer >> User Management as below.
5.  Install default sources of carbon emissions by going to 
    'Administer >> Site Configuration >> Carbon and click on 
    'install_carbon_sources'.
6.  Users can now create carbon accounts. 


Permissions
-----------------------------
Permissions are not configured during installation. For typical use, 
you should grant these permissions.

*role*                 *permissions*     
anonymous user         access accounts          
authenticated user     access accounts
                       create/edit account

Some users do need help and advice about creating their carbon
accounts. You might want to add a carbon accountant or carbon 
admin role and grant the permission 'administer carbon nodes' 
to those users providing the help.  


Configuring carbon sources 
-----------------------------
Administrators can add, adjust and remove different sources of 
carbon emissions. During installation, the carbon sources relevant 
to the UK are loaded. However these have to be amended for other 
countries. For example, much of France's electricity comes from
nuclear power and so the Kg of carbon emitted from one KilowattHour 
of energy will be lower that than in the UK. 


Shortcomings of this software
-----------------------------

1. Paging is not supported. If you ask to see all accounts
and you have access rights to see 5000, you will see them all. 

2. Sharing of accounts between users is not supported. 
However sharing between group members is. Individuals 
can currently change their group.

3. The conversion factors used to calculate KwH are not set. 
I am hoping that someone will produce a new model that includes
KwH or Joule conversion factors. 


Support
-----------------------------

Please submit issues (e.g. support requests, bug reports) to the 
drupal.org issue tracker. You will normally get a response in 
under a day. 


John Ackers   john.ackers HATT ymail.com
