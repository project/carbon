<?php
/*
 * Created on 30-May-2007
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */



/**
 * Render a page showing a table of carbon accounts and various columns
 * The actual content is determined by a specfic class and the class
 * name is derived from the name of the report.
 *
 * Note that the param $target_user is the user we are focused on, not necessarily
 * the user that is making the query.
 *
 * Additional information is extracted from the query using GET.
 *
 * Called directly from drupal menu.
 *
 * @param $target_user select by $target_user; set to null to retrieve footprints from all users
 * @param $map results returned here as array of $nid => $footprint
 */



function carbon_report_create($target_user, $displayQueryFormWanted)
{
  global $user;
  global $charts ; $charts = array(t("none"), t("bar"), t("pie"));
  global $orgs ;   $orgs = array("");  // add a blank field to represent all

  $params = _gather(array("report"=>"accounts", "firstdate"=>"20080101", "period"=>12,
  "org"=>null,"chart"=>"pie",
  "target"=> null, "cost"=> 0.05,
  "datatype"=>""));

  $org = $params["org"] ;
  $firstdate = CarbonDate::fromYYYYMMDD($params["firstdate"]);

  $path = carbon_include_core_js();
  require_once(CARBON_PATH ."/carbon_chart.inc");    // generate bar and pie PNGs
  require_once(CARBON_PATH ."/carbon_calculate.inc");// co2 & kWh settings

  $output = "" ;

  if (variable_get("carbon_enable_crag_functions", 0) > 0) // these lines
    require_once(CARBON_PATH ."/carbon_crag.inc");         // can be removed

  $reportName = "Carbon".ucFirst($params["report"]) ;

  if (!class_exists ($reportName))
  {
    return t("Report %report is unknown or unable to be loaded", array("%report" => $reportName));
  }

  $reporter = new $reportName();
  $reporter->initialize($params);
  $reporter->initHeader();

  $uid = isset($target_user) ? $target_user->uid : null ;

  $all_account_map = carbon_report_base($uid, null, $reporter->header);
  $account_map  = array();  $org_map = array();
  // build a list of all known orgs (it is filtered - bug))

  foreach ($all_account_map as $nid => $account)
  {
  	if (empty($org) or $org == $account->organization)
      $account_map[$nid] = $account ;
    $org_map[$account->organization]= 0 ;  // only interested in the keys
  }
  ksort($org_map);  $orgs = array_keys($org_map);

  $output .= $reporter->writeOutput($account_map, $uid, $firstdate, $params["period"]);

  if ($params["datatype"] == "json")
  {
    $output = drupal_json($output);
    print($output);
    exit(0);
  }

  $reporter->setTitle($org, $target_user, $firstdate, $params["period"]);

  // include IMG links to chart/pie image(s) specified in the query as 'chart='

//  $output .= carbon_chart_embed(null, null, null);

  if ($displayQueryFormWanted)
  {
    $output .= drupal_get_form("carbon_report_account_form", $params);

    $output .= drupal_get_form("carbon_report_sector_form", $params);

    $output .= drupal_get_form("carbon_report_history_form", $params);

    if (variable_get("carbon_enable_crag_functions", 0))
      $output .= drupal_get_form("carbon_crag_form", $params);
  }

  if (!empty($target_user))
  {
    $attributes = array('title'=>'Add first meter reading for a meter not listed above. ' .
                                 'Make sure you select meter reading on next form.',
                                 'class'=>'first-add');
    $output .= l("New account", "node/add/carbon-account",array('attributes'=>$attributes));
  }

  $output .= theme('pager', array(), 2, 0);

  return $output ;
}

/**
 * The CarbonAccounts class
 * creates a table with all the account names down on side
 * and columns containing other misc. account information
 *
 * This is the default view as it uses just one (the
 * original query and does not require any computation).
 *
 */


class CarbonAccounts
{
  var $header = null ;
  var $sampleRecord = null ;

	function initialize()
  {

  }

  function initHeader()
  {
    $this->header = array(
      array("data" => t("account name"),"field" => "title","sort" => "asc"),
      array("data" => t("postcode"),   "field" => "postcode"),
      array("data" => t("group"),      "field" => "organization"),
      array("data" => t("first date"), "field" => "firstdate"),

      array("data" => t("#stamps"),    "field" => "numstamps"),
      array("data" => t("last change"),"field" => "last_changed")
    );
  }

  function writeOutput($account_map, $uid, $firstdate, $period)
  {
    $rows = array();
    $lastdate = _add_months($firstdate, $period);

    foreach ($account_map as $nid => $account)
    {
      $rows[] =  array($account->account_link,
          $account->postcode,
          l($account->organization,
            "carbon/report",
            array("query"=> array("org"=>$account->organization))),
          empty($account->firstdate) ? '' : _format_date($account->firstdate),
          $account->numstamps <= 1 ? (isset($account->fid) ? 1 : 0) : $account->numstamps,
          _format_date_time($account->last_changed));
    }
    $output = theme("table", $this->header, $rows);
    $output .= theme("pager", NULL, 50, 0);
    return $output ;
  }

  function setTitle($org, $user, $firstdate, $period)
  {

    if (!empty($org))
      $title = t("Carbon Accounts for !org", array("!org" => $org));

    elseif (!empty($user))
      $title = t("Carbon Accounts owned by !name", array("!name" => $user->name));

    else
      $title = t("Carbon Accounts");

    drupal_set_title(check_plain(t($title)));
  }

  function attachForm()
  {
  	return "";
  }
}

/**
 * The CarbonSector class creates a spreadsheet that shows
 * account names on the y axis and energy use by sector on
 * the x axis.
 */


class CarbonSector
{
  var $header = null ;

  function initialize()
  {

  }

  function initHeader()
  {
    $this->header = array(array(
      "data" => t("account name"),"field" => "title","sort" => "asc"));
  }

  function writeOutput($account_map, $uid, $firstdate, $period)
  {
    global $orgs ;

    // for each item in the map, change the key and value to title and amount ;
    $footprint_by_title = array();  $legend = array();

    $results = array();
    $sectors = array();
    $lastdate = _add_months($firstdate, $period);

    foreach ($account_map as $nid => $account)
    {
      $stamps = carbon_stamp_array_load($nid, null, null);
      $footprint = carbon_calculate_total($account, $stamps, $firstdate, $lastdate);

      foreach ($footprint->sector_co2 as $sector => $amount)
      {
        $sectors[$sector] = 1 ;
      }
      $results[] = $footprint ;
    }

    $rows = array();
    foreach ($results as $footprint)
    {

  		// add first column, title
      $row = array($footprint->account->account_link);

      // add subsequent columns to table
      foreach ($sectors as $sector => $ignore)
      {
        if (!empty($footprint->sector_co2[$sector]))
          $row[] = sprintf("%.0f",$footprint->sector_co2[$sector]) ;
        else
          $row[] = "0" ;
      }
      $row[] = sprintf("%.0f",$footprint->grand_co2) ;
      $rows[] = $row ;

      $legend[] = $footprint->account->anonomized_title ;
      $footprint_by_title[] = $footprint->sector_co2 ;
    }

    foreach ($sectors as $sector => $ignore)
    {
      $this->header[] = $sector ;
    }
    $this->header[] = "total" ;

    $output .= theme("table", $this->header, $rows);

    $output .= carbon_chart_stacked_bar("Carbon emissions", $footprint_by_title, $legend, "Kg of CO2");

    return $output ;
  }

  function setTitle($org, $user, $firstdate, $period)
  {
    $title = empty($org) ? "" : "!org " ;
  	$title .= "CO2 emissions by sector for !dates" ;
    $lastdate = _add_months($firstdate, $period);
  	$daterange =  _format_date_range($firstdate, $lastdate);
    drupal_set_title(t($title, array("!org"=>$org, "!dates"=>_format_date_range($firstdate, $lastdate))));
  }

  function attachForm()
  {
    return "";
  }
}


/**
 * The CarbonHistory class creates a spreadsheet that shows
 * account names on the y axis and energy use by sector on
 * the x axis.
 */


class CarbonHistory
{
  var $header = null ;

  function initialize()
  {

  }

  function initHeader()
  {
    $this->header = array(array(
      "data" => t("account name"),"field" => "title","sort" => "asc"));
  }

  /**
   *
   * @param $account_map
   * @param $uid
   * @param $firstdate
   * @param $lastdate  is the last date of the first period
   * @return unknown_type
   */

  // todo rationalize calculations, use narrow bars, use merged palette

  function writeOutput($account_map, $uid,  $firstdate, $period)
  {
    global $orgs ;

    // for each item in the map, change the key and value to title and amount ;

    $report_by_sector = array();
    $report_by_account = array();

    $results = array();
    $sectors = array();
    $alias = array();

    // it would be nice to calculate emissions for multiple accounts
    // in one go.  However that would require loading of a large number
    // of carbon stamps at a time.

    foreach ($account_map as $nid => $account)
    {
   //   if ($nid != 362 && $nid != 425) continue ;
   //   if ($nid != 419 && $nid != 1607) continue ;

      $stamps = carbon_stamp_array_load($nid, null, null);
      $dateRange = _stamp_date_range($stamps);

      // the last date of the report is determined by the last stamp

      $reportDates = carbon_calculate_reporting_dates($dateRange, $firstdate, $period);


      for ($i = count($reportDates) - 2 ;  $i >= 0  ; $i--)
      {
        $footprint = carbon_calculate_total($account, $stamps, $reportDates[$i], $reportDates[$i+1]);

        $firstdate_t = CarbonDate::asYYYYMMDD($reportDates[$i]);

        $alias[$reportDates[$i]] = _format_date_range($reportDates[$i], $reportDates[$i+1]);

        foreach ($footprint->sector_co2 as $sector => $value)
        {
          // store data with keys in different orders
          _add_to_subarray($report_by_sector,  $sector, $reportDates[$i], $footprint->account->anonomized_title, $value );
          _add_to_subarray($report_by_account, $footprint->account->anonomized_title, $reportDates[$i],  $sector, $value );

          // store data in separate arrays
          _add_to_subarray($report_by_account, '#all-account-table', $reportDates[$i],  $sector, $value);
          _add_to_subarray($report_by_account, '#all-account-table', $reportDates[$i], '#total', $value);

          _add_to_subarray($report_by_account, '#all-account-bar-chart', $reportDates[$i],  $sector, $value );

        }

        // have to keep totals out of the data presented as elements in the bar chart
        // but we won't be presenting all-accounts

        if (!empty($report_by_account['#all-account-table'][$reportDates[$i]]['#total']))  // skip empty accounts
        {
          _add_to_subarray($report_by_account, '#all-account-table', $reportDates[$i], '#numAccounts', 1);
        }

        foreach ($footprint->sector_co2 as $sector => $value)
        {
          $sectors[$sector] = true ;
        }
      }
    }

//    $report_by_account['stay-at-'] = array("1992"=>array("heating"=>55, "elec"=>25, "air"=>20), "1996"=>array("heating"=>15, "elec"=>45, "surface"=>40));

    foreach($report_by_account['#all-account-table'] as $timestamp => $sector_co2)
    {
      $numAccounts = $sector_co2['#numAccounts'] ;

//      _add_to_subarray($report_by_account, '#all-account-table', $timestamp,
  //     '#total', $sector_co2['#total'] );

      _add_to_subarray($report_by_account, '#all-account-table', $timestamp,
        '#average', empty($numAccounts) ? 0 : $sector_co2['#total'] / $numAccounts);

      foreach($sector_co2 as $sector => $val)
      {
        _add_to_subarray($report_by_account, '#all-account-table', $timestamp,
          $sector, empty($numAccounts) ? 0 : $val / $numAccounts);
      }
    }

    // we now have a results matrix, now to present it in a table.

    $this->header[] = "accounts" ;
    $this->header[] = "total" ;
    $this->header[] = "average" ;

    foreach ($sectors as $sector => $ignore)
    {
      $this->header[] = $sector ;
    }

    $rows = array();
    foreach ($report_by_account['#all-account-table'] as $reportdate => $sector_array)
    {
      $numAccounts = $sector_array['#numAccounts'];

      // add first column, title
      $row = array($alias[$reportdate]);

      $row[] = $numAccounts ;
      $row[] = sprintf("%.0f",$sector_array['#total']) ;
      $row[] = sprintf("%.0f",$sector_array['#average']) ;

      // add subsequent columns to table
      foreach ($sectors as $sector => $ignore)
      {
        if (is_numeric($sector_array[$sector]))
        {
          $row[] = sprintf("%.0f",$sector_array[$sector]) ;
        }
        else
        {
          $row[] = "0" ;
        }
      }
      $rows[] = $row ;
    }

    $output .= theme("table", $this->header, $rows);

    $chart = carbon_chart_stacked_bar($title = "Historic carbon emissions",
      $report_by_account['#all-account-bar-chart'], $alias, "Kg of CO2");

    $output .= count($report_by_account) == 0  ? "" : theme("carbon_static_box", $title, $chart);

    // now generate one chart for each sector

//  test data
//  $report_by_sector['heating'] = array("1992"=>array("john"=>55, "fred"=>53, "susan"=>51), "1993"=>array("john"=>53, "fred"=>51, "susan"=>49));

    foreach($report_by_sector as $sector => $data)
    {
      $output .= $this->create_sector_report($sector, $data, $alias);
    }
    return $output ;
  }


  private function create_sector_report($sector, $data, $alias)
  {
     $chart =  carbon_chart_grouped("Carbon emissions from '". $sector . "'", $data, $alias, $sector);
     return count($data)==0  ? "" : theme("carbon_static_box", $sector, $chart);
  }


  function setTitle($org, $user, $firstdate, $lastdate)
  {
    $title = empty($org) ? "" : "!org " ;
    $title .= "emissions history by sector" ;
    drupal_set_title(t($title, array("!org"=>$org)));
  }

  function attachForm()
  {
    return "";
  }
}

/**
 * Place a number in a two dimensional array
 *
 * @param unknown_type $target   (address of) existing target array
 * @param unknown_type $key1     outer key
 * @param unknown_type $key2     outer key
 * @param unknown_type $key3     inner key
 * @param unknown_type $number   numeric value
 * @return unknown_type
 */

function _add_to_subarray(&$target, $key1, $key2, $key3, $number)
{
  if (!is_array($target[$key1]))   // to avoid warnings
    $target[$key1]= array();
  if (!is_array($target[$key1][$key2]))   // to avoid warnings
    $target[$key1][$key2]= array();
  if (!array_key_exists($key3, $target[$key1][$key2]))
   $target[$key1][$key2][$key3] = 0 ;

  $target[$key1][$key2][$key3] += $number ;
}


function carbon_report_account_form(&$form_state, &$default_params)
{
  $path = drupal_get_path("module", "carbon");
  drupal_add_css($path . "/carbon.css");

  $form = array();

  $form["select"] = array(
    "#type" => "fieldset",
    "#title" => "Account report selection",
    "#collapsible" => FALSE,
    "#collapsed" => FALSE);

  $form["select"]["report"] = array(
    "#type" => "hidden",
    "#value" => "accounts");

  carbon_report_form_build($form["select"], $default_params, array("org"));

  $form["select"]["submit"] = array(
    "#type" => "submit",
    "#value" => t("show accounts"),
    "#prefix"=> "<DIV class='float-left'>",
    "#suffix"=> "</DIV>",
    "#weight" => 2);

  return $form ;
}


/**
 * this form allows the user to set the carbon price, target etc
 * and then request the report that shows the calculations.
 */


function carbon_report_sector_form(&$form_state, &$default_params)
{
  $path = drupal_get_path("module", "carbon");
  drupal_add_css($path . "/carbon.css");

  $form = array();

  $form["select"] = array(
    "#type" => "fieldset",
    "#title" => "Sector report selection",
    "#collapsible" => FALSE,
    "#collapsed" => FALSE);


  $form["select"]["report"] = array(
    "#type" => "hidden",
    "#value" => "sector");

  carbon_report_form_build($form["select"], $default_params, array("org", "firstdate", "period" /*, "chart" */));

  $form["select"]["submit"] = array(
    "#type" => "submit",
    "#value" => t("show CO2"),
    "#prefix"=> "<DIV class='float-left'>",
    "#suffix"=> "</DIV>",
    "#weight" => 2);

  $form["select"]["#title"] = "Sector report selection";

  return $form ;
}



/**
 * this form allows the user to request history of a group of
 * accounts
 */


function carbon_report_history_form(&$form_state, &$default_params)
{
  $path = drupal_get_path("module", "carbon");
  drupal_add_css($path . "/carbon.css");

  $form = array();

  $form["select"] = array(
    "#type" => "fieldset",
    "#title" => "Sector report selection",
    "#collapsible" => FALSE,
    "#collapsed" => FALSE);

  $form["select"]["report"] = array(
    "#type" => "hidden",
    "#value" => "history");

  carbon_report_form_build($form["select"], $default_params, array("org", "firstdate", "period" /*, "chart" */));

  $form["select"]["submit"] = array(
    "#type" => "submit",
    "#value" => t("show history"),
    "#prefix"=> "<DIV class='float-left'>",
    "#suffix"=> "</DIV>",
    "#weight" => 2);

  $form["select"]["#title"] = "History report selection";

  return $form ;
}



/**
 * add specified fields to the form structure
 *
 * @param $form    target form
 * @param $params  array of default parameters
 * @param $fields  desired fields
 * @return unknown_type
 */


function carbon_report_form_build(&$form, $params, $fields)
{
  global $charts, $orgs ;
  $path = drupal_get_path("module", "carbon");

  foreach($fields as $field)
  {
  	switch ($field)
    {
      case "org" :
        $form["org"] = array(
          "#type" => "select",
          "#title" => t("group"),
          "#options" => $orgs,
          "#default_value" => array_search($params["org"],$orgs),
          "#required" => FALSE,
          "#prefix"=> "<DIV class='float-left'>",
          "#suffix"=> "</DIV>",
          "#weight" => -4);
        break;


    	case "firstdate" :
        $form["firstdate"] = array(
          "#type" => "textfield",
          "#title" => t("starting date"),
          "#required" => FALSE,
          "#size" => 15,
          "#maxlength" => 40,
          "#description" => t("Format: %time", array("%time" => _format_date_only(time()))),
          "#default_value" => _format_date(CarbonDate::fromYYYYMMDD($params["firstdate"])),
          "#prefix"=> "<DIV class='float-left'>",
          "#suffix"=> "</DIV>",
          "#weight" => -3);
        break;

      case "period" :
        $form["select"]["period"] = array(
          "#type" => "textfield",
          "#title" => t("period"),
          "#description" => t("months"),
          "#default_value" => $params["period"],
          "#required" => FALSE,
          "#size" => 10,
          "#maxlength" => 40,
          "#prefix"=> "<DIV class='float-left'>",
          "#suffix"=> "</DIV>",
          "#weight" => -2);
        break;

      case "chart" :
        $form["chart"] = array(
          "#type" => "select",
          "#title" => t("chart"),
          "#options" => $charts,
          "#default_value" => array_search($params["chart"],$charts),
          "#required" => FALSE,
          "#maxlength" => 3,
          "#prefix"=> "<DIV class='float-left'>",
          "#suffix"=> "</DIV>",
          "#weight" => 1);
        break;

      case "submit" :
        $form["submit"] = array(
          "#type" => "submit",
          "#value" => t("show accounts"),
          "#prefix"=> "<DIV class='float-left'>",
          "#suffix"=> "</DIV>",
          "#weight" => 2);
        break;
    }
  }
}

/**
 * redirect a post operation to a new URL
 */

function carbon_report_account_form_submit($form, $form_state)
{
  return carbon_report_form_submit($form, $form_state);
}
function carbon_report_history_form_submit($form, $form_state)
{
  return carbon_report_form_submit($form, $form_state);
}
function carbon_report_sector_form_submit($form, $form_state)
{
  return carbon_report_form_submit($form, $form_state);
}

/**
 * When the user submits a form request, an HTTP post is generated.
 * We convert this into an HTTP get. The reason for doing this is
 * so that users can bookmark interesting reports for subsequent use
 * as well as editing them to change start date, change period.
 *
 * @param unknown_type $form
 * @param unknown_type $form_values
 */

function carbon_report_form_submit($form, $form_state)
{
  global $orgs, $charts ;

  $link = "";

  $params = array("report", "period");
  $params[] = "target" ; $params[] = "cost" ;  // used only for CRAG page

  // extract all the parameters that we'd expect to find in the POST
  $first = true ;
  foreach ($params as $name)
  {
  	if (isset($form_state['values'][$name])) $link .= ($first ? "" : "&").$name."=".$form_state['values'][$name];
    $first = false ;
  }
  // parse date to 6 char string
  if (isset($form_state['values']['firstdate']))
  {
    $firstdate_s = CarbonDate::asYYYYMMDD(strtotime($form_state['values']['firstdate']));
    $link .= "&firstdate=".$firstdate_s ;
  }

  // have to handle org and chart fields differently because they are pull down boxes
  if (isset($form_state['values']["org"]))
    if (!empty($orgs[$form_state['values']["org"]]))
      $link .= "&org=".$orgs[$form_state['values']["org"]];

  if (isset($form_state['values']["chart"]))
    if (!empty($charts[$form_state['values']["chart"]]))
      $link .= "&chart=".$charts[$form_state['values']["chart"]];

  drupal_goto($_GET['q'], $link);
}


/**
 * Create a skeleton report page.
 * It creates a set of rows each representing a carbon account
 * There is only one mandatory column, the account name.
 * Optional columns can be added depending on the report name.
 *
 * Protection and page selection are done here.
 */

function carbon_report_base($uid, $org, $header)
{
  $account_map = array();

  $queryResult = carbon_report_query($uid, $org, $header);

  while ($account = db_fetch_object($queryResult))
  {
    $access = carbon_access("view", $account) or ($org == $account->organization) ;

    $account->anonomized_title = $access ? $account->title : carbon_anonomize($account->title);
  	$account->account_link = carbon_link_from_title($account, $account, "");
    $account->transfer_link = carbon_link_from_title($account, $account, "transfer");
    $account_map[$account->nid] = $account  ;
  }
  return $account_map ;
}



/**
 * Account retrieval, used by several other functions
 * There is an underlying simple query that only involves
 * a single select query.
 */

function carbon_report_query($uid, $org, $header)
{
  $sql =     "SELECT an.nid, an.title, an.uid, an.type, a.*, u.name, " .
             "count(*) as numstamps, max(sn.changed) as last_changed " .
             ",s.uid AS shared_uid, protection " .
             "FROM {carbon_account} a " .
             "INNER JOIN {node} an USING (nid) " .

             "LEFT JOIN {carbon_stamp_account} j ON  an.nid = j.fid " . // to get num stamps
             "LEFT JOIN {node}                 sn ON j.sid = sn.nid " . // to get last entry

             "LEFT  JOIN {carbon_account_share} s ON  a.nid = s.nid " . // to get perm to access
             "INNER JOIN {users} u ON an.uid = u.uid " .
             "WHERE (u.uid = %d " .          (empty($uid) ?  " OR 1) " : ") ").     // opt select by user
             "AND (a.organization = '%s' " . (empty($org) ?  " OR 1) " : ") ") ;    // opt select by org

  //if og_carbon is present, filter by organic group - sql condition
  if (module_exists('og_carbon') && module_exists('og_basic')) {
    $sql = og_carbon_sql($sql, 'condition');
  }

  // non admin users only permitted to see shared footprints and
  // footprints from same group or public footprints.
  // have to filter in the SQL in case there are a lot of records
  // Note. private footprints in same group are shown but not linked

  $sql .=  "AND (  (s.uid = %d)  " .   // ie shared with target user
             "    OR (an.status AND length(a.organization) > 1 and a.organization = '%s') " .
             "    OR (an.uid = %d) ".  // should restrict anonymous user
             "    OR  1 " .
             "    ) " ;
  $sql .=    "GROUP BY an.nid " ;   // all stamps with the same account
  $sql .= tablesort_sql($header);

  //if og_carbon is present, filter by organic group - sql rewrite
  if (module_exists('og_carbon') && module_exists('og_basic')) {
    $sql = og_carbon_sql($sql, 'rewrite');
  }

  // drupal_set_message(" my sql ". $sql . " org:". $org. " uid:" . $uid, 'info');

  // get the links (no range limit here)

  global $user ;
  $queryResult = db_query($sql, $uid, $org, $uid, $org, $user->uid);

  return $queryResult ;
}



/**
 * Render an image showing multiple carbon footprints
 *
 * Note that the param $uid is the user we are focused on, not the user doing
 * the browsing.
 *
 * @param $account_map an array of $nid => $footprint
 * @param $uid select by uid; set to zero to retrieve footprints from all users
 * @param $org select by organisation; set to '' or null to retrieve footprints
 * from users in any group.
 */

function carbon_report_page_chart($account_map, $uid, $org, $firstdate, $lastdate){
  global $user;
  // for each item in the map, change the key and value to title and amount ;
  $footprint_by_title = array();
  $legend = array();

  foreach ($account_map as $nid => $account)
  {
    $stamps = carbon_stamp_array_load($nid, $uid, $org);

    $daterange = _format_date_range($firstdate, $lastdate);

    $results = carbon_calculate_total($account, $stamps, $firstdate, $lastdate);

    // there is a potential problem here in that only one of a group
    // of footprints of the same name will be shown.

    $footprint_by_title[] = $results->sector_co2 ;
    $legend[] = $account->anonomized_title ;
  }
  $chart = carbon_chart($footprint_by_title, $legend, "Kg of CO2");

  return carbon_chart_generate($footprint_by_title, $legend, "CO2 emissions");
}





/**
 * Provide support for account select boxes
 */


class CarbonAccountMap
{
  var $account_map = null ;
  var $title_array = array();
  var $nid_array = array();

  function init()
  {
    $this->account_map = carbon_report_base(null,null,array());
    foreach ($this->account_map as $account)
    {
      $this->title_array[] = $account->title ;
      $this->nid_array[] = $account->nid ;
    }
    return $this ;
  }

  function getArray()
  {
    return $this->title_array ;
  }

  function getArrayIndex($nid)
  {
  	$position = array_search($nid, $this->nid_array);
    return $position ;
  }

  function getAccountLink($nid)
  {
    $account = $this->account_map[$nid];
    return $account->account_link ;
  }

  function getTransferLink($nid)
  {
    $account = $this->account_map[$nid];
    return $account->transfer_link ;
  }

  function getTitle($nid)
  {
    $account = $this->account_map[$nid];
    return $account->title ;
  }

  function getNid($index)
  {
  	return $this->nid_array[$index];
  }

  function sortByRevisionDate()
  {
    $account_map_recent = array();
  	foreach ($this->account_map as $key=>$account)
    {
      $account_map_recent[$account->last_changed] = $account ;
    }
    $this->account_map =  $account_map_recent;
    krsort($this->account_map);
    return $this->account_map ;
  }
}


/**
 * gather a range of parameters using GET
 * used by carbon_report().
 * There's probably a better way to do this.
 *
 * @param $names is array of keys / default values
 * @return array of keys / values
 * */

function _gather($names)
{
  $q = array();
  foreach ($names as $name => $default_value)
  {
    if (!empty($_GET[$name]))
      $q[$name] = $_GET[$name] ;
    else
      $q[$name] = $default_value ;
  }
  return $q ;
}

function _gather_form($form, $names)
{
  $q = array();
  foreach ($names as $name => $default_value)
  {
    if (!empty($form[$name]))
      $q[$name] = $form[$name] ;
    else
      $q[$name] = $default_value ;
  }
  return $q ;
}





?>
