<?php
 /**
 *
 * Drupal carbon CRAG module
 *
 * Created on 17-May-2007
 *
 * @file
 *
 * Provides the settlement reports that are unique to the Carbon
 * Rationing Action Groups  http://www.carbonrationing.org.uk.
 *
 * The file is included by carbon_report_page
 *
 * The functions in this file are not used or required for
 * general use of the carbon module.
 *
 */


class CarbonCrag
{
  var $header = null ;
  var $sampleRecord = null ;
  var $params = null ;

  function initialize(&$params)
  {
    if (empty($params["cost"]))       $params["cost"] = 0.05 ;
    $this->params = $params ;
    require_once(CARBON_PATH ."/carbon_transfer.inc");   // carbon purchases/sales (used by crags)
  }

  /**
   * create column headers in array this->header
   */

  function initHeader()
  {
    $this->header = array(array(
      "data" => t("account name"),"field" => "title","sort" => "asc"));
  }

  function writeOutput($map, $uid,  $firstdate, $period)
  {
    $target = $this->params["target"];
    $lastdate = _add_months($firstdate, $period);

    // for each item in the map, change the key and value to title and amount ;
    $footprint_by_title = array();

    $rows = array();
    $debitCarbon = 0 ;     $creditCarbon = 0 ;
    $debitCash = 0 ;       $debitCredit = 0 ;

    $results = array();
    foreach ($map as $nid => $account)
    {
      $stamps = carbon_stamp_array_load($nid, null, null);

      $footprint = carbon_calculate_total($account, $stamps, $firstdate, $lastdate);

      $results[] = $footprint;

      $over = 0 ;
      if (!empty($target))
      {
        $actual = sprintf("%.0f",$footprint->grand_co2);
        $over = $actual - $target ;

        if ($over > 0)
          $debitCarbon += $over ;
        else
          $creditCarbon -= $over ;
      }
    }

    if ($creditCarbon > $debitCarbon)
    {
      $creditPrice = $this->params["cost"]*$debitCarbon/$creditCarbon ;
    }
    else
    {
      $creditPrice = $this->params["cost"] ;
    }
    $debitPrice = $this->params["cost"] ;         // debtors pay the same

    $nummembers = 0 ;
    $grouptotal = 0 ;
    foreach ($results as $footprint)
    {
      $nummembers++ ;
      $actual = sprintf("%.0f",$footprint->grand_co2);
      $grouptotal += $footprint->grand_co2 ;
      $under = $target - $actual ;
      if ($under < 0)
      {
        $debitCash += $cash = $this->params["cost"] * $under ;
      }
      else
      {
        $creditCash -= $cash = $creditPrice * $under ;
      }
      $rows[] = array($footprint->account->account_link, $actual, $this->sign($under), $this->sign(sprintf("%.2f",$cash)));

    }
    $header = array(
      array("data" => t("account name")),
      array("data" => t("Emissions Kg")),
      array("data" => t("Credit or Debit Kg")),
      array("data" => t("Credit of Debit £"))
    );
    if ($nummembers == 0)
      $comment = "No members" ;
    else
      $comment = t("Total emissions are !carbon-grand Kg. Average emissions per person are !carbon-average Kg." .
                 "</p><p>".
                 "Over emitters: the carbon price is set at !debit-carbon-price. ".
                 "The total carbon debt is !debit-carbon Kg. ".
                 "The total money to pay is !debit-cash. ".
                 "</p><p>".
                 "Under emitters: the carbon price is !credit-carbon-price. ".
                 "The total carbon credit is !credit-carbon Kg. ".
                 "If under emitters paid, total money that might be received: !credit-cash.",

                 array(
                       "!carbon-grand"=>sprintf("%.2f",$grouptotal),
                       "!carbon-average"=>sprintf("%.2f",$grouptotal/$nummembers),
                       "!credit-carbon"=>$creditCarbon,
                       "!credit-cash"=>sprintf("%.2f",$creditCash),
                       "!credit-carbon-price"=>sprintf("%.2f",$creditPrice),

                       "!debit-carbon"=>$debitCarbon,
                       "!debit-cash"=>sprintf("%.2f",$debitCash),
                       "!debit-carbon-price"=>sprintf("%.2f",$debitPrice)));

    $daterange = _format_date_range($firstdate, $lastdate);
    if (!empty($target))
    {
      $output .= "<p>Suggested CRAG Settlement  for ". $daterange. ". Target is ".$target. "Kg CO2.</p>";
      $output .= "<p>".$comment."</p>" ;
    }
    $output .= theme("table", $header, $rows);
    return $output ;
  }


  function allocate(&$map, $uid, $org, $firstdate, $lastdate, $target, $carbonCost)
  {
    // calculate the footprints
    foreach ($map as $nid => $footprint)
    {
      $t = new CarbonTransaction();
      $t->initialize();
      $t->title = "allocation" ;
      $t->carbon = $target ;
      $t->cost = null ;
      $t->seller = 1 ;    // admin
      $t->buyer =  $nid ; //
      $t->valuedate = $lastdate ;
      $transactions[] = $t ;
    }
    foreach ($transactions as $t)
    {
      $r = node_save($t);
      drupal_set_message($t->toString($map), "info");
    }
  }

  function deduct(&$map, $uid, $org, $firstdate, $lastdate, $target, $carbonCost)
  {
    // calculate the footprints
    foreach ($map as $nid => $account)
    {
      $stamps = carbon_stamp_array_load($nid, null, null);
      $footprints[$account->nid] = carbon_calculate_total($account, $stamps, $firstdate, $lastdate);

      $t->initialize();
      $t->title = "deduct" ;
      $t->carbon =  - $footprints[$account->nid];
      $t->cost = null ;
      $t->seller = 1 ;    // admin
      $t->buyer =  $nid ; //
      $t->valuedate = $lastdate ;
      $transactions[] = $t ;
    }
    foreach ($transactions as $t)
    {
      $r = node_save($t);
      drupal_set_message($t->toString($map), "info");
    }
  }

  /**
   * execute settlement by
   * 1. adding quota to account at start period.
   * 2. settle carbon amongst CRAG members
   *
   * @param key-value map of account number/footprint
   * @param unknown_type $org
   */

  function settle(&$map, $uid, $org, $firstdate, $lastdate, $target, $carbonCost)
  {
    $footprints = array();

    // calculate the footprints
    foreach ($map as $nid => $account)
    {
      $stamps = carbon_stamp_array_load($nid, null, null);
      $footprints[$account->nid] = carbon_calculate_total($account, $stamps, $firstdate, $lastdate);
    }
    $debitCarbon = 0 ;     $creditCarbon = 0 ;
    $debitCash = 0 ;       $debitCredit = 0 ;


    if (empty($target))
      return ;

    $numMembers = 0 ;
    $creditors = array() ;
    $debtors = array() ;

    foreach ($footprints as $nid => $f)
    {
      $numMembers++ ;
    	$actual = $f->grand_co2;
      $f->CO2_above_target = $actual - $target ;

      if ($f->CO2_above_target  > 0)
      {
        $debitCarbon += $f->CO2_above_target;
        $debtors[$nid] = $f->CO2_above_target ;
      }
      if ($f->CO2_above_target  < 0)
      {
        $creditCarbon -= $f->CO2_above_target;
        $creditors[$nid] = -$f->CO2_above_target ;
      }
    }

    if (count($creditors) >= $numMembers)
    {
      drupal_set_message("All accounts in credit, no settlement required.");
      return ;
    }

    if (count($debtors) >= $numMembers)
    {
    	drupal_set_message("All accounts in debit, settlement not possible.");
      return ;
    }

/*
 *
p(. If total debts is greater than total credits  (i.e. there are enough surplus units and the group has come in under target)

p((. number of credits that each CRAGger in debt must purchase from ever other CRAGger [1..n] if the CRAGger is in credit

p(((. = size of debt  * number of credits held by CRAGger [1..n] in credit /  total credits

p(. else (the group has missed its target)

p((. number of credits that each CRAGger in debt must purchase from every other CRAGger [1..n] if s/he is in credit

p(((. = size of debt * number of credits held by CRAGger [1..n] /  total debts

CRAGger [1..n] represents all the other members of the CRAG.

 */
    $group_met_target =  $creditCarbon < $debitCarbon  ;
    $total_purchase = 0 ;
    $transactions = array();
    foreach ($debtors as $did => $d)
    {
    	// go to each account with carbon credit and purchase carbon
      foreach ($creditors as $cid => $c)
      {
        if ($group_met_target)
        	$purchase = $footprints[$did]->CO2_above_target * -$footprints[$cid]->CO2_above_target / $debitCarbon ;
        else
          $purchase = $footprints[$did]->CO2_above_target * -$footprints[$cid]->CO2_above_target / $creditCarbon ;

        $debtors[$did] -= $purchase ;
        $creditors[$cid] -= $purchase ;
        $total_purchase += $purchase ;
	      $t = new CarbonTransaction();
	      $t->initialize();
	      $t->title = "settlement" ;
	      $t->carbon = $purchase ;
	      $t->cost = $purchase * $carbonCost ;
	      $t->seller = $cid ;  // creditors account id
	      $t->buyer =  $did ;  // debtors account id
	      $t->valuedate = $lastdate ;
	      $transactions[] = $t ;
      }
    }

    // commit all the transactions as a batch

    foreach ($transactions as $t)
    {
      $r = node_save($t);
      drupal_set_message($t->toString($map), "info");
    }
  }

  function setTitle($org, $user, $firstdate, $lastdate)
  {
    $title = empty($org) ? "" : "!org " ;
    $title .= "CRAG settlement for !dates" ;
    $daterange =  _format_date_range($firstdate, $lastdate);
    drupal_set_title(t($title, array("!org"=>$org, "!dates"=>_format_date_range($firstdate, $lastdate))));
  }


  function attachForm()
  {
    return "" ;
  }

  /**
   * represent a signed number as a credit of debit as preferred by Jessica.
   */

  function sign($amount)
  {
    return $amount < 0 ? "D ".-$amount : "C ".$amount ;
  }
}


/**
 * this form allows the user to set the carbon price, target etc
 * and then request the report that shows the calculations.
 */


function carbon_crag_form($form_state, $default_params)
{
  $path = drupal_get_path("module", "carbon");
  drupal_add_css($path . "/carbon.css");

  $form = array();

  $form["select"] = array(
    "#type" => "fieldset",
    "#title" => "CRAG report selection",
    "#collapsible" => FALSE,
    "#collapsed" => FALSE);

  $form["select"]["report"] = array(
    "#type" => "hidden",
    "#value" => "crag");

  carbon_report_form_build($form["select"], $default_params, array("org", "firstdate", "period"));


  $form["select"]["target"] = array(
    "#type" => "textfield",
    "#title" => t("target"),
    "#description" => t("Kg of CO2"),
    "#default_value" => $default_params["target"],
    "#required" => FALSE,
    "#size" => 12,
    "#maxlength" => 40,
    "#prefix"=> "<DIV class='float-left'>",
    "#suffix"=> "</DIV>",
    "#weight" => 3);

  $form["select"]["cost"] = array(
    "#type" => "textfield",
    "#title" => t("cost of carbon"),
    "#description" => t("£ per Kg"),
    "#default_value" =>  $default_params["cost"],
    "#required" => FALSE,
    "#size" => 12,
    "#maxlength" => 40,
    "#prefix"=> "<DIV class='float-left'>",
    "#suffix"=> "</DIV>",
    "#weight" => 4);
/*
  $form["select"]["allocate"] = array(
    "#type" => "submit",
    "#value" => t("allocate"),
    "#prefix"=> "<DIV class='float-left'>",
    "#suffix"=> "</DIV>",
    "#weight" => 6);

  $form["select"]["deduct"] = array(
    "#type" => "submit",
    "#value" => t("deduct"),
    "#prefix"=> "<DIV class='float-left'>",
    "#suffix"=> "</DIV>",
    "#weight" => 7);
*/
  $form["select"]["submit"] = array(
    "#type" => "submit",
    "#value" => t("show settlement"),
    "#prefix"=> "<DIV class='float-left'>",
    "#suffix"=> "</DIV>",
    "#weight" => 8);

  $form["select"]["pay"] = array(
    "#type" => "submit",
    "#value" => t("pay settlement"),
    "#prefix"=> "<DIV class='float-left'>",
    "#suffix"=> "</DIV>",
    "#weight" => 9);

  return $form ;
}



function carbon_crag_form_submit(&$form, &$form_state)
{
  $form_values = $form_state['values'];
	if (isset($form_values["pay"]))
	{
	  $op = ($form_values["op"]);
	  if ($op == t("pay settlement"))
	  {
	  	_carbon_settle($form, $form_values);
	  	drupal_set_message("Settlement processed", "info");
	  }
	}
  return carbon_report_form_submit($form, $form_state);
}

function _carbon_settle($form, $form_values)
{
  global $user;
  global $charts ; $charts = array(t("none"), t("bar"), t("pie"));
  global $orgs ;   // no idea where this is set

  $params = _gather_form($form_values, array("report"=>"accounts", "firstdate"=>"20050101", "period"=>60, "org"=>"","chart"=>"pie",
  "target"=> null, "cost"=> 0.05));

  if (!empty($params["org"]))
    $org = $orgs[$params["org"]]; // convert org_index into real name

  $firstdate = CarbonDate::fromYYYYMMDD($params["firstdate"]);
  $lastdate = _add_months($firstdate, $params["period"]);

  // build a list of all known orgs (scaleable?)

  $reporter = new CarbonCrag();
  $reporter->initialize($params);

  $map = carbon_report_base($uid, $org, $reporter->header);

  $reporter->settle($map, $uid, $org, $firstdate, $lastdate, $params["target"], $params["cost"]);

  drupal_goto("carbon/transfer");
}

?>
