<?php
/*
 *
 * Created on 19-Feb-2008
 *
 *
 * Provide the helper functions that support row editing in a
 * table with ajax transfers. Functions are listed in the order
 * that they would be invoked.
 */

/**
 * Wrap a standard table theme finction with a form wrapper.
 * Invoked when the whole table is created.
 *
 *
 * @param $header
 * @param $rows
 * @return unknown_type
 */

function theme_table_inside_form($header, $rows)
{
  $output = "<form class='ajaj'>"   // must match class that is hard coded in carbon.js
             . theme("table", $header, $rows)
             . '</form>';
  return $output ;
}


/**
 * This function is called to replace a row of fixed data in a table with a
 * number of form fields that represent the editable fields of the node.
 *
 * Menu entry point.
 *
 * @param $node
 * @return unknown_type
 */

function carbon_ajax_edit($account, $tablename, $verb, $node)
{
  global $nid_update ;  $nid_update = 0 ;  // set when node updated/added

  if ($verb == "delete")
  {
    node_delete($node->nid); // force new node to be created
    $fragment = "<tr class='deletion'><td colspan=10>Item deleted</td></tr>";
    $json = drupal_json($fragment);
    print($json);
    exit(0);
  }

  if ($verb == "clone")
  {
    $node->cid = $node->nid ;
    unset($node->nid); // force new node to be created
  }

  if ($verb == "add")
  {
    $node->cid = $node->nid ;
    $node->reading = $node->enddate = null ;  // zero out key fields
    unset($node->nid); // force new node to be created
  }

  $output = drupal_get_form("carbon_editable_row_form", $node, $account, $tablename, "carbon_stamp_submit");

  // todo: presume that status messages might get stripped out which is not ideal!
  //       need to get them back to the user.

  // if we have just completed an update, discard the forms and render a new
  // row for the table using the same routine that created the original table.

  if ($nid_update > 0)
  {
    $singleRow = ($verb == 'add') ? 'insertion' : 'replacement' ;
    $output = carbon_view_stamp_one($account, $tablename, $nid_update, $singleRow);
  }
  // we want to use standard drupal form creation but you cannot use/add the
  // form tag to an existing page (at least that's what i found).
  // So the original page should already be wrapped in form tags.
  // So before injecting the html fragment, we extract the html that is wrapped
  // by the outside <TD>..col.1..col.2..col.3..</TD> tags.

  $fragment = extract_string($output, "<tr", "</tr>");

  $json = drupal_json($fragment);
  print($json);
  exit(0);
}

/**
 * Cut out a fragment of a string
 *
 * @param $haystack the target string
 * @param $start   start marker of desired string
 * @param $end     end marker of desired string (reverse search)
 * @return string fragment including the start and end markers
 */

function extract_string($haystack, $start, $end)
{
  $p1 = strpos($haystack, $start);
  if ($p1 !== false)
  {
    $p2 = strrpos($haystack, $end);
    if ($p2 !== false)
    {
      return substr($haystack, $p1, $p2 + strlen($end) - $p1);
    }
  }
  return t("<tr><td colspan=10>Error: Item not available, refresh page</td></tr>");
}


function carbon_editable_row_form($form_state, $node, $account, $tablename, $submit_hook)
{
  $form = array();
  $form["aid"] = array("#type"=>"hidden", "#name"=>"aid", "#value" => $account->nid);
  $form["uid"] = array("#type"=>"hidden", "#name"=>"uid", "#value" => $account->uid);

  if ($node != null)
  {
    if (isset($node->nid))
      $form["nid"] = array("#type"=>"hidden", "#name"=>"nid", "#value" => $node->nid);
    if (isset($node->cid))
      $form["cid"] = array("#type"=>"hidden", "#name"=>"cid", "#value" => $node->cid);
  }

  _stamp_form($form, $node, CarbonSources::getDefault());

  // pass $header structure to $form so cols can be rendered in correct order

  $get_header = "_get_".$tablename."_header";
  $form['#header'] = $get_header(); // adding non standard field

  $form['save'] = array("#type" => "submit", "#value" => "save");
//  $form['cancel'] = array("#type" => "submit", "#value" => "cancel");  //todo, put in op box
  $form['#submit'] = array($submit_hook, 'carbon_ajax_submit');
  return $form ;
}


/**
 *
 * Invoked by drupal's form routines to render a set of fields that will
 * evenutually be inserted in a table.  Drupal can render this form
 * without our help, however we want to create a set of minimal set of
 * fields without titles and descriptions and most importantly rendered
 * as a set of columns.
 *
 * @param $form  form that should be rendered
 * @return html string
 *
 */


function theme_carbon_editable_row_form(&$form)
{
  global $cols ; $cols = array();

  // first of all render the fields that are named
  // in the header

  foreach ($form['#header'] as $h)
  {
    $fieldname = isset($h['form']) ? $h['form'] : $h['field'] ;

    $field = &$form[$fieldname]; // $field is large array here
    if (!isset($field))
      $field = &$form['stamp'][$fieldname]; // $field is large array here

    if (is_array($field))
    {
      if ($field["#type"] == "hidden") // save hidden fields
        continue ;

      if ($field["#type"] == "submit") // save submit buttons
        continue;

      // prevent title and description being placed around field
      $fcopy = $field ;  // this should create new copy
      unset($fcopy['#title']) ;  unset($fcopy['#description']);

      $cols[] .=  drupal_render($fcopy);
    }
    else
      $cols[] = t("not avail.");

    $field['#printed'] = true ;  // mark as done
  }
  // put all the submit buttons in one column

  foreach($form as $key => $f)
  {
    if ($f["#type"] == "submit")
      $cols[sizeof($cols)-1] =  drupal_render($f) . " " ;
  }
  // at this stage, we have handled all the fields required
  // for the table.  But markup the other fields as hidden.

  _markFieldsAsHidden($form);

  // add the tokens, unique form identifier etc and render the rest of the form
  $other_fields = drupal_render($form); // the whole form has been rendered

  // shove the other fields in the last column,
  // we don't want to put an extra column into
  // the existing table

  if (sizeof($cols) == 0)  $cols[0] = "" ;
  $cols[sizeof($cols)-1] .= $other_fields ;

  // render form in a table with one row
  // we'll extract the signle row later on.

  $class = "replacement"  ;
  return theme_table(array(), array(array("data" => $cols, 'id'=> 0, 'class'=> $class)));
}


/**
 * We don't want to render all fields because there isn't room in each
 * row of the table so mark them as #hidden so they are not rendered
 * but the default values (like scope) are available for saving.
 *
 * @param $form
 * @return undefined
 */


function _markFieldsAsHidden(&$form)
{
  foreach($form as $key => &$f)
  {
    if (!is_array($f))
      continue ;    // never do anything with a single attribute

    if ($f["#printed"])          // skip rendered fields
      continue ;

    if ($f["#type"] == "hidden") // keep hidden fields
      continue ;

    if ($f["#type"] == "token")  // keep token fields
      continue ;

    if ($f['#type'] == 'fieldset')
    {
      _markFieldsAsHidden($form[$key]);
      $f['#printed'] = true ;
      continue ;
    }
    $f['#type'] = 'hidden' ;
  }
}



function form_render($col)
{
  if (is_array($col)) return $col[0];
  return $col ;
}

/**
 * create the links for different operations available on each line
 * on the table
 *
 * @param $account
 * @param $tablename
 * @param $node
 * @param $query
 * @return unknown_type
 */


function _format_link_insitu_edit($account, $tablename, $node, $query="")
{
  return _fancy_link($node,"", "edit stamp",
    "carbon/".$account->nid."/".$tablename."/edit/".$node->nid, $query, "edit").
         _fancy_link($node,"","copy stamp, attach to this account and then edit",
    "carbon/".$account->nid."/".$tablename."/clone/".$node->nid, $query, "clone").
         _fancy_link($node,"","delete stamp",
    "carbon/".$account->nid."/".$tablename."/delete/".$node->nid, $query, "delete") ;
}


function _format_link_insitu_add($account, $tablename, $node)
{
  $nid = isset($node) ? $node->nid : ""  ;
  return _fancy_link($node,t("New"), "add a new stamp",
    "carbon/".$account->nid."/".$tablename."/add/".$nid, "", "new");
}



?>