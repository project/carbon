/**
 *  
 * Drupal carbon module
 *  
 * Basic javascript functions
 */


Drupal.behaviors.carbon_scope = function () 
{	
  $("input:checked.scope").each(function () { 
    scope_change(this.value, "fast");
  });
  
  $('input:radio.scope').bind("change", function () {   
    scope_change(this.value, "slow") 
  }); 
}

/**
 * the function is used to swap the help text around on a carbon stamp form
 * as the user switches from one type (scope) to another. 
 * 
 * @param scope_id
 * @param speed
 * @return
 */ 
 
function scope_change(scope_id, speed) {

  if (speed == null)
    speed = "slow";

  switch (scope_id) {
    case "0":
      $("div.scope-0").show(speed);
      $("div.scope-1").hide(speed);
      $("div.scope-2").hide(speed);
      $("div.startdate").show(speed);
      break;

    case "1":
      $("div.scope-0").hide(speed);
      $("div.scope-1").show(speed);
      $("div.scope-2").hide(speed);
      $("div.startdate").hide(speed);
      break;

    case "2":
      $("div.scope-0").hide(speed);
      $("div.scope-1").hide(speed);
      $("div.scope-2").show(speed);
      $("div.startdate").show(speed);      
      break;
  }
}
