<?php
/**
 *
 * Drupal carbon module.
 *
 * Carbon transfers (purchases and sales of carbon) are recorded in
 * carbon_transfers nodes. The functions in this file manage the
 * database operations and reporting functions.
 *
 * Intended for (but not exclusively) CRAG use.
 *
 * Created on Feb 2008.
 *
 */


/**
 * Implementation of hook_access().
 *
 * Delegate carbon_access to the carbon module because it
 * is easier to check access for all carbon types in one place.
 */

function carbon_transfer_access($op, $node)
{
  return carbon_access($op, $node);
}


/**
 * Render a page listing a one line summary of all carbon transfers
 */


function carbon_transfer_page($account)
{
  global $user;

  require_once(CARBON_PATH ."/carbon_report.inc");  // has CarbonAccountMap

  $aid = empty($account) ? null : $account->nid ;


  $account_map = new CarbonAccountMap() ; $account_map->init();

  // the nid's below correspond to carbon accounts

  $sql = "SELECT  t.*, n.*, u.name, u.uid " .
         "FROM {carbon_transfer} t " .
         "LEFT JOIN {node} n ON t.nid = n.nid " .
         "INNER JOIN {users} u on n.uid = u.uid "  ;

  if (empty($aid))
  {
    $header = carbon_transfer_admin_header();
  }
  else {
    $header = carbon_transfer_header();
    $sql .= " WHERE t.seller=%d OR  t.buyer=%d ";
  }

  $sql .= tablesort_sql($header);

  // get the links (no range limit here)
  $queryResult = db_query($sql, $aid, $aid);

  $transfers = array();
  while ($node = db_fetch_object($queryResult))
  {
    $node->protection = _log2($node->protect2);
    $ro_access = carbon_access("view", $node, $node->uid);
    if ($ro_access)
    {
      $transfers[] = $node ;
    }
  }
  $output .= _create_transfer_table($account, "transfer", $header, $transfers, $singleRow = false, $account_map);


  $format = $_GET["datatype"];
  if ($format == 'json')
  {
    $json = drupal_json($output);
    print($json);
    exit(0);
  }

  if (empty($aid))
  {
    $title = t("All Carbon Purchases");
  }
  else
  {
    $title = t("Cash Movements for ". $account_map->getTitle($aid));
  }

  drupal_set_title(check_plain($title));
  return $output;

}


function _create_transfer_table($account, $pagename, $header, $transfers, $singleRow, $account_map)
{
  $rows = array() ;  // updated when form created
  $balance = array();

  foreach ($transfers as $transfer)
  {
    // we have to format each cell because there may be embedded links.

    $cols = _format_transfer_cells($header, $transfer, $account, $class = $pagename, $account_map, $balance);
    $rows[] = array('data' => $cols, 'id'=> $transfer->nid, 'class'=> empty($singleRow) ? 'editable' : $singleRow) ;
    $last = $transfer ;
  }

  if (empty($singleRow))
  {
    $cols = array();  $width = count($header);
    for ($i = 0 ; $i < $width - 1 ; $i++)
      $cols[] = "" ;
//    $cols[] = _format_link_insitu_add($account, $pagename, $last);
    $rows[] = array("data" => $cols, 'id'=> 0, 'class'=>'inSituEdit', 'class'=>'addable');
  }
  $output = theme("table_inside_form", $singleRow ? array() : $header, $rows);

  return $output ;
}


function _format_transfer_cells($header, $transfer, $account, $class, $account_map, &$balance )
{
  $cols = array();
  foreach ($header as $h)
  {
    switch($f = $h['field'])
    {
      case  'valuedate' :$cols[] = _format_date($transfer->valuedate); break ;
      case  'title' :    $cols[] = carbon_link_from_title($transfer, $account, ""); break ;

      case  'buyer' :  $cols[] = $account_map->getTransferLink($transfer->buyer); break ;
      case  'seller' : $cols[] = $account_map->getTransferLink($transfer->seller); break ;
      case  'counterparty' :

        if ($transfer->seller == $account->nid)
        {
          $cols[] = $account_map->getTransferLink($transfer->buyer); break ;
        }
        else
        {
          $cols[] = $account_map->getTransferLink($transfer->seller); break ;
        }

      case  'carbon' : $cols[] = $transfer->carbon ;  break ;
      case  'carbon-balance' :
        if ($transfer->seller == $account->nid)
        {
          $cols[] = $balance['carbon'] -= $transfer->carbon ;  break ;
        }
        else
        {
          $cols[] = $balance['carbon'] += $transfer->carbon ;  break ;
        }

      case  'cost' :   $cols[] = $transfer->cost ;   break ;
      case  'cost-balance' :
        if ($transfer->seller == $account->nid)
        {
          $cols[] = $balance['cost'] += $transfer->cost ;  break ;
        }
        else
        {
          $cols[] = $balance['cost'] -= $transfer->cost ;  break ;
        }

      case  'status' :   $cols[] = ($transfer->status >= 1 ? 'pub ' : ' '); break ;
      case  'operations':$cols[] = _format_link_insitu_edit($account, $class, $transfer); break ;
      default :          $cols[] = $f." ?" ;  break ;
    }
  }
  return $cols ;
}


function carbon_transfer_header()
{
  global $carbon_transfer_header ;
  if (!isset($carbon_transfer_header))
  {
    $carbon_transfer_header = array(
    array("data" => t("date"),           "field" => "valuedate"),
    array("data" => t("description"),    "field" => "title"),
    array("data" => t("counterparty"),   "field" => "counterparty"),

    array("data" => variable_get("carbon_units", "Kg of C02"), "field" => "carbon"),
    array("data" => t("CO2 balance"),    "field" => "carbon-balance"),

    array("data" => variable_get("carbon_money_units", "$"),  "field" => "cost"),
    array("data" => t("balance"),        "field" => "cost-balance")
    );
  }
  return $carbon_transfer_header ;
}



function carbon_transfer_admin_header()
{
  global $carbon_transfer_admin_header ;
  if (!isset($carbon_transfer_admin_header))
  {
    $carbon_transfer_admin_header = array(
    array("data" => t("date"),           "field" => "valuedate"),
    array("data" => t("description"),    "field" => "title"),
    array("data" => t("seller"),         "field" => "seller"),
    array("data" => t("buyer"),          "field" => "buyer"),
    array("data" => t("CO2"),            "field" => "carbon"),
    array("data" => t("cost"),           "field" => "cost")
    );
  }
  return $carbon_transfer_admin_header ;
}

/**
 *
 * Joins the recorded stamps in a carbon account with all the available carbon
 * sources.
 *
 * Note that some source fields have been renamed e.g. sourceid, sourceclass, sourcetitle
 *
 *@param $nid  select stamps attached to this footprint id, can be null
 *@param $uid  select stamps with this uid, can be null
 *@param $org  select stamps with this org, can be null
 *@return an array of carbon stamps (and an array of carbon sources)
 */


function carbon_transfer_array_load($nid, $uid, $org)
{
	$query ="SELECT  ntransfer.title, ntransfer.status, ntransfer.uid, ntransfer.type, " .
    "transfer.* AS shared_uid " .
    "FROM {carbon_transfer} transfer " .
    "INNER JOIN {node} ntransfer ON  transfer.nid = ntransfer.nid ".
    "WHERE   ntransfer.status " ;          // transfer must be published

 //   "AND (j.fid = %d  " .                 (empty($nid) ?  " OR 1) " : ") ").
 //   "AND (ntransfer.uid = %d " .          (empty($uid) ?  " OR 1) " : ") ").
 //   "AND (account.organization = '%s' " . (empty($org) ?  " OR 1) " : ") ").   "" ;

  if (!empty($nid))
  {
    $query .= "AND  (transfer.seller=%d OR transfer.buyer=%d) ";
  }
  $queryResult =  db_query($query, $nid, $nid);   // needs fixing
//  $query .= tablesort_sql(carbon_transfer_header());

  $transfers = array();
  $num = 0 ;
	while ($record = db_fetch_object($queryResult))
	{
    $transfers[] = $record ;
//    echo $record->title . " bb <br/>" . $query;
	}
	return array("transfer" => $transfers);
}


function carbon_transfer_form(&$node, $form_state) {
	global $user ;

	require_once(CARBON_PATH ."/carbon_report.inc");   // has CarbonAccountMap

  $account_map = new CarbonAccountMap();  $account_map->init();
  $form = array();

    $form["title"] = array(
      "#type" => "textfield",
      "#title" => "description",
      "#required" => true,
      "#default_value" => $node->title,
      "#size" => 10,
      "#maxlength" => 40,
      "#weight" => -8,
      "#description" => "purpose or reason for transfer");


    $form["seller_index"] = array(
      "#type" => "select",
      "#options" => $account_map->getArray(),
      "#title" => "seller",
      "#required" => true,
      "#default_value" => $account_map->getArrayIndex($node->seller),
      "#size" => 10,
      "#maxlength" => 4,
      "#weight" => -7,
      "#description" => "carbon account to debit");

    $form["buyer_index"] = array(
      "#type" => "select",
      "#options" =>  $account_map->getArray(),
      "#title" => "buyer",
      "#required" => true,
      "#default_value" =>  $account_map->getArrayIndex($node->buyer),
      "#size" => 10,
      "#maxlength" => 4,
      "#weight" => -6,
      "#description" => "carbon account to credit");

    $form["carbon"] = array(
      "#type" => "textfield",
      "#title" => "carbon",
      "#required" => true,
      "#default_value" =>  $node->carbon,
      "#size" => 20,
      "#maxlength" => 20,
      "#weight" => -5,
      "#description" => "amount of carbon credit");  // don't specify units'

    $form["cost"] = array(
      "#type" => "textfield",
      "#title" => "cost",
      "#required" => true,
      "#default_value" =>  $node->cost,
      "#size" => 20,
      "#maxlength" => 20,
      "#weight" => -4,
      "#description" => "monetary value of transaction");

//    $firstdate = mktime(0, 0, 0, substr($params["firstdate"],4,2), substr($params["firstdate"],6,2), substr($params["firstdate"],0,4));

    $form["valuedate_s"] = array(
      "#type" => "textfield",
      "#title" => "value date",
      "#required" => true,
      "#default_value" =>  _format_date($node->valuedate == 0 ? CarbonDate::today() : $node->valuedate),
      "#size" => 20,
      "#maxlength" => 20,
      "#weight" => -3,
      "#description" => t("Effective date of transfer, format: %time", array("%time" => _format_date_only(time())))
    );
    $form['#submit'] = array('carbon_transfer_submit');

  return $form ;
}



/**
 * Validate our forms
 * @param string form id
 * @param array form values
 */
function carbon_transfer_validate($form_id, $form_values)
{
/*
	for ($i = 0 ; $i < 500 ; $i++)
  {
	  _validate_adjustment($form_id, "ff". $i .".adjustment");
    _validate_date($form_id, "ff". $i .".startdate_s");
    _validate_date($form_id, "ff". $i .".enddate_s");
//    _validate_date($form_id, "ff". $i .".firstdate");
  }
*/
}


/**
 * Implementation of hook_load
 */


function carbon_transfer_load(&$node)  {

	$query = db_query(
	"SELECT t.* " .
	"FROM {carbon_transfer} t " .
  "LEFT  JOIN {carbon_stamp_account} j ON t.nid = j.sid " .
  "LEFT  JOIN {node}                 n ON j.fid = n.nid " . // to get footprint title
	"WHERE t.nid = %d " .
	" ", $node->nid);
  $first_record = null ;

  while ($first = db_fetch_object($query))
  {
    return $first ;
  }
  drupal_set_message("Failed to load carbon_transfer where nid=", $node->nid);
}


function carbon_transfer_submit(&$form, &$form_state)
{
  require_once(CARBON_PATH ."/carbon_report.inc");   // has CarbonAccountMap

  $account_map = new CarbonAccountMap();  $account_map->init();

  $transfer = &$form_state['values'];

  $transfer['seller'] = $account_map->getNid($transfer['seller_index']) ;
  $transfer['buyer'] = $account_map->getNid($transfer['buyer_index']) ;

  $s = &$transfer['valuedate_s'] ;
  $transfer['valuedate'] = mktime(0, 0, 0, substr($s,4,2),  // should use drupal routines
                                     substr($s,6,2),
                                     substr($s,0,4));

//drupal_set_message("ghi " . $node->seller . " ... " . $node->buyer_index) ;
}

/**
 * Implementation of hook_insert, which saves carbon-specific information
 * into the carbon table
 * @param node object
 */

function carbon_transfer_insert(&$node)
{
	db_query("INSERT INTO {carbon_transfer} ".
	"(nid, seller, buyer, carbon, cost, valuedate) " .
	"VALUES (%d, %d, %d, %f, %f, %d)",
	$node->nid, $node->seller, $node->buyer, $node->carbon, $node->cost, $node->valuedate);
}



/**
 * Implementation of hook_update, which saves updated todo-specific
 * information into the todo table
 * @param node object
 */
function carbon_transfer_update(&$node)
{
//  drupal_set_message("def " . $node->seller . " ... " . $node->seller_index) ;
  db_query("UPDATE {carbon_transfer} set seller=%d, buyer=%d, carbon=%f, cost=%f, valuedate=%d " .
    		   " WHERE nid=%d",
	$node->seller, $node->buyer, $node->carbon, $node->cost, $node->valuedate,
 	$node->nid);
}


/**
 * Implementation of hook_delete()
 */
function carbon_transfer_delete($node) {
	db_query("DELETE FROM {carbon_stamp_account} WHERE sid = %d", $node->nid);
	db_query("DELETE FROM {carbon_transfer}      WHERE nid = %d", $node->nid);
}


/**
 * Implementation of hook_view, add our node specific information
 * @param node object to display
 * @param boolean is this a teaser or full node?
 * @param boolean is this displaying on its own page
 *
 */
function carbon_transfer_view(&$node, $teaser = FALSE, $page = FALSE)
{
  $node = node_prepare($node, $teaser);
  $carbon_account_info = theme('carbon_transfer_table_view', $node);
  $node->content["transfer"] = array("#value" => $carbon_account_info);
  $node->body .= $carbon_account_info;
  $node->teaser .= $carbon_account_info;
  return $node ;
}



function theme_carbon_transfer_table_view($node)
{
	// content variable that will be returned for display
  $header = array("valuedate", "description", "seller", "buyer", "carbon", "cost", "published");

  $account_map = new CarbonAccountMap() ; $account_map->init();

  $rows = array();

	$rows[] = array(_format_date($node->valuedate),
									$node->title,
                  $account_map->getAccountLink($node->seller),
                  $account_map->getAccountLink($node->buyer),
                  $node->carbon,
                  $node->cost,
                  $node->status ? "Yes" : "No");

	$output = theme("table", $header, $rows);

  return "<h1></h1>".$output;  // the H1s make bluebreeze theme work
}


class CarbonTransaction
{
  var $title ;    // used for references and descriptive text
	var $seller ;
	var $buyer ;
	var $carbon ;
	var $cost ;
	var $valuedate	;

	function initialize()
	{
		global $user ;
    $this->type = 'carbon_transfer' ;
    $this->status = 1 ;  // published
    $this->promote = 0 ;
    $this->sticky = 0;
    $this->body = null ;
    $this->comment = 1 ;
    $this->uid = $user->uid ;
    $this->teaser = "" ;
    $this->body = "" ;
	}

	function toString($account_map)
	{
    $carbon = sprintf("%.2f", $this->carbon);
    $cost = sprintf("%.2f", $this->cost);
    return $account_map[$this->seller]->title.
		" sold to ".$account_map[$this->buyer]->title.
		", ".$carbon. " carbon credits".
		" at a cost of ".$cost;
	}
}


?>