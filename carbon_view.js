 *  
 * javascript functions that support ajax loading and editing.
 * 
 * carbon_debug.js also required.
 */

var formInUse = false; 
var formOptions = null ;
var formLoad = 0 ;

Drupal.behaviors.carbon_view = function () 
{	
  // initialize elements on page that can be expanded 

  var numLoadable = 0 ;
  $('A.loadOnDemand').each(function () {	   		         	    	      
    $(this).click(function() { carbonRequestData(this); return false }); // must return to suppress link click  
    numLoadable++ ; 
  })  // A tag 	

  // override onclick() event handlers on edit,clone,add 
  // buttons but only on the forms that we created
    
  var numEditable = 0 ;
  $('form.ajaj a.edit, form.ajaj a.clone, form.ajaj a.delete,form.ajaj a.new').each(function () { 		
    $(this).click(function() { carbonRequestForm(this); return false }); // must return to suppress link click  
    numEditable++ ; 
  })  // A tag 	 
  log("The page has " + numLoadable + " loadable elements and " + numEditable + " editable elements");    
}
 
/**
 * Need to merge this function with above function
 * but could wait for jQuery 1.3 
 * 
 * @param element
 * @return
 */

function carbonAddHandler(element)
{
  var numEditable = 0 ;
  $(element).find("a.edit, a.clone, a.delete, a.add").each(function () { 
    this.href += "&datatype=json" ;
    $(this).click(function() { carbonRequestForm(this); return false }); // must return to suppress link click  
    numEditable++ ; 
  })  // A tag     
  log("The page has " + numEditable + " new editable elements");   
}


/** 
 * HTML block manipulation functions
 */

/**
 * Respond to a link clicked on by a user
 * @param id  unique id for each panel on the page
 * @param url
 * @return
 */

function carbonRequestData(link)
{
  url = link.href + "&datatype=json" ;
  
  // look around for the loadingZone close to the link in the DOM 
  target = $(link).parent().find('.loadingZone');
  
  // then look further afield
  if (target.length < 1)   
    target = $(link).parent().parent().find('.loadingZone');    
    
  if (target.length < 1) 
  {
    alert("Unable to locate 'loadingZone' on page");
    return ; 
  }

  fieldset = $(link).parent().parent().filter('fieldset');

  // Fieldsets. The fieldset is loaded once when it is first opened. 
  // When the fieldset is closed and opened the original unrefreshed 
  // data is put back on the screen. 
  
  if (fieldset.length > 0)
  {
    if ($(link).hasClass('collapsed'))  // fieldset is closed, do nothing
      return ;
    if ($(link).hasClass('loaded'))     // data already loaded, do nothing
      return ;
    $(target).html("<a class='loading' href='"+ link.href + "'>loading...</a>").removeAttr("style");
    log("requesting element:" + link.href);   
    jQuery.getJSON(url, { datatype: "json"} , function(fragment) { carbonAcceptData(link, target, fragment); }) ;
    return ;
  }
  
  if ($(link).hasClass('loaded') == false)
  {
    $(target).html("<a class='loading' href='"+ link.href + "'>loading...</a>").removeAttr("style");
    log("requesting element:" + link.href); 	
  	jQuery.getJSON(url, { datatype: "json"} , function(fragment) { carbonAcceptData(link, target, fragment); }) ;	
  }
  else
  {	 
    log("closing") 
    $(link).removeClass('loaded')
    $(target).hide("slow");
  }
  return false ; 
}

/**
 * Respond to a callback from the browser to receive data from the host
 * 
 * @param id    unique id for each panel on the page
 * @param url
 * @param body  
 * @param msg   replaace HTML
 * @return
 */


function carbonAcceptData(link, target, fragment)
{  
  $(link).addClass("loaded");
  $(target).html(fragment) ; 
  log("accepted element:" + link.href);
}


/**
 * replace the readonly row in the table with a loading 
 * message and fetch the form.
 */


function carbonRequestForm(elem)
{    
  elem.href += "&datatype=json" ;  
  rowClicked = $(elem).parents('tr.editable, tr.addable');
 
  if (rowClicked.length != 1) { alert("Error: Found " + rowClicked.length + " editable rows in table"); return ; }
  
  form = rowClicked.parents('form');  

  if (formInUse == true)
  {
    alert("Please refresh page before editing or adding more items");
    return false ;
  }
  
  if ($(elem).hasClass("delete"))
  { 
    // title assumed to be first column 
    title = $(rowClicked).children(":first").text();
  	response = confirm("Delete " + title + "?");
	  if (response == false)  // ignore delete click
	    return true ; 
  }
  
  log(elem.id + ":requesting form:" + elem.href); 	  
  
  loadingMessage = "<tr><td class='loading' colspan=10>loading...</td></tr>" ;

  $(rowClicked).after(loadingMessage);
  
  rowLoading = $(rowClicked).next();  
 
  if (rowLoading.length != 1) { alert("Error: Found " + rowLoading.length + " loading rows in table"); return ; }
  
  // if we request a clone, leave the existing row 
  // in the table. For add and edit, remove this row,
  // we will overwrite it when we save the node.
  
  if (elem.className != "clone")
  {
    $(rowClicked).remove();
  }
  
  jQuery.getJSON(elem.href, 
    { datatype: "json" } , 
    function(fragment) 
    { 
      log (elem.id + ":accepting form:" + elem.href);	
      carbonAcceptForm(elem.id, elem.href, rowLoading, fragment, form); 
    }) ;	

}

/**
 * We have got the form as HTML code. Slot it back into 
 * table where the loading message is on display. 
 */


function carbonAcceptForm(id, url, rowLoading, fragment, form)
{
  formInUse = true ; 
  
  // insert new row and remove old 'loading' row   
  
  $(rowLoading).after(fragment);  

  rowInserted = $(rowLoading).next();  $(rowLoading).remove();

  if (rowInserted.length != 1) { alert("Error: Found " + rowInserted.length + " inserted rows in table"); return ; }
  
  if ($(rowInserted).filter('.deletion').length == 1) 
  {
    formInUse = false ; 
    return ; 
  }
  carbonEnableSubmit(id, url, rowInserted, form);
}


function carbonEnableSubmit(id, url, element, form)
{  
  if (formLoad == 0)
  {
    form.submit(function()    // attach handler to form's submit event 
    {     
      $(this).ajaxSubmit(formOptions); 
      return false;          // prevents normal browser submission.
    });
  }
//  alert(formOptions.url);

  formOptions = {          // information http://malsup.com/jquery/form/#options-object
    url : url,				     // use the edit url, not the table url
    complete: function(response, status) {
      if (status == 'error' || status == 'parsererror') {
    	  alert(status)
      }
      else
      { 
    	  fragment = eval(response.responseText);
    	  carbonRestoreRow(id, url, element, fragment, form);
      }    	  
    }, 
    
    type:      'post',       // override for form's default 'method' attribute 	
    dataType:  'json',       // 'xml', 'script', or 'json' (expected server response type)   
    
//	target:    arRow,        // target element(s) to be updated with server response       
//	beforeSubmit:  showRequest,  // pre-submit callback 
//  success:       showResponse, // post-submit callback 

    clearForm: false,       // clear all form fields after successful submit 
    numLoad : ++formLoad 
  };
}

/**
 * Update the row in the table. The difficult bit is deciding whether 
 * this is an update or an insertion. 
 * 
 *  edit         clone        add (failed)     add (success)
 *  
 *  replace      replace      replace          insert row          
 *  
 * @param id
 * @param url
 * @param rowWithForm
 * @param fragment
 * @param form
 * @return
 */


function carbonRestoreRow(id, url, rowWithForm, fragment, form)
{    
  log (id + ":restoring row4:" + url);
  
//  logJQueryObject("rWF", rowWithForm);
  
  log()
  
  try {
    // insert the new row above the form
  	rowUpdated = $(rowWithForm).before(fragment).prev();
	
    rowUpdated.addClass("highlight");
  	// has server provided an insertion or replacement?
  	// could use hasClass but want to make sure only 1 row
 	
  	numInsertion = $(rowUpdated).filter(".insertion").length;
    numReplacement = $(rowUpdated).filter(".replacement").length;
    
    logMsg = "Got " + numInsertion + " insertion and " + numReplacement + " replacement, expected total of one." ;
  	if (numInsertion + numReplacement != 1)
  	  throw(logMsg);
  }
  catch (error) {
    alert("Unable to add row : " + error);
    return ; 
  }  
  
  try 
  {
  	if (numInsertion == 0) 
  	{
  	  $(rowUpdated).removeClass('replacement').addClass('editable');
  	  $(rowWithForm).remove();
      
  	  carbonEnableSubmit(id, url, rowUpdated, form);   
      // enable edit, clone buttons
      carbonAddHandler(rowUpdated);  
      
      // find out if input fields are on new row
      numInput = $(rowUpdated).find("input").length ; 
  	  if (numInput == 0)   // if not, ok to start editing elesewhere.
  	    formInUse = false ; 
  	}
  	else 
    {
      $(rowUpdated).removeClass('insertion').addClass('editable');

      carbonEnableSubmit(id, url, rowWithForm, form);
      carbonAddHandler(rowWithForm); 
    }  	
  	

 
  }
  catch (error) {
	  alert("Unable to remove row or alter class : " + error)
  }
}
