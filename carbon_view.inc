<?php
/*
 *
 * Created on 19-Feb-2008
 *
 *
 * Functions in this file create the first tabbed pane that
 * shows the carbon _account status.
 *
 * Javascript to handle client side form modification
 * It relies on JQuery which is now part of the drupal core
 * and carbon_debug.js;
 */



/**
 * Menu entry point to display different pages
 *
 * @param $account
 * @param $pagename   'meter' | 'travel'
 * @return unknown_type
 */


function carbon_view($account, $pagename)
{
  // load the basic javascript routines
  $path = drupal_get_path("module", 'carbon');
  // submit a form via ajax
  drupal_add_js('misc/jquery.form.js', 'core', 'header', FALSE, TRUE);
  $path = carbon_include_core_js();  // need the .css
  drupal_add_js($path . "/carbon_debug.js", "module");
  drupal_add_js($path . "/carbon_view.js", "module");

  // should be removed by javascript
  $output = "<div id='nojavascript'>Javascript not working or not enabled.</div>";

  // are we previewing a new account, then don't try to recall account activity
  if (empty($account->nid))  return $output ;

  require_once(CARBON_PATH ."/carbon_chart.inc");    // generate bar and pie PNGs
  require_once(CARBON_PATH ."/carbon_calculate.inc");// co2 & kWh settings

  switch ($pagename)
  {

    case 'meter' :
    case 'travel' :
        $output .= carbon_view_stamp_one($account, $tablename = $pagename, null);
        break ;

    default :
        $output = _view_historic_chart($account, null, null);
        $output .= _buildPeriodPanels($account);
        if (variable_get("carbon_enable_crag_functions", 0))
          $output .= theme("carbon_loadable_box", "Carbon Trades", "?q=carbon/".$account->nid."/transfer");
        break ;
  }
  return $output ;
}



//http://chart.apis.google.com/chart?chs=450x150&chd=t:10,20,30|10,9,30|178,87,90&cht=bhs&chl=Hello|World|basic&chco=ff0000,00ff00,0000ff&chds=5,500&chdl=heating|elec|gas&chxp=alison|doug|chris
/**
 * Render an image showing multiple carbon footprints
 *
 * Note that the param $uid is the user we are focused on, not the user doing
 * the browsing.
 *
 * @param $account_map an array of $nid => $footprint
 * @param $uid select by uid; set to zero to retrieve footprints from all users
 * @param $org select by organisation; set to '' or null to retrieve footprints
 * from users in any group.
 */

function _view_historic_chart($account, $uid, $org){
  global $user;
  // for each item in the map, change the key and value to title and amount ;

//  if (!carbon_chart_requested())
  //  return "" ;

  $footprint_by_year = array();  $legend = array();

  $stamps = carbon_stamp_array_load($account->nid, $uid, $org);

  $dateRange = _stamp_date_range($stamps);

  $reportDates = carbon_calculate_reporting_dates($dateRange, $account->firstdate, $account->period);

  for ($i = count($reportDates) - 2 ;  $i >= 0  ; $i--)
  {
    $results = carbon_calculate_total($account, $stamps, $reportDates[$i], $reportDates[$i+1]);

    $firstdate_s = CarbonDate::asYYYYMMDD($reportDates[$i]);
    $footprint_by_year[$reportDates[$i]] = $results->sector_co2;
    $legend[$reportDates[$i]] = _format_date_range($reportDates[$i], $reportDates[$i+1]);
  }
  $chart = carbon_chart_stacked_bar("Historic carbon emissions", $footprint_by_year, $legend, "Kg of CO2");

  return count($footprint_by_year)==0  ? "" : theme("carbon_static_box", "Chart", $chart);
}


/**
 * Add a link for every accounting period for which there is carbon data
 * @param $aid
 * @return unknown_type
 */
function _buildPeriodPanels($account)
{
  $account->stamp = carbon_stamp_array_load($account->nid, null, null);

	if (count($account->stamp) == 0)
    return carbon_calculate_empty_table($account);

  // find out the range of dates that we are dealing with
  $dateRange = _stamp_date_range($account->stamp);

  // then work out the dates for each footprint
  $reportDates = carbon_calculate_reporting_dates($dateRange, $account->firstdate, $account->period);

  $header = carbon_stamp_header();

  for ($i = count($reportDates) - 2 ;  $i >= 0  ; $i--)
  {
    $firstdate_s = CarbonDate::asYYYYMMDD($reportDates[$i]);

    $annual_report = "" ;

    $sector_q  = sprintf("q=carbon/report/&report=sector&firstdate=%s&period=%d&org=%s",
      $firstdate_s,$account->period,$account->organization);

    $results = carbon_calculate_total($account, $account->stamp, $reportDates[$i], $reportDates[$i+1]);

    $annual_report .=  carbon_calculate_present_results($results);

    if (isset($account->organization))
    {
      $annual_report .= theme("carbon_loadable_box",
                          $account->organization . " sector report", url("", array("query"=>$sector_q)));
    }
	  $output .= theme("carbon_static_box",
	    t("Emissions from "). _format_date_range($reportDates[$i], $reportDates[$i+1]), $annual_report);

  }
  return $output ;
}


/**
 *
 * @param $account
 * @param $tablename
 * @param $nid    if specified, restrict table 1 stamp presented in 1 row
 * @return html
 */


function carbon_view_stamp_one($account, $tablename, $nid, $singleRow = "")
{
  $meterPage = $tablename == "meter" ? true : false ;

  $sql = carbon_stamp_query($account->nid, null, $nid, $meterPage);
  $get_header = "_get_".$tablename."_header";
  $sql .= tablesort_sql($header = $get_header());

  // get the links (no range limit here)
  $queryResult = db_query($sql);

  $stamps = array();

  $titles = array();
  while ($node = db_fetch_object($queryResult))
  {
  	$node->protection = _log2($node->protect2);
    $ro_access = carbon_access("view", $node);
    if ($ro_access)
    {
      $stamps[] = $node ;
    }
  }

  $output = $meterPage ? _meter_create_table($account, $tablename, $header, $stamps, $singleRow)
                       : _stamp_create_table($account, $tablename, $header, $stamps, $singleRow);

  return $output ;
}

/**
 * return html for table of meter readings
 *
 * @param $stamps   array of meter readings
 * @param $titles   array of titles; each represents a a different meter
 * @return html
 */


function _meter_create_table($account, $tablename, $header, $stamps, $singleRow)
{
	$output = '' ;
  $header = _get_meter_header();

  $titles = array();
  foreach ($stamps as $stamp)
    $titles[$stamp->title] = "";

  $unique_title = array_keys($titles);  // establish the meter names

  // for each unique meter name
  $output = "" ;
  foreach ($unique_title as $title)
  {
	  $selectedStamps = array();
	  foreach($stamps as $stamp)
	  {
	    if ($stamp->title == $title)
	     $selectedStamps[] = $stamp ;
	  }
	  $meter_table = _stamp_create_table($account, $tablename, $header, $selectedStamps, $singleRow);
	  $output .=   theme("carbon_static_box", $title, $meter_table);
  }

  $attributes = array('title'=>'Add first reading for a meter not listed above. ',
                      'class'=>'new-stamp');
  $meter_table = l("New meter", "node/add/carbon-stamp",
                              $options = array('attributes'=>$attributes,
                              'query' => 'destination=node/'.$account->nid.'/meter&scope=1',
                              'html'=>true))."<br/>" ;

  $output .= theme("carbon_static_box", "", $meter_table, $class='not-collapsible');

  return $output ;
}


/**
 * return html for table of stamps (NOT meters)
 *
 * @param $stamps   array of stamps
 * @return html
 */


function _stamp_create_table($account, $pagename, $header, $stamps, $singleRow)
{
  $rows = array() ;  // updated when form created
  foreach ($stamps as $stamp)
  {
    // we have to format each cell because there may be embedded links.

    $cols = _format_stamp_cells($header, $stamp, $account, $class = $pagename);
    $rows[] = array("data" => $cols, 'id'=> $stamp->nid, 'class'=> empty($singleRow) ? 'editable' : $singleRow) ;
    $lastStamp = $stamp ;
  }

  if (empty($singleRow))
  {
    $cols = array();  $width = count($header);
    for ($i = 0 ; $i < $width - 1 ; $i++)
      $cols[] = "" ;
    $cols[] = carbon_access("update", $account) ?
              _format_link_insitu_add($account, $pagename, $lastStamp) : "" ;
    $rows[] = array("data" => $cols, 'id'=> 0, 'class'=>'inSituEdit', 'class'=>'addable');
  }
  $output = theme("table_inside_form", $singleRow ? array() : $header, $rows);

  return $output ;
}

/**
 *
 * @param $header   defines each column for the desired view
 * @param $stamp    single stamp
 * @param $account  $account->nid needed for links
 * @param $class
 * @return unknown_type
 */

function _format_stamp_cells($header, $stamp, $account, $class )
{
  $cols = array();
  foreach ($header as $h)
  {
    switch($col = empty($h['col']) ? $h['field'] : $h['col'])
    {
      case  'title' :    $cols[] = carbon_link_from_title($stamp, $account, ""); break ;
      case  'startdate' :$cols[] = _format_date($stamp->startdate); break ;
      case  'enddate' :  $cols[] = _format_date($stamp->enddate); break ;
      case  'datetime':  $cols[] = _format_date_time($stamp->enddate); break ;
      case  'reading' :  $cols[] = $stamp->reading; break ;   // right adjust
      case  'code' :     $cols[] = $stamp->code; break ;
/*
        if (isset($emission->units))
        $units = is_numeric($emission->units) ? sprintf("%.0f",$emission->units) : $emission->units ;
        else
        $units = $emission->end->amount ; // is this line used?

        $model = $show_model ? $emission->end->sourcemodel."/" : "" ;
        $units_col =  isset($units) ? $units." ".$emission->end->units." of " : "" ;

        $units_col .= l($model.$emission->end->sourcetitle, 'node/'.$emission->end->sourceid);
  */
      case  'adjustment':$cols[] = $stamp->adjustment . "&nbsp;". ( $stamp->scope == 2 ? "R" : "" ) ; break ;
      case  'status' :   $cols[] = ($stamp->status >= 1 ? 'pub ' : ' ') . $stamp->protection ; break ;
      case  'operations':$cols[] =  carbon_access("update", $account) ?
                                    _format_link_insitu_edit($account, $class, $stamp) : "" ; break ;
      default :          $cols[] = $f." ?" ;  break ;
    }
  }
  return $cols ;
}



function _get_meter_header()
{
  global $carbon_meter_header ; // create once
  if (!isset($carbon_meter_header))
  {
    $carbon_meter_header = array(
      //              visible name,  database column name,   key in form table,    column name
      array("data" => t("date"),    "field" => "enddate",   "form" => "enddate_s", "col" => "datetime", "sort" => "asc"),
      array("data" => t("reading"), "field" => "reading"),   // todo need to distinguish between 2 vals
      array("data" => t("code"),    "field" => "code",      "form" => "code_index"),
      array("data" => t("adjust"),  "field" => "adjustment"),
//      array("data" => t("status"),  "field" => "status"),
      array("data" => t("operations"),"field" => "operations"));
  }
  return $carbon_meter_header ;
}


function _get_travel_header()
{
  global $carbon_travel_header ;  // create once
  if (!isset($carbon_travel_header))
  {
    $carbon_travel_header = array(
      //              visible name,  database column name,   key in form table
      array("data" => t("description"),   "field" => "title"  ),
      array("data" => t("start"),   "field" => "startdate", "form" => "startdate_s"),
      array("data" => t("end"),     "field" => "enddate",   "form" => "enddate_s",  "sort" => "asc"),
      array("data" => t("amount"),  "field" => "reading"),
      array("data" => t("code"),    "field" => "code",      "form" => "code_index"),
      array("data" => t("adjust"),  "field" => "adjustment"),
//      array("data" => t("status"),  "field" => "status"),
      array("data" => t("operations"),"field" => "operations"));
      // the long length of 'operations' forces the icons to be on one row.
  }
  return $carbon_travel_header ;
}


/**
 * Filter out fields that aren't explicitly specified
 * @param $form
 * @param $fieldset
 * @return unknown_type
 */

function _filter_fields($form, $fieldset)
{
  foreach($form as $key => $f)
  {
    if (!is_array($f))
      continue ;
    if ($f["#type"] == "hidden") // keep hidden fields
      continue ;
    if ($f["#type"] == "token") // keep token fields
      continue ;
    if (in_array($key,$fieldset))
      continue;               // this field is in the list
    unset($form[$key]);       // don't recognise it, get rid of it.
  }
  return $form ;
}



/**
 * The submit handler invoked by drupal to add/update a carbon stamp
 *
 * @param $form
 * @param $form_state
 * @return unknown_type
 */

function carbon_ajax_submit(&$form, &$form_state)
{
  $nid = $form_state['values']['nid'];
  $cid = $form_state['values']['cid'];    // set if we are cloning another node
  $uid = $form_state['values']['uid'];
  $aid = $form_state['values']['aid'];

  if (empty($nid) && empty($cid))
  {
    $node = new Source();   // todo - use something else
    $node->status = 1 ;     // published
    $node->promote = 0 ;
    $node->sticky = 0;
    $node->body = null ;
    $node->comment = 0 ;    // not permitted
    $node->uid = $uid ;
    $node->teaser = "" ;
    $node->body = "" ;
    $node->type = "carbon_stamp" ;  // todo no hard coding
    $node->scope = 0 ;
    // attach stamp to the account that we are looking at
    $node->aid_map = array();
    $node->aid_map_updated = array($aid => null);  // attach a new stamp to this account
    $op = "saved" ;
  }
  else
  {
    if (!empty($cid))
    {
      $node = node_load($cid);  unset($node->nid);  // force a fresh save
      $node->aid_map_updated = $node->aid_map ; $node->aid_map = array();
      $op = "cloned" ;
    }
    else
    {
      $node = node_load($nid);
      $node->aid_map_updated = $node->aid_map ;
      $op = "saved" ;
    }
  }
  $header = $form['#header'];  // amazed this is kept intact

  foreach ($header as $h)  // note that carbon_stamp_submit has done all the conversions from string etc
  {
    $fieldname = $h['field'] ;
    if (isset($form_state['values'][$fieldname]))
    {
      $v = $form_state['values'][$fieldname];
      $node->$fieldname = $v ;
    }
  }
  node_save($node);
  $msg = l($form_state['values']['title'], 'node/'.$node->nid);
  drupal_set_message($msg . " " . $op , "info");
  $form_state['redirect'] = false ;  // prevent form api from executing a redirect and exiting
  global $nid_update ;  $nid_update = $node->nid ;
}

/**
 * Generate HTML for a collapsible panel.
 *
 * Uses standard collapsible fieldset javascript provided by drupal.
 *
 * @param string $title
 * @param string $body
 * @return none
 */

function theme_carbon_static_box($title, $body, $class = 'collapsible')
{
  return '<fieldset class="'.$class.'">'.
         '  <legend class="header">'.$title.'</legend>'.
         '  <div class="body">'.$body.'</div>' .
         '</fieldset>' ;
}


/**
 * Generate HTML for a panel which can be loaded with data on demand.
 *
 * Uses standard javascript but content is loaded by carbon_view.js
 *
 * @param $title
 * @param $url
 * @return none
 */


function theme_carbon_loadable_box($title, $url)
{
  return '<fieldset class="collapsible collapsed">'.
         '  <legend class="header">'.
         '     <a class="loadOnDemand" href="' . $url .'">' . $title . '</a>'.
         '  </legend>'.
         '  <div class="loadingZone"></div>' .
         '</fieldset>' ;
}




?>