<?php
/**
 *
 * created on 11-Nov-2006
 *
 * @file
 * definition of carbon_source node. Each carbon_source
 * node contains parameters that allow the carbon dioxide
 * emissions to be calulated from a particular source of
 * emissions e.g. domestic gas supply, car miles etc.
 *
 * This is a very straightforward standard drupal node
 * implementation.
 *
 */


/**
 * Implementation of hook_access().
 *
 * Refer to carbon_access because it is easier to check
 * access for all carbon types in one place.
 */


function carbon_source_access($op, $node)
{
	return carbon_access($op, $node);
}




/**
 * Render a page listing a one line summary of all carbon sources
 *
 * TODO add edit operations
 */

function carbon_source_page($format) {
  	global $user;

  $header = array(
//    array("data" => t("created")),
    array("data" => t("model"), "field" => "model"),
    array("data" => t("sector"),"field" => "sector"),
    array("data" => t("code"),  "field" => "code"),
    array("data" => t("title"), "field" => "title"),
    array("data" => t("units"), "field" => "units"),
    array("data" => t("CO2 multiplier or class"), "field" => "classname")
  );

  $sql = _get_carbon_source_sql();

  $sql .= tablesort_sql($header);

  // get the links (no range limit here)
  $queryResult = db_query($sql);
  $rows = array();
  $protection = _get_protection_options();
  $source_map = array();
  while ($node = db_fetch_object($queryResult))
  {
    $access_rw = carbon_access("update", $node, $node->uid);
    $link = "node/".$node->nid .($access_rw ? "" : "") ; // go to view mode first

    $source_map[] = array("code" => $node->code,
                          "title" => $node->title,
                          "units" => $node->units,
                          "model" => $node->model ) ;
	  $rows[] = array(
					$node->model,
          $node->sector,
					l($node->code,$link),
					$node->title,
					$node->units,
					$node->classname == "simple" ? $node->toco2 : $node->classname
					);
 	}

  if ($params["datatype"] == "json")
  {
    $output = drupal_json($output);
    print($output);
    exit(0);
  }

  $output = theme("table", $header, $rows);
  $output .= theme("pager", NULL, 10, 0);

  if (count($rows) == 0)
    $output .= "<p>".carbon_source_missing()."</p>";

  drupal_set_title(t("Carbon Sources"));
  return $output;
}

/**
 * hold the list of available sources globally because
 * different functions use this array during the same
 * transaction.
 */


function carbon_source_fetch_array()
{

}

/**
 * The administrator can select which carbon models to
 * make available. Every select for a carbon source
 * is appended with the system wide list of approved
 * models.
 */

function _get_carbon_source_sql()
{
  $sql = "SELECT r.*, n.* FROM {carbon_source} r " .
  "LEFT JOIN {node} n ON r.nid = n.nid " .
  "INNER JOIN {users} u on n.uid = u.uid " ;

  $model = variable_get("carbon_models", "");
  if (!(empty($model)))
  {
    $models = explode(",", $model);

    // wrap each element with " "
    for ($i = 0 ; $i < count($models) ; $i++) $models[$i] = "'".trim($models[$i])."'";
        $model = implode(",", $models);

    $sql .= " WHERE model in (".$model.") " ;
  }
  return $sql ;
}






/**
 * @return a map of key=> value pairs where the
 * key is the code in the database and
 * and the value is text presented to the user
 *
 * note that adding sources might while a user
 * is selecting might result in the wrong option.
 */


/**
 * Implementation of hook_form().
 */



function carbon_source_form(&$node, &$param) {
  global $user;
  $form = array() ;

  $form["source"] = array(
    "#type" => "fieldset",
    "#title" => "Carbon source",
    "#weight" => -3,
    "#collapsible" => TRUE,
    "#collapsed" => FALSE);

  $form["source"]["title"] = array(
    "#type" => "textfield",
    "#title" => t("title"),
    "#required" => TRUE,
    "#size" => 20,
    "#maxlength" => 80,
    "#weight" => -5,
    "#default_value" => $node->title,
    "#description" => t("Full descriptive name of this source"));

  $form["source"]["units"] = array(
    "#type" => "textfield",
    "#title" => t("units"),
    "#required" => TRUE,
    "#weight" => -4,
    "#default_value" => $node->units,
    "#size" => 20,
    "#maxlength" => 20,
    "#description" => t("Should be fairly short e.g. kilowatt hours, more details can be provided in notes"));


  $form["source"]["code"] = array(
    "#type" => "textfield",
    "#title" => t("code"),
    "#required" => TRUE,
    "#weight" => -3,
    "#size" => 20,
    "#maxlength" => 20,
    "#default_value" => $node->code,
    "#description" => t("Short unique code used to define this source e.g. kwh. " .
    		"This code must match the same code in other models if you want " .
    		"to compare the carbon footprint calculated by two models from " .
    		"the same inputtted values"));

  $form["source"]["model"] = array(
    "#type" => "textfield",
    "#title" => t("model"),
    "#required" => TRUE,
    "#size" => 20,
    "#maxlength" => 20,
    "#weight" => -2,
    "#default_value" => $node->model,
    "#description" => t("The overall model e.g. crag, carbon coach. You can use any arbitrary name."));

  $form["source"]["sector"] = array(
    "#type" => "textfield",
    "#title" => "sector",
    "#required" => TRUE,
    "#size" => 20,
    "#maxlength" => 20,
    "#weight" => -1,
    "#default_value" => $node->sector,
    "#description" => t("General category of energy use e.g. 'transport' or 'surface transport'. You can use any arbitrary name."));

  $e0 = variable_get("carbon_energy_units_0", "");

  $descrip = t('Conversion factor. Number of energy units expended by one unit. This field can be disabled in carbon settings.') ;

  if (!empty($e0))
    $form["source"]["tokwh0"] = array(
    "#type" => "textfield",
    "#title" => $e0,
    "#required" => TRUE,
    "#size" => 20,
    "#maxlength" => 20,
    "#weight" => 0,
    "#default_value" => $node->tokwh0,
    "#description" => $descrip);

  $e1 = variable_get("carbon_energy_units_1", "");

  if (!empty($e1))
    $form["source"]["tokwh1"] = array(
    "#type" => "textfield",
    "#title" => $e1,
    "#required" => TRUE,
    "#size" => 20,
    "#maxlength" => 20,
    "#weight" => 1,
    "#default_value" => $node->tokwh1,
    "#description" => $descrip);

  $form["source"]["toco2"] = array(
    "#type" => "textfield",
    "#title" => "CO2",
    "#required" => TRUE,
    "#size" => 20,
    "#maxlength" => 20,
    "#weight" => 2,
    "#default_value" => $node->toco2,
    "#description" => t("Conversion factor. Number of Kilograms of CO2 emitted by one unit."));

  $form["source"]["classname_index"] = array(
    "#type" => "select",
    "#title" => "classname",
    "#required" => TRUE,
    "#size" => 3,
    "#maxlength" => 20,
    "#weight" => 3,
    "#options" => _get_source_class_key(null),
    "#default_value" => array_search($node->classname, _get_source_class_key(null)),
    "#description" => t("PHP class that calculates the emissions"));

  $form["source"]["params"] = array(
    "#type" => "textarea",
    "#title" => "params",
    "#required" => false,
    "#size" => 20,
    "#maxlength" => 80,
    "#weight" => 5,
    "#height" => 2,
    "#default_value" => $node->params,
    "#description" => t("Additional parameters used by the class"));


  $form["source"]["body"] = array(
    "#type" => "textarea",
    "#title" => "notes",
    "#required" => false,
    "#size" => 4,
    "#maxlength" => 320,
    "#weight" => 4,
    "#default_value" => $node->body,
    "#description" => t("Notes about this carbon source and how the conversion " .
    		"factors have been calculated"));

  $form['#submit'] = array('carbon_source_submit');

  return $form ;
}


/**
 * Validate our forms
 * @param string form id
 * @param array form values
 */
function carbon_source_validate($form_id, $form_values)
{

}


/**
 * Implemenation of hook_load
 * @param node object to load additional information for
 * @return object with carbon fields
 */
function carbon_source_load($node)  {

	$record = db_fetch_object(db_query("SELECT * " .
			"FROM {carbon_source} WHERE nid = %d ", $node->nid));
	return $record;
}


function carbon_source_submit(&$form, &$form_state)
{
	$form_state['values']['classname'] = _get_source_class_key($form_state['values']['classname_index']);
}


/**
 * Implementation of hook_insert, which saves carbon-specific information
 * into the carbon table
 * @param node object
 */
function carbon_source_insert(&$node) {

  db_query("INSERT INTO {carbon_source} " .
    		"(nid, model, sector, code, units, classname, tokwh0, tokwh1, toco2, params)" .
    		"VALUES (%d, '%s', '%s', '%s', '%s', '%s', %f, %f, %f, '%s' )",
	$node->nid, $node->model, $node->sector, $node->code, $node->units,
	$node->classname, $node->tokwh0, $node->tokwh1, $node->toco2, $node->params);
}


/**
 * Implementation of hook_update, which saves updated todo-specific
 * information into the todo table
 * @param node object
 */
function carbon_source_update(&$node) {

  db_query("UPDATE {carbon_source} set model='%s',sector='%s',code='%s',units='%s'," .
    		"classname='%s', tokwh0=%f, tokwh1=%f, toco2=%f, params='%s' WHERE nid=%d",
	$node->model, $node->sector, $node->code, $node->units, $node->classname,
	$node->tokwh0, $node->tokwh1, $node->toco2, $node->params, $node->nid);
}


/**
 * Implementation of hook_delete().
 * (not certain that this is necessary)
 */
function carbon_source_delete($node) {
  db_query("DELETE FROM {carbon_source} WHERE nid = %d", $node->nid);
}



/**
 * Implementation of hook_view, add our node specific information
 * @param node object to display
 * @param boolean is this a teaser or full node?
 * @param boolean is this displaying on its own page
 */
function carbon_source_view(&$node, $teaser = FALSE, $page = FALSE)
{
  $node = node_prepare($node, $teaser);
  $carbon_source_info = theme('carbon_source_table_view', $node);  // try item_list
  $node->content["source"] = array("#value" => $carbon_source_info);
  $node->body .= $carbon_source_info;
  $node->teaser .= $carbon_source_info;
  return $node ;
}

/**
 * Theme function to display additional node data
 * @param node to display
 * @return HTML string with additional node information
 */


// then create an array of column names

function theme_carbon_source_table_view($node)
{
  $header = array(
    array("data" => t("created")),
    array("data" => t("model")),
    array("data" => t("sector")),
    array("data" => t("code")),
    array("data" => t("units")),
    array("data" => t("classname")),
    array("data" => t("renewable energy"), "field" => "tokwh0"),
    array("data" => t("non renewable energy"), "field" => "tokwh1"),
    array("data" => t("co2")),
    array("data" => t("params"))
  );


  $rows[] = array(
					_format_date($node->created),
					$node->model,
          $node->sector,
					$node->code,
					$node->units,
					$node->classname,
          $node->tokwh0,
          $node->tokwh1,
					$node->toco2,
					$node->params
					);

  $output = theme('table', $header, $rows);
//  $output .= theme('pager', NULL, 50, 0);

  return "<h1></h1>".$output;  // the H1s make bluebreeze work$output;
}


/**
 * @return the class for a given numeric index. If index is null, return keys (names) for all classes
 */

function _get_source_class_key($i)
{
  global $source_class_keys ;
  if (empty($source_class_keys))
    $source_class_keys = array_keys(_source_class_map());
  if (!isset($i)) return $source_class_keys ;
  return $source_class_keys[$i] ;
}


/**
 * Each carbon source can have a different classname.
 * Invoked from carbon_source.inc
 *
 * @return a map of class names (keys) and descriptions (values). Descriptions (values) not used yet.
 */

function _source_class_map()
{
  return array(
    "simple"        =>  t("simple multiplier"),
    "climatecare" =>    t("model used by commercial offsetting company"),
    "chooseclimate" =>  t("model created by climate mathematician")
  );
}



/**
 * @return the text to present to the user if no carbon sources are configured.
 */


function carbon_source_missing()
{
  return t(
    "<p class='message'>There are no available carbon sources. If you think you have already ".
    "configured or installed them, then try removing any carbon source model filter at !model. <p>".

    "<p class='message'>If you have never installed carbon sources, !create or !install.</p>",

    array("!model"=>l(t("Home » Administer » Site configuration » Carbon >> Settings"),
                      "admin/settings/carbon/settings"),
          "!create"=>l(t("create you own carbon sources"),
                      "node/add/carbon-source"),
          "!install"=> l(t("install the default carbon sources"),
                      "admin/settings/carbon/database"))
    );
}


/**
 * This function is called backed from the menu (defined in carbon.module)
 * On first entry, the form is set up. On second entry, the action
 * is executed.
 */

function carbon_source_install() {

  $edit = $_POST;
  if ($edit["confirm"] && $edit["form_id"] = "carbon_source_install_confirm")
    return carbon_source_install_default();

  return confirm_form(array(),
    t("Are you sure you want to install the default carbon sources"),
    "admin/settings/carbon/source/install",
    t("This action cannot be undone."),
    t("Install"), t("Cancel"));
}


/**
 * Provides utilities to access all possible source keys
 *
 */


class CarbonSources
{
  var $source = array() ;
  var $map = array() ;
  var $keys = array();

  static function getDefault()
  {
    global $defaultSources ;
    if (empty($defaultSources))
      $defaultSources = new CarbonSources();
    return ($defaultSources);
  }

  function CarbonSources()
  {
    $queryResult = db_query(_get_carbon_source_sql());

    while ($node = db_fetch_object($queryResult))
    {
      $this->source[] = $node ;
    }

    foreach($this->source as $s)
    {
        $this->map[$s->code] = $s->title. ', ' . $s->units ;
    }
    ksort($this->map);

    $this->keys = array_keys($this->map);
    $this->values = array_values($this->map);
  }


  /**
   * returns a map of key=> value pairs where the
   * key is the code in the database and
   * and the value is text presented to the user
   *
   * note that adding sources might while a user
   * is selecting might result in the wrong option.
   */

  function getMap()
  {
    return $this->map ;
  }

  function keys()
  {
    return $this->keys ;
  }

  function values()
  {
    return $this->values ;
  }

  function getModelMap()
  {
    $map = array();
    $map[''] = '-no preference-' ;
    foreach($this->source as $s)
    {
        $map[$s->model] = $s->model ;
    }
    ksort($map);
    return $map ;
  }
}



class Source
{
}


function  carbon_source_install_default()
{
  global $user ;
  $sources = _get_sources();
  $insert = $update = 0 ;
  foreach ($sources as $source)
  {
    $model = $source[2];
    $code = $source[3];

    // this routine is unused for updates as well as inserts.
    // get the nid for this particular source
    $node = db_fetch_object(db_query("SELECT nid " .
            "FROM {carbon_source} WHERE model = '%s' AND code = '%s' ", $model, $code));

    // now get the whole object
    if (!empty($node))
    {
      // fiddling around here so that only the nid is visible
      // to node_load.
      $node2 = new Source();
      $node2->nid = $node->nid ;
      $node = node_load($node2->nid);
      $update++ ;
    }
    else
    {
      $node = new Source();
      $node->nid = 0 ;
      $insert++ ;
    }

    $i = 0 ;
    $node->sector = $source[$i++];
    $node->title = $source[$i++];
    $node->model = $source[$i++];
    $node->code = $source[$i++];
    $node->units = $source[$i++];
    $node->classname = $source[$i++];
    $node->tokwh0 = $source[$i++];
    $node->tokwh1 = $source[$i++];
    $node->toco2 = $source[$i++];
    $node->params = $source[$i++];
    $node->type = 'carbon_source' ;
    $node->status = 1 ;  // published
    $node->promote = 0 ;
    $node->sticky = 0;
    $node->body = null ;
    $node->comment = 1 ;
    $node->uid = $user->uid ;
    $node->teaser = "" ;
    $node->body = "" ;
    node_save($node);
  }

  // Notify of changes
  drupal_set_message("Carbon module: inserted ".$insert.", updated  ".$update." sources of emissions", "info");
}




function _get_sources()
{
  return array(

    // CRAG numbers are based on COIN info - http://coinet.org.uk/solutions/carbon_rationing

    array("heating","natural gas","crag","gas-m3","cubic metres","simple","0.0","0.0","2.2",""),
    array("heating","natural gas","crag","gas-therms","therms","simple","0.0","0.0","6.2",""),
    array("heating","natural gas","crag","gas-kwh","kWh","simple","0.0","0.0","0.2",""),

    array("heating","heating oil","crag","oil-litres","litres","simple","0.0","0.0","3",""),
    array("heating","heating oil","crag","oil-gallons","gallons","simple","0.0","0.0","13.6",""),

    array("heating","anthracite coal","crag","coal-anthracite","25 Kg bags","simple","0.0","0.0","1.9",""),
    array("heating","bituminous coal","crag","coal-bituminous","25 Kg bags","simple","0.0","0.0","2.5",""),

    array("elec","conventional tariff electricity","crag","elec-kwh","kWh","simple","0.0","0.0","0.5",""),
    array("elec","green tariff electricity","crag","elec-green-kwh","kWh","simple","0.0","0.0","0",""),

    array("air","air travel","crag","air-miles","miles","simple","0.0","0.0","0.82",""),
    array("air","air travel","crag","air-km","Km","simple","0.0","0.0","0.51",""),
    array("air","air travel","crag","air-co2-kg","Kg CO2","simple","0.0","0.0","1",""),

    array("car","small car","crag","car-small-km","Km","simple","0.0","0.0","0.17",""),
    array("car","small car","crag","car-small-miles","miles","simple","0.0","0.0","0.28",""),
    array("car","medium car","crag","car-med-km","Km","simple","0.0","0.0","0.22",""),
    array("car","medium car","crag","car-med-miles","miles","simple","0.0","0.0","0.36",""),
    array("car","large car","crag","car-large-km","Km","simple","0.0","0.0","0.27",""),
    array("car","large car","crag","car-large-miles","miles","simple","0.0","0.0","0.43",""),
    array("car","small diesel car","crag","car-d-small-km","Km","simple","0.0","0.0","0.12",""),
    array("car","small diesel car","crag","car-d-small-miles","miles","simple","0.0","0.0","0.19",""),
    array("car","large diesel car","crag","car-d-large-km","Km","simple","0.0","0.0","0.14",""),
    array("car","large diesel car","crag","car-d-large-miles","miles","simple","0.0","0.0","0.22",""),
    array("car","LPG car","crag","car-lpg-km","Km","simple","0.0","0.0","0.17",""),
    array("car","LPG car","crag","car-lpg-miles","miles","simple","0.0","0.0","0.28",""),

    array("car","petrol","crag","petrol-litres","litres","simple","0.0","0.0","2.3",""),
    array("car","petrol","crag","petrol-gallons","gallons","simple","0.0","0.0","10.4",""),
    array("car","diesel","crag","diesel-litres","litres","simple","0.0","0.0","2.7",""),
    array("car","diesel","crag","diesel-gallons","gallons","simple","0.0","0.0","12.2",""),

    array("surface","public transport","crag","pt-miles","miles","simple","0.0","0.0","0.11",""),
    array("surface","public transport","crag","pt-km","Km","simple","0.0","0.0","0.07",""),

    array("elec","electricity","carbon coach","elec-gbp","pounds","simple","0.0","0.0","5",""),
    array("heating","natural gas","carbon coach","gas-gbp","pounds","simple","0.0","0.0","10",""),
    array("car","petrol","carbon coach","petrol-gbp","pounds","simple","0.0","0.0","2.5",""),
    array("surface","public transport","carbon coach","pt-miles","miles","simple","0.0","0.0","0.1",""),
    array("air","air travel, short haul","carbon coach","air-short-hours","hours","simple","0.0","0.0","333",""),
    array("air","air travel, long haul","carbon coach","air-long-hours","hours","simple","0.0","0.0","250",""),

    // http://www.resurgence.org/carboncalculator/

    array("elec","electricity","resurgence","elec-kwh","kWh","simple","0.0","0.0","0.43",""),
    array("heating","natural gas","resurgence","gas-kwh","kWh","simple","0.0","0.0","0.19",""),
    array("heating","natural gas","resurgence","gas-therms","therms","simple","0.0","0.0","5.5",""),
    array("heating","natural gas","resurgence","gas-m3","cubic metres","simple","0.0","0.0","1.77",""),
    array("heating","LPG","resurgence","lpg-litres","litres","simple","0.0","0.0","1.51",""),
    array("heating","butane","resurgence","butane-4.5","4.5 Kg bottles","simple","0.0","0.0","11.82",""),
    array("heating","butane","resurgence","butane-7","7 Kg bottles","simple","0.0","0.0","18.39",""),
    array("heating","butane","resurgence","butane-15","15 Kg bottles","simple","0.0","0.0","39.41",""),
    array("heating","propane","resurgence","propane-3.9","3.9 Kg bottles","simple","0.0","0.0","11.48",""),
    array("heating","propane","resurgence","propane-6","6 Kg bottles","simple","0.0","0.0","17.67",""),
    array("heating","propane","resurgence","propane-13","13 Kg bottles","simple","0.0","0.0","38.28",""),
    array("heating","propane","resurgence","propane-19.5","19.5 Kg bottles","simple","0.0","0.0","57.42",""),
    array("heating","heating oil","resurgence","oil-litres","litres","simple","0.0","0.0","2.68",""),
    array("heating","wood","resurgence","wood-kg","Kg","simple","0.0","0.0","0.518",""),
    array("heating","bituminous coal","resurgence","coal-bituminous","25 Kg bags","simple","0.0","0.0","2.41",""),

    array("air","air travel","resurgence","air-hours","hours","simple","0.0","0.0","435",""),

    array("car","petrol","resurgence","petrol-litres","litres","simple","0.0","0.0","2.31",""),
    array("car","diesel","resurgence","diesel-litres","litres","simple","0.0","0.0","2.68",""),
    array("car","kerosene","resurgence","kerosene-litres","litres","simple","0.0","0.0","2.52",""),

    array("air","air travel (Boeing 747)","choose climate","air-747-km","Km","chooseclimate","0.0","0.0","1.0",""),
    array("air","air travel (Boeing 747)","choose climate","air-747-miles","miles","chooseclimate","0.0","0.0","1.609",""),

    array("air","air travel","climate care","air-km","Km","climatecare","0.0","0.0","1.0",""),
    array("air","air travel","climate care","air-miles","miles","climatecare","0.0","0.0","1.609",""),

    // this is for people in groups who prefer to use a speadsheet and work offline

    array("mixed", "co2 calculated offline","crag","co2-kg","Kg","simple","0.0","0.0","1.0","")
    );
}

?>
