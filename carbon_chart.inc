<?php

/**
 *
 * chart functions for drupal carbon module.
 *
 * Created    29-jan-2006
 * Modified   09-feb-2010   use google chart api
 *
 *
 */

/**
 *  this function pre processes data then calls the drupal chart
 *  module which then invokes (google) chart modules
 */

//todo check escape sequences are good.

function carbon_chart_stacked_bar($title, $raw_data, $alias, $y_label)
{
    if (!function_exists('chart_render'))
      return(t("<a href='@chart'>Chart API module</a> not installed",
        array("@chart"=>"http://drupal.org/project/chart")));


    $palette = new Palette();
    $palette->initialize();
    $ordered_sectors = $palette->customizeForSectorMap($raw_data);

    $data = _fill_inner_cells($raw_data, $ordered_sectors);

    $transposed_data = _transpose_two_dimension_array($data);

    $chart = array(
      '#title' => chart_title(t($title), '444444', 15),
      '#chart_id' => 'carbon',
      '#type' => CHART_TYPE_BAR_H,
      '#data' => $transposed_data,
      '#adjust_resolution' => false,     // it does not work for stacked bar chart
      '#legends' => array()
    );

    // not sure why array has to be reversed
    foreach(array_reverse($data, true) as $key => $ignore)
    {
      $l = array_key_exists($key, $alias) ? $alias[$key] : $key."?" ;
      $chart['#mixed_axis_labels'][CHART_AXIS_Y_LEFT][0][] = chart_mixed_axis_label($l);
    }

    $chart['#size'] = chart_size(550, 100 + 25 * count($data)) ;

    $max_value = _max_value($transposed_data, true);

    $chart['#mixed_axis_labels'][CHART_AXIS_X_BOTTOM][0][] =
      chart_mixed_axis_range_label(0, $max_value, true);

    _normalize($chart['#data'], $max_value);

    // it's y_label because chart is horizontal
    $chart['#mixed_axis_labels'][CHART_AXIS_X_BOTTOM][1][] = chart_mixed_axis_label($y_label, 50);

    $palette->writeLegends($chart);

    return chart_render($chart);   // call google chart module
}



function carbon_chart_grouped($title, $raw_data, $alias, $y_label)
{
    if (!function_exists('chart_render'))
      return(t("<a href='@chart'>Chart API module</a> not installed",
        array("@chart"=>"http://drupal.org/project/chart")));

        // stick in test data here

    $palette = new Palette();
    $palette->initialize();
    $orderedSectors = $palette->customizeForSectorMap($raw_data);

    $data = _fill_inner_cells($raw_data);
    $transposed_data = _transpose_two_dimension_array($data);

    $bar_size_max = 15 ;
    $bar_space = 0 ;        // start off assuming there is no space
    $bar_group_space = 4 ;

    $max_chart_height = 550 ; // arbitrary google constraint

    $group_height = ($max_chart_height - 120) / count($data);
    $bar_size = (int) ( ($group_height - $bar_group_space) / count($transposed_data) - $bar_width );
    $bar_size = min($bar_size, $bar_size_max);
    if ($bar_size >= 3)
    {
      $bar_space = 1 ; $bar_size-- ;
    }

    $chart = array(
      '#title' => chart_title(t($title), '444444', 15),
      '#chart_id' => $y_label,
      '#type' => CHART_TYPE_BAR_H_GROUPED,
      '#data' => $data ,
      '#adjust_resolution' => false,     // it does not work for stacked bar chart
      '#legends' => array(),
      '#data_colors' => array(),
      '#bar_size'=>chart_bar_size($bar_size,$bar_space, $bar_group_space)
    );

    $chart['#size'] = chart_size(450, 120 +  ((($bar_size + $bar_space) * count($transposed_data) + $bar_group_space)  *  count($data))) ;


    $i = 0 ;
    $colors = Palette::$spareColors ;

    $p = new Palette();
    $p->initialize();
    $colors = array_values($p->defaultPalette) ;

    foreach($data as $key => $ignore)
    {
      $chart["#legends"][] = array_key_exists($key, $alias) ? $alias[$key] : $key ;
      $chart["#data_colors"][] = $colors[$i++] ;
    }

    // get account names from inner array

    foreach(array_reverse(array_keys($transposed_data)) as $l)
      $chart['#mixed_axis_labels'][CHART_AXIS_Y_LEFT][0][] = chart_mixed_axis_label($l);


//    $chart['#size'] = chart_size(450, 600 );

    $max_value = _max_value($chart['#data']);

    $chart['#mixed_axis_labels'][CHART_AXIS_X_BOTTOM][0][] =
      chart_mixed_axis_range_label(0, $max_value);

    _normalize($chart['#data'], $max_value);

    // it's y_label because chart is horizontal
    $chart['#mixed_axis_labels'][CHART_AXIS_X_BOTTOM][1][] = chart_mixed_axis_label($y_label, 50);

    return chart_render($chart);   // call google chart module
}


function _print_date_range($range)
{
  $string = "" ;
  foreach ($range as $date)
  {
    $string .= CarbonDate::asYYYYMMDD($date) . " ";
  }
  return $string . " \n<br/>" ;
}

/**
 * It is more natural to construct/calculate carbon
 * emissions with the sectors in the inner array and
 * the date or the account name on the outer array.
 * However google api requires the date to be organised
 * the other way round thus transposition required.
 */


function _transpose_two_dimension_array($arrin)
{
  $arrout = array();
  foreach ($arrin as $key => $subarr)
  {
    foreach ($subarr as $subkey => $subval)
    {
      $val = $arrin[$key][$subkey] ;
      // todo check if array exists
      $arrout[$subkey][$key] = $val ;
    }
  }
  return $arrout;
}


/**
 * Iterate through the chart data array and find the max data value.
 * If there are two levels of data (e.g. in stacked bar chart), add
 * all the elements together in the inner array.
 *
 * @param unknown_type $chart_data
 * @param unknown_type $max
 * @return mixed
 */


function _max_value(&$chart_data, $accumulate = false, $max = 0)
{
  $max = 0 ;
  $bar = array();
  foreach ($chart_data as $sector => $ar)
  {
    foreach ($ar as $timestamp => $val)
    {
      if (empty($bar[$timestamp]))
        $bar[$timestamp] = 0 ;
      if (!is_numeric($val))
        $val = 0 ;

      $accumulate ? $bar[$timestamp] += $val : $bar[$timestamp] = max($bar[$timestamp],$val);
    }
  }
  foreach ($bar as $timestamp1 => $val1)
    $max = max($val1, $max);

  return $max ;
}



function _fill_inner_cells($chart_data, $keys = null)
{
  if (empty($keys))
  {
    // collect all the innner keys
    $keys = array();
    foreach ($chart_data as $key => $subarr) {
      foreach ($subarr as $subkey => $subvalue) {
        $keys[] = $subkey ;
      }
    }
  }
  $oa = array();
  foreach ($chart_data as $key => $ar)
  {
    $oa[$key] = array();
    foreach ($keys as $subkey)
    {
      $oa[$key][$subkey] = $ar[$subkey] ;
    }
  }
  return $oa ;
}



function _normalize(&$chart_data, $max_value)
{
  foreach ($chart_data as $key => $ar)
  {
    foreach ($ar as $subkey => $subval)
    {
      $num = (int) ($subval * 100 / $max_value) ;
      $chart_data[$key][$subkey] = $num ;
    }
  }
}

/**
 * The Palette defines the colour of each bar or pie in a chart.
 * The user can preassign colours to particular sectors which
 * makes all the charts visually consistent.
 *
 * @author johna
 *
 */


class Palette
{
  var $defaultPalette, $palette ;

  function initialize()
  {
    $p = variable_get("carbon_sectors", CARBON_PALETTE);
    if (empty($p))  // reset the original string
      variable_set("carbon_sectors", CARBON_PALETTE);

    $sectors = explode(",", variable_get("carbon_sectors", CARBON_PALETTE));
    $this->defaultPalette = array();
    foreach ($sectors as $sector)
    {
       $sector = trim($sector);
       $pair = explode('=', $sector);  $key = $pair[0] ;  $val = $pair[1] ;

       $this->defaultPalette[$key] = $val ;
    }
  }

  /**
   * iterate through a sector array produced by carbon_calculate
   * and add/extend the pallete to match the selectors
   *
   * @param array of sector data (as created by carbon_calculate)
   * @return array of sectors in order that they should be presented
   */

  //                                  b_blue    b_green   yellow    pink      grey1     grey2
  public static $spareColors = array('5555FF', '55ff55', 'ff55ff', 'ffff55', 'eeeeee', 'dddddd');

  function customizeForSectorMap($data)
  {
    $this->initialize();

    // create a map of sectors in use
    $sectorAssigned = array();
    foreach($data as $inner)
      if (is_array($inner))
        foreach($inner as $key => $i)
          $sectorAssigned[$key] = false ;
      else
      {
      }

    // take colours out of the default palette, in the order
    // they should appear in the charts

    foreach ($this->defaultPalette as $sector => $color)
    {
      if (key_exists($sector, $sectorAssigned))
      {
        $this->palette[$sector] = $color ;
        $sectorAssigned[$sector] = true ;
      }
    }

    $i = 0 ;
    foreach ($sectorAssigned as $sector => $assigned)
    {
      if ($assigned)  continue ;
      $this->palette[$sector] = $this->spareColors[$i++];
    }
    return array_keys($this->palette);
  }

  /**
   * we have already populated palette,
   * now create legends and data_colors used by
   * google charts.
   *
   * @param $chart
   * @return unknown_type
   */

  function writeLegends(&$chart)
  {
    $chart['#data_colors'] = array();
    $chart['#legends'] = array();

    foreach ($chart['#data'] as $sector => $i)
    {
      $chart['#legends'][] = $sector ;
      if (!empty($this->palette[$sector]))
      {
        $chart['#data_colors'][] = $this->palette[$sector] ;
      }
      else  // should never get here.
      {
        $chart['#data_colors'][] = 'EEEEEE' ;  // light grey
      }
    }
  }
}


   function preint_r($array)
   {
      echo '<pre>';
      print_r($array);
      echo '</pre>';
   }


?>