<?php
/**
 *
 * Drupal carbon stamp module.
 *
 * A carbon stamp is a record of an activity that resulted in CO2 emission.
 *
 * Created on 18-Oct-2006
 *
 */


/**
 * Implementation of hook_access().
 *
 * Delegate carbon_access to the carbon module because it
 * is easier to check access for all carbon types in one place.
 */

function carbon_stamp_access($op, $node)
{
  return carbon_access($op, $node);
}


/**
 * Render a page listing a one line summary of all carbon footprints
 */


function carbon_stamp_page($aid, $target_user, $meterPage) {
  global $user;

  $header = array(
    array("data" => t("title"),   "field" => "title"),
    array("data" => t("start"),   "field" => "startdate"),
    array("data" => t("end"),     "field" => "enddate"),
    array("data" => t("code"),    "field" => "code"),
    array("data" => t("amount"),  "field" => "reading"),
    array("data" => t("fp's"),    "field" => "numfoot"),
    array("data" => t("status"),  "field" => "status"),
    array("data" => t("operations"))
  );

  if (empty($uid))
    $header[] = array("data" => t("author"),"field" => "name");

  $sql = carbon_stamp_query($aid, $target_user->uid, null, $meterPage);

  $sql .= tablesort_sql($header);

  // get the links (no range limit here)
  $queryResult = db_query($sql);

  while ($node = db_fetch_object($queryResult))
  {
    $node->protection = _log2($node->protect2);
    $ro_access = carbon_access("view", $node);
    if ($ro_access)
    {
      $row =  array(carbon_link_from_title($node, $node, ""),
        $node->startdate == 0 ? '' : _format_date_time($node->startdate),
        _format_date_time($node->enddate),
        $node->code,
        $node->reading.' '._get_scope_key($node->scope),
        $node->numfoot <= 1 ? (isset($node->sid) ? 1 : 0) : $node->numfoot,
        $node->status >= 1 ? 'pub' : '',
        $node->protection . " " . carbon_format_edit_ops($node, null));

      if (empty($uid))
        $row[] = $node->name ;
      $rows[] = $row ;
    }
  }

  $output = theme("table", $header, $rows);
  $output .= theme("pager", NULL, 10, 0);

  if ($params["datatype"] == "json")
  {
    $output = drupal_json($output);
    print($output);
    exit(0);
  }

  $title = t("Carbon Stamps");

  if (!empty($target_user))
    $title .= t(" owned by !name", array("!name" => $target_user->name));

  drupal_set_title(check_plain($title));

  return $output;
}


function carbon_stamp_query($aid, $uid, $nid, $meterPage)
{
  $sql = "SELECT  t.*, n.title, n.nid, n.status, n.uid, n.type, name, count(*) as numfoot, " .
         "BIT_OR(POWER(2,a.protection)) as protect2, j.sid " .
         "FROM {carbon_stamp} t " .
         "INNER JOIN {node} n                   ON t.nid = n.nid " .
         "INNER JOIN {users} u                  ON n.uid = u.uid " .
         "LEFT  JOIN {carbon_stamp_account} j   ON t.nid = j.sid " .
         "LEFT  JOIN {carbon_account} a         ON j.fid = a.nid " .
         "WHERE TRUE " .
         (empty($aid) ?   '' : " AND a.nid   = ".$aid.   " ") .
         (empty($uid) ?   '' : " AND n.uid   = ".$uid.   " ") .
         (empty($nid) ?   '' : " AND n.nid   = ".$nid.   " ") .
         (!isset($meterPage) ? '' : " AND t.scope". ($meterPage ? "=1 " : "!=1 ")) .
         "GROUP BY t.nid ";

  return $sql ;
}


/**
 * convert
 *
 * @param $protect
 * @return converted number
 *
 */

function _log2($protect2)
{
	$k = 0 ;
	for ($i = 1 ; $i < $protect2 ; $i *= 2)
    $k++ ;
  return $k  ;
}



function carbon_stamp_header()
{
  global $carbon_stamp_header ;
  if (!isset($carbon_stamp_header))
  {
    $carbon_stamp_header = array(
    array("data" => t("date"),  "field" => "enddate", "sort" => "asc"),
    array("data" => t("title"), "field" => "sourcetitle"),
    array("data" => t("model"), "field" => "sourcemodel"),
    array("data" => t("emission"),"field" => "title"),
	  array("data" => t("amount"), "field" => "reading"),
	  array("data" => t("units"),  "field" => "units"),
	  array("data" => t("adjust"), "field" => "adjustment"));
  }
	//    array("data" => t("type or size")),
  return $carbon_stamp_header ;
}

/**
 *
 * Joins the recorded stamps in a carbon account with all the available carbon
 * sources.
 *
 * Note that some source fields have been renamed e.g. sourceid, sourceclass, sourcetitle
 *
 *@param $nid  select stamps attached to this footprint id, can be null
 *@param $uid  select stamps with this uid, can be null
 *@param $org  select stamps with this org, can be null
 *@return an array of carbon stamps
 */


function carbon_stamp_array_load($nid, $uid, $org)
{
  /**
   * this is a very tricky SQL statment. It is currently arranged to
   * show all carbon stamps that do not have a matching carbon source
   * so that user can be warned.
   */

	$query ="SELECT  nstamp.title, nstamp.status, nstamp.uid, nstamp.type, stamp.*, s.uid AS shared_uid, " .
    "nsource.title as sourcetitle, source.nid as sourceid, source.code as sourcecode, ".
    "source.classname as sourceclass, nsource.status as sourcestatus, ".
    "source.sector, source.model as sourcemodel, units, tokwh0, tokwh1, toco2, params, " .
    "f.model as preferred_model, f.organization, f.enablekwh0, f.enablekwh1, f.enableco2 " .
    "FROM {carbon_stamp} stamp " .
    "LEFT JOIN {carbon_source} source ON stamp.code=source.code " .
    "LEFT JOIN {carbon_stamp_account} j ON stamp.nid = j.sid " .
    "INNER JOIN {carbon_account} f      ON     f.nid = j.fid " .
    "LEFT JOIN {carbon_account_share} s ON     f.nid = s.nid " .
    "INNER JOIN {node} nstamp ON  stamp.nid = nstamp.nid ".
    "LEFT JOIN {node} nsource ON  source.nid = nsource.nid ".
    "WHERE   nstamp.status ".      // stamp must be published
 //   "AND nsource.status ".         // source must be published
    "AND (j.fid = %d  " .           (empty($nid) ?  " OR 1) " : ") ").
    "AND (nstamp.uid = %d " .       (empty($uid) ?  " OR 1) " : ") ").
    "AND (f.organization = '%s' " . (empty($org) ?  " OR 1) " : ") ").
    "" ;

//	$query = db_rewrite_sql($query);
//  drupal_set_message(" my sql ". $query, "info");

  $query .= tablesort_sql(carbon_stamp_header());
  $queryResult =  db_query($query, $nid, $uid, $org);

  $stamps = array();
  $sources = array();
  $num = 0 ;
	while ($record = db_fetch_object($queryResult))
	{
    $stamps[] = $record ;
	}
  $filtered_stamps = _select_stamps_from_preferred_sources($stamps);

	return $filtered_stamps;
}


function _stamp_form(&$form, $stamp, $carbonSources)
{
    $form["stamp"] = array(
    "#type" => "fieldset",
    "#title" => "Carbon stamp",
    "#weight" => -3,
    "#collapsible" => TRUE,
    "#collapsed" => FALSE);

	  $form["stamp"]["title"] = array(
		  "#type" => "textfield",
    	"#title" => t("description"),
	    "#required" => true,
 	    "#default_value" => empty($stamp->title) ? "" : $stamp->title ,
      "#size" => 20,
      "#maxlength" => 255,
	    "#weight" => -2,
	    "#description" =>
          _div_wrap("scope-0",t("Name of activity e.g. train to Leeds")).
          _div_wrap("scope-1",t("Meter name e.g. gas. " .
          "This must be the same for all readings from the same meter.")).
          _div_wrap("scope-2",t("Name of repeating activity e.g. annual gas bill"))
          );

	  $form["stamp"]["scope_index"] = array(
      "#attributes" => array("class"=>"scope"),
		  "#type" => "radios",
	    "#title" => t("type of carbon stamp"),
	    "#required" => FALSE,
		  "#options" => _get_scope_description(),
      "#size" => 14,
      "#maxlength" => 20,
	    "#weight" => -1,    // $stamp->scope can be 0 !
	    "#default_value" => isset($stamp->scope) ? $stamp->scope : 1,
	    "#description" =>
          _div_wrap("scope-0",t("A one off emission can be used for single journeys ")).
          _div_wrap("scope-1",t("Note that meter readings are subtracted from one another " .
          "to calculate consumption. You need to have at least two meter readings before this " .
          "source of emissions will be included in your carbon footprint. You need not specify " .
          "start or end dates, just the date the meter was read.")).
          _div_wrap("scope-2",t("A source of emissions that regularly repeats. Enter the the number ".
          "of days per year in the adjustment field below. ".
          "Use the start and stop dates as needed."))
          );

	  $form["stamp"]["code_index"] = array(
		  "#type" => "select",
    	"#title" => t("select source of emissions"),
	    "#required" => FALSE,
		  "#options" => $carbonSources->values(),
	    "#default_value" => empty($stamp->code) ? 0 : array_search($stamp->code, $carbonSources->keys()),
      "#size" => 1,
      "#maxlength" => 20,
	    "#weight" => 0,
	    "#description" => t("Select from pull down list of emissions configured for this site."));

	  $form["stamp"]["reading"] = array(
		  "#type" => "textfield",
    	"#title" => t("amount"),
	    "#required" => true,
	    "#default_value" => empty($stamp->reading) ? "" : $stamp->reading,
      "#size" => 8,
      "#maxlength" => 20,
	    "#weight" => 0,
	    "#description" =>
	       "<div class='scope-0'>"._help_on_distance()."</div>".
         "<div class='scope-1'>".t("Enter meter reading")."</div>".
	       "<div class='scope-2'>"._help_on_distance()."</div>"
         );

	  $form["stamp"]["startdate_s"] = array(
		  "#type" => "textfield",
    	"#title" => t("starting date"),
	    "#required" => FALSE,
	    "#default_value" => empty($stamp->startdate)  ? "" : _format_date_time($stamp->startdate),
      "#size" => 10,
      "#maxlength" => 20,
	    "#weight" => 1,
      "#prefix" => "<div class='startdate'>",
      "#suffix" => "</div>",
	    "#description" =>
	    		t("Format: %time.",
	    		array("%time" => _format_date_only(time()))).
          _div_wrap("scope-0",t("The first date of the activity. Leave blank".
          " except when you are specifying an activity that spreads over days or weeks".
          " such as the total distance travelled by bus over the last month or two.")).
          _div_wrap("scope-1",t("Not used")).
          _div_wrap("scope-2",t("The first date that this repeating event started.")));

	  $form["stamp"]["enddate_s"] = array(
		  "#type" => "textfield",
    	"#title" => t("date"),
	    "#required" => FALSE,
	    "#default_value" => $stamp->enddate == 0 ? "" : _format_date_time($stamp->enddate),
      "#size" => 10,
      "#maxlength" => 20,
	    "#weight" => 2,
	    "#description" =>
	    		t("Format: %time. Leave blank to use today's date. ",
	    		array("%time" => _format_date_time(time()))).
          _div_wrap("scope-0",t("The last date of the activity")).
          _div_wrap("scope-1",t("The date that the meter was read")).
          _div_wrap("scope-2",t("The day after the last date on which this repeating event stopped.")));


    $help_shared_heating = t("<h4>Sharing heating, electricity or a car trip</h4>" .
          "For example if you are sharing with 4 people you might enter <b>1/4</b>.");

    $help_repeating_trips = t(
          "<h4>For an Annual Return Trip</h4>Enter <b>2</b> ".
          "<h4>For a Frequent Trip</h4>Enter the number of days each year that you make this trip. ".
          "For a monthly return trip, enter <b>2*12</b>. ".
          "<h4>For a daily commute to work</h4> Assuming you take 5 weeks holiday, you " .
          "might want to enter <b>2*220</b>. ");

	  $form["stamp"]["adjustment"] = array(
		  "#type" => "textfield",
    	"#title" => t("adjustment"),
	    "#required" => FALSE,
	    "#default_value" =>  $stamp != null && $stamp->adjustment != 0 ? $stamp->adjustment : 1,
	    "#weight" => 3,
      "#size" => 6,
      "#maxlength" => 20,
	    "#description" => t("Multiply emissions or energy up to the date of this reading by this number. Normally left blank but:").
          _div_wrap("scope-0",t($help_shared_heating)).
          _div_wrap("scope-1",t($help_shared_heating)).
          _div_wrap("scope-2",t($help_repeating_trips)));

	  $form["stamp"]["code"] = array(
		  "#type" => "hidden",
	    "#value" => null);
}

function _help_on_distance()
{
  return (
    t("Number of units, miles, gallons, litres, KwH etc. "). t("Calculate distance between ").
    l(t("airports around the world"), "http://www.timeanddate.com/worldclock/distance.html",
      array("TARGET" => "distance_calculation_global"))." or between ".
    l(t("local towns"), "http://co2balance.uk.com/co2calculators/rail-travel/",
      array("TARGET" => "distance_calculation_local")))."&nbsp;".
    t("Enter one way distances and set adjust to 2 for return trips");
}



/**
 * When a user creates or edits a carbon_stamp, s/he may want to
 * add or remove it from a particular carbon footprint. This
 * form is only attached when individual carbon stamps are
 * edited.
 */

function _add_footprint_select(&$form, $node)
{
  global $user ;

  // find all the carbon footprints owned by the same user as this stamp
  // or , if this is a new stamp, the logged in user. This is really to
  // make testing easier.

  $uid = isset($node->uid) ? $node->uid : $user->uid ;
  $queryResult = db_query("SELECT n.nid,n.title from {node} n ".
        " INNER JOIN {carbon_account} f ON n.nid=f.nid ".
        " WHERE type='carbon_account' and uid = ".$uid);


  // get all the normal attachments
  while ($record  = db_fetch_object($queryResult))
  {
    $options[$record->nid] = $record->title ;
  }

  $ticked_boxes = is_array($node->aid_map) ? $node->aid_map : array();

  // add all the existing attachments which include footprints
  // belonging to other UIDs.

  foreach($ticked_boxes as $nid => $title)
    $options[$nid] = $title ;


  $form['attach'] = array(
    "#type" => "fieldset",
    "#weight" => -2,
    "#title" => t("Select carbon account(s)"),
    "#collapsible" => TRUE,
    "#collapsed" => FALSE);

  $f = &$form['attach'];

  // this clumsy code sets the only check box if this is a new
  // carbon stamp and there is only one footprint to attach to

  if (empty($node->nid) && empty($exist) && count($options) == 1)
  {
    $ticked_boxes = $options;
  }

  // if there is only one option and it is ticked, collapse the frame

  if (count($options) == 1 && count($ticked_boxes) == 1)
  {
    $f["#collapsed"] = true;
  }

  $f["aid_map_updated"] = array(
  "#type" => "checkboxes",
  "#default_value" => array_keys($ticked_boxes),
  "#options" => $options,
  "#description" => t("Select the the carbon accounts to which you would like to attach this carbon stamp.")
  );
}

/**
 *
 *
 * @param $node
 * @param $form_state
 * @return the constructed form
 */


function carbon_stamp_form(&$node, $form_state) {
	global $user, $aid_map;

  $form = array();
/*
  if (count($source_descriptions) == 0)
  {
    $form["no-sources"] = array(
      "#type" => "item",
      "#value" => carbon_source_missing());
    return $form ;
  }
*/
  _stamp_form($form, $node, CarbonSources::getDefault());
  _add_footprint_select($form , $node, 0 , 0 );
  $form['#submit'] = array('carbon_stamp_submit');

  $path = carbon_include_core_js();
  drupal_add_js($path . "/carbon_debug.js", "module");

  return $form ;
}

/**
 * scope is held as a integer in the database which
 * indexes this map directly, so take care about changing the
 * order of the entries in the table.
 */

function _scope_map()
{
  return array(
    "" =>    t("a single event"),
    "m" =>   t("a meter or mileometer reading"),
    "r" =>   t("an annually repeating event")
    );
}


/**
 * @return a short code/description of this numeric scope value
 */

function _get_scope_key($i)
{
  global $scope_keys ;
  if (empty($scope_keys))
    $scope_keys = array_keys(_scope_map());
  return $scope_keys[$i] ;
}

/**
 * @return a long description of this numeric scope value
 */

function _get_scope_description($i = null)
{
  global $scope_values ;
  if (empty($scope_values))
    $scope_values = array_values(_scope_map());
  return $i == null ?   $scope_values : $scope_values[$i] ;
}

/**
 * @return a short description of this numeric scope value
 */

function _get_scope_short_description($i = null)
{
  if ($i == 0) return null ;
  return _get_scope_description($i);
}


/**
 * Validate our forms
 * @param string form id
 * @param array form values
 */
function carbon_stamp_validate($form_id, $form_values)
{
	for ($i = 0 ; $i < 500 ; $i++)
  {
	  _validate_adjustment($form_id, "ff". $i .".adjustment");
    _validate_date($form_id, "ff". $i .".startdate_s");
    _validate_date($form_id, "ff". $i .".enddate_s");
  }
}


/**
 * the field is evaluated PHP so it is important not to
 * allow any code to be maliciously inserted.
 */

function _validate_adjustment($form_id, $field_id)
{
  $matches = array();
  if (!empty($form_id->$field_id))
    if (!preg_match('/^[0-9\.\*\/\+\-\(\)]*$/', $form_id->$field_id, $matches))
      form_set_error($field_id, t("Only . 0-9 ( ) * / + or - accepted in 'adjustment' field"));
}


function _validate_date($form_id, $field_id)
{
  $matches = array();
  if (!empty($form_id->$field_id))
    if (!preg_match("/^\d\d\d\d\-\d\d\-\d\d/", $form_id->$field_id, $matches))
      form_set_error($field_id, t("Please check that date is in YYYY-mm-dd format."));
}


/**
 * This function is called prior to carbon_stamp node_save.
 *
 * @param $form
 * @param $form_state   discovered that there is a ['values'] field.
 *
 */
function carbon_stamp_submit(&$form, &$form_state)
{
  $upd = 0 ;
  $f = &$form_state['values'] ;

  if (isset($form_state['values']['code_index']))
  {
    $keys = CarbonSources::getDefault()->keys();
    $f['code'] = $keys[$f['code_index']];  $upd += 1;
  }
  if (isset($f['scope_index']))
  {
    $f['scope'] = $scope = $f['scope_index'];  $upd += 2;
  }
  if (!empty($f['startdate_s']))
  {
    $f['startdate'] = strtotime($f['startdate_s']);  $upd += 4;
  }
  else
  {
    $f['startdate'] = "" ;    // remove existing date
  }

  if (!empty($f['enddate_s']))
  {
    $f['enddate'] = strtotime($f['enddate_s']); $upd += 8;
  }
  else
  {
    $f['enddate'] = $scope == 1 ? time() : CarbonDate::today();  $upd += 16;
  }
//  drupal_set_message('(debug) updated fields map :'.$upd, 'info');

  if (!isset($f['aid_map_updated']))
  {
//    drupal_set_message("Unable to attach to a footprint", "info");
    return ;
  }
  global $aid_map ;   // this is yuk., assumes we have just loaded the node
  if (!isset($aid_map))
    $aid_map = array();

  // after the editing, on checked boxed the values in
  // aid_updated are set to the keys, on unchecked boxes
  // the values are set to zero.

  $aid_map_updated = array_flip($f['aid_map_updated']);
  if (isset($aid_map_updated[0]))
    unset($aid_map_updated[0]);   // loose the 0 key created by any unchecked boxes

  $f['aid_map'] = $aid_map ;
  $f['aid_map_updated'] = $aid_map_updated ;
}



/**
 * Implementation of hook_load.
 * Slightly cheekily get the count of the number of footprints in the query
 * stamp->aid_map[] contains account_id => title of stamp
 *
 * @param $node
 * @return carbon_stamp
 */

function carbon_stamp_load(&$node)  {

	$query = db_query(
	"SELECT t.* , j.fid as attached_aid, n.title as attached_title " .
	"FROM {carbon_stamp} t " .
  "LEFT  JOIN {carbon_stamp_account} j ON t.nid = j.sid " .
  "LEFT  JOIN {node}                   n ON j.fid = n.nid " . // to get footprint title
	"WHERE t.nid = %d " .
	" ", $node->nid);
  $first_record = null ;
  global $aid_map ; $aid_map  = array() ;  // having to hold this array because it is being wiped.
  while ($record = db_fetch_object($query))
  {
  	if (empty($first_record))
    {
    	$first_record = $record ;
      $first_record->aid_map = array();
    }
    if (!empty($record->attached_aid))
      $first_record->aid_map[$record->attached_aid] = $record->attached_title ;
    $aid_map = $first_record->aid_map ;
  }
  if (empty($first_record))
    drupal_set_message(t("Failed to load carbon_stamp where nid=%nid", array("%nid"=>$node->nid)));

	return $first_record;
}


/**
 * Implementation of hook_insert, which saves carbon-specific information
 * into the carbon table
 * @param node object
 */

function carbon_stamp_insert(&$node)
{
  db_query("INSERT INTO {carbon_stamp} ".
	"(code, scope, reading, adjustment, protection, " .
	" exclude, startdate, enddate, nid) ".
	"VALUES ('%s', %d, '%s', '%s', %d, %d, %d, %d, %d)",
	$node->code, $node->scope, $node->reading, $node->adjustment,
	$node->protection, $node->exclude, $node->startdate, $node->enddate,
	$node->nid);
  _update_footprint_join($node);
}



/**
 * Implementation of hook_update, which saves updated todo-specific
 * information into the todo table
 * @param node object
 */
function carbon_stamp_update(&$node)
{

  db_query("UPDATE {carbon_stamp} set code='%s',scope=%d,reading='%s',adjustment='%s',protection=%d," .
    		" exclude=%d, startdate=%d, enddate=%d " .
    		" WHERE nid=%d",
	$node->code, $node->scope, $node->reading, $node->adjustment,
	$node->protection, $node->exclude, $node->startdate, $node->enddate,
 	$node->nid);
  _update_footprint_join($node);
}

/**
 * the use may choose to insert and delete the stamp from different
 * footprints. carbon_stamp_submit() works out which tables need
 * updating. This function inserts/removes the row.
 */


function _update_footprint_join($node)
{
  $node->aid_insert = array_diff(array_keys($node->aid_map_updated), array_keys($node->aid_map));
  $node->aid_delete = array_diff(array_keys($node->aid_map), array_keys($node->aid_map_updated));

  if (isset($node->aid_insert))
    foreach ($node->aid_insert as $aid)
  {
    db_query("INSERT INTO {carbon_stamp_account} (sid,fid) VALUES (%d, %d)", $node->nid, $aid);
  }

  if (isset($node->aid_delete))
    foreach ($node->aid_delete as $aid)
  {
    db_query("DELETE FROM {carbon_stamp_account} WHERE sid=%d AND fid=%d", $node->nid, $aid);
  }
}


/**
 * Implementation of hook_delete()
 */
function carbon_stamp_delete($node) {
	db_query("DELETE FROM {carbon_stamp_account} WHERE sid = %d", $node->nid);
	db_query("DELETE FROM {carbon_stamp}         WHERE nid = %d", $node->nid);
}


/**
 * Implementation of hook_view, add our node specific information
 * @param node object to display
 * @param boolean is this a teaser or full node?
 * @param boolean is this displaying on its own page
 *
 */
function carbon_stamp_view(&$node, $teaser = FALSE, $page = FALSE)
{
  $node->content["stamp_view"] = array("#weight"=>0, "#value" => theme('carbon_stamp_view', $node));
  return $node ;
}


function theme_carbon_stamp_view($node)
{
  $header = array(
    array("data" => t("a")),
    array("data" => t("b"))
  );
  global $aid_map ;
  $attached_account = '' ;
  foreach ($aid_map as $aid => $title)
  {
  	$attached_account .= l($title, "node/".$aid)." ";
  }

	// content variable that will be returned for display

	$rows[] = array("description", $node->title);
  $rows[] = array("model", $node->model);

	$rows[] = array("code", $node->code. "  ");
	$rows[] = array("amount",$node->reading );
	$rows[] = array("adjustment",$node->adjustment);
	$rows[] = array("scope", _get_scope_description($node->scope));
	$rows[] = array("start date", empty($node->startdate) ? "" : _format_date($node->startdate ));

	$rows[] = array("end date", _format_date($node->enddate ));
//$rows[] = array("exclude", $node->exclude);
	$rows[] = array("created", _format_date_time($node->created ));
	$rows[] = array("changed", _format_date_time($node->changed ));
	$rows[] = array("published", $node->status);
	$rows[] = array("accounts attached", $attached_account);

	$output = theme("table", null, $rows);

  return $output;
}

/**
 * Implementation of hook_view, add our node specific information
 * @param node object to display
 * @param boolean is this a teaser or full node?
 * @param boolean is this displaying on its own page
 *
 */
function carbon_stamp_edit(&$node, $teaser = FALSE, $page = FALSE)
{
//  if (!user_access('view carbon footprints')) {
//      return true;
//  }

  $node = node_prepare($node, $teaser);
  $carbon_account_info = theme('carbon_stamp_table_view', $node);
  $node->body .= $carbon_account_info;
  $node->teaser .= $carbon_account_info;
}



function _div_wrap($wrap_class, $text)
{
  return "<div class='".$wrap_class."'>".$text."</div>" ;
}


/**
 * walk through an array of carbon_stamps and find
 * out the earliest and latest date for the stamps
 *
 * @return an array containing 'first' and 'last' dates.
 */


function _stamp_date_range($ar)
{
  $dateRange = array();
  $dateRange['first'] = $dateRange['last'] = null ;
  foreach ($ar as $stamp)
  {
    if ($stamp->nid == 895)
    {
      $ab = "gg" ;
    }
    if (!empty($stamp->startdate))
    {
      $dateRange['first'] = empty($dateRange['first']) == false ?
        min($stamp->startdate, $dateRange['first']) : $stamp->startdate;
    }
    if (!empty($stamp->enddate))
    {
      $dateRange['first'] = empty($dateRange['first']) == false ?
        min($stamp->enddate, $dateRange['first']) : $stamp->enddate;
      $dateRange['last'] = empty($dateRange['last']) == false  ?
        max($stamp->enddate, $dateRange['last']) : $stamp->enddate;
    }
  }
  return $dateRange;
}




function _select_stamps_from_preferred_sources(&$stamps)
{
  $sourceWeight = new SourceWeight();
  $sourceWeight->initialize();
  $wstamps = array();

  foreach($stamps as $stamp)
  {
    $w = $sourceWeight->getWeight($stamp->sourcemodel);
    if ($w < 1)  // if weight 1 or more means not found
      $wstamps[$stamp->nid][$w] = $stamp;
    else
      $wstamps[$stamp->nid][] = $stamp ;
  }

  $selected_stamps = array();
  foreach($wstamps as $nid => $stamp_ar)
  {
    ksort($stamp_ar);  // move the lightest weighted item to be first in the list
    foreach($stamp_ar as $stamp)
    {
      $selected_stamps[$stamp->nid] = $stamp ; // take the first stamp only
      break ;
    }
  }
  return $selected_stamps ;
}

/**
 * this handles the weighting of different carbon_sources
 * The admin determines the carbon sources weighting via
 * settings. The first listed source is the heaviest.
 */


class SourceWeight
{
	var $weighted_filters = array();

  function initialize()
  {
    $max = 20 ;
    $filter_s = variable_get("carbon_model_filter", "", $max);
    if ($filter_s == "")
      return ;

    $filters = explode(",", $filter_s, 20);

    /**
     * create an array consiting of model_name => weight
     */
    for ($i = 0 ; $i < count($filters) ; $i++)
    {
      $weight = -count($filters) + $i ;
      $this->weighted_filters[trim($filters[$i])] = $weight ;
    }
  }

  /**
   * look up a sourcemodel and return a weighting for it
   */

  function getWeight($sourcemodel)
  {
//  	print_r($this->weighted_filters);
    $val = $this->weighted_filters[$sourcemodel] ;

    if (isset($this->weighted_filters[$sourcemodel]))
      return $this->weighted_filters[$sourcemodel] ;
    return 1 ;
  }
}




?>