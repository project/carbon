<?php
/**
 *
 * Drupal carbon module, diagnostic/tools routines
 *
 * Created on 30-Nov-2006
 *
 * @file
 * This file provides optional support, diagnostic and repair tools for the
 * carbon module. It is not required for normal operation but keep in mind
 * that a file of this name is imported by the carbon module.
 */


function carbon_admin_form()
{
  $form = array();

  $form["descrip"] = array (
        "#type" => "item",
        "#weight" => -3,
        "#value" => "If you are working with a production system backup your database" .
            " before executing any of the repair or purge operations available below." .
            " The underlying SQL statements have been tested with MySql 5.0.18 but " .
            " should work with other versions. The commands below act immediately, " .
            " there is no confirmation screen.") ;


  $form["check_tables"] = array("#type" => "fieldset",
    "#title" => t("Show number of records"),
    "#weight" => "0");

  $form["check_tables"]["descrip"] = array (
        "#type" => "item",
        "#value" => "Show the number of nodes and the number of rows for each table/carbon " .
            " node. In the case of carbon_stamp, carbon_account and carbon_source, there " .
            " should be an equal number of nodes and rows.");

  $form["check_tables"]["op"] = array("#type" => "submit", "#value" => t("check_tables"));


  $form["install_sources"] = array("#type" => "fieldset",
    "#title" => t("Install carbon sources"),
    "#weight" => "0");

  $form["install_sources"]["descrip"] = array (
        "#type" => "item",
        "#value" => "Install (or update existing) sources of carbon emissions with a" .
            " a default set of carbon emissions.");

  $form["install_sources"]["op"] = array("#type" => "submit", "#value" => t("install_carbon_sources"));





  $form["check_ownership"] = array("#type" => "fieldset",
    "#title" => t("Carbon stamp ownership"),
    "#weight" => "3");

  $form["check_ownership"]["descrip"] = array (
        "#type" => "item",
        "#value" => "Select that stamps attached to footprints but with a different owner. " .
            "This can happen if another user e.g. admin enters carbon stamps then attaches " .
            "them to a footprint not beloning to him or her or the author of the footprint " .
            "is innocently changed.") ;

  $form["check_ownership"]["op"] = array("#type" => "submit",
      "#value" => t("check_ownership"));

  $form["check_ownership"]["op2"] = array("#type" => "submit",
      "#value" => t("repair_ownership"));


  $form["purge_nodes"] = array("#type" => "fieldset",
    "#title" => t("Node table management"),
    "#weight" => "3");

  $form["purge_nodes"]["descrip"] = array (
        "#type" => "item",
        "#value" => "Purge nodes in the node table that don't have any corresponding row in a carbon table. " .
            "This is useful if you drop or truncate the carbon_source, carbon_stamp or " .
            "carbon_account tables.") ;

  $form["purge_nodes"]["op"] = array("#type" => "submit",
      "#value" => t("purge_nodes"));


  $form["purge_rows"] = array("#type" => "fieldset",
    "#title" => t("Carbon table management"),
    "#weight" => "3");

  $form["purge_rows"]["descrip"] = array (
        "#type" => "item",
        "#value" => "Purge rows in any of the carbon tables for which there is no corresponding entry " .
            "in the node table. Not implemented.");

  $form["purge_rows"]["op"] = array("#type" => "submit",
      "#value" => t("purge_rows"));

  $form["remove_module"] = array("#type" => "fieldset",
    "#title" => t("Carbon module total removal"),
    "#weight" => "4");

  $form["remove_module"]["descrip"] = array (
        "#type" => "item",
        "#value" => "Remove module from Drupal by removing the carbon ".
        " entry from the system table. When this module ".
        " is reenabled, drupal will attempt to run the install script.".
    " Don't do this unless you are sure or desperate.");

  $form["remove_module"]["op"] = array("#type" => "submit",
      "#value" => t("remove_module"));

  $form["drop_tables"] = array("#type" => "fieldset",
    "#title" => t("Drop tables"),
    "#weight" => "4");

  $form["drop_tables"]["descrip"] = array (
        "#type" => "item",
        "#value" => "Drop tables from database. All collected data is ".        "lost. You cannot reverse this so backup if you are not sure ".        "what you are doing");

  $form["drop_tables"]["op"] = array("#type" => "submit",
      "#value" => t("drop_carbon_tables"));



/*
  $form["show_orphans"] = array("#type" => "fieldset",
    "#title" => t("Orphaned carbon stamps"),
    "#weight" => "2",
  );
  $form["show_orphans"]["op"] = array("#type" => "submit", "#value" => t("show_orphans"));
*/
  return $form ;
}


function carbon_admin($a = NULL, $b = NULL, $c = NULL)
{
  $map = _map_of_footprints();
  $map_values = array_values($map);
  $map_keys = array_keys($map);

  $footprint_idx = isset($_POST["edit"]["footprint_idx"]) ? $_POST["edit"]["footprint_idx"] : 0 ;

  $output = "" ;
  if (isset($_POST["op"]))
  {
    $op = $_POST["op"];

    $fn = "_".$op ;
    if (function_exists($fn))
    {
      $footprint = $map_values[$footprint_idx];
      $fid = $map_keys[$footprint_idx];

      $result =  $fn($footprint, $fid);

      // $form seems to get cached, added field not diplayed!!

      $output .= $result ;
      $form[$op]["result0"] = array (
        "#type" => "item",
        "#weight" => "-5",
        "#value" => "oidjioew") ;
    }
    else
      $output = "function ". $op . " does not exist or is not installed" ;

  }
  drupal_set_message($output);
  $output .= drupal_get_form("carbon_admin_form", null);
  return $output ;
}


function _map_of_footprints()
{
  require_once(CARBON_PATH."/carbon_report.inc");
  $map = carbon_report_base(null, null, array());
  $map2 = array();
  foreach($map as $nid=>$footprint)
  {
    $map2[$nid] = $footprint->title ;
  }
  return $map2 ;
}


function _check_tables()
{
  $tables = _check_tables_present(array("carbon_source", "carbon_account",
  "carbon_stamp", "carbon_transfer", "carbon_stamp_account", "carbon_account_share"));

  $out = "" ;
  $num = 0 ;

  $header = array(
    array("data" => t("table name")),
    array("data" => t("rows in table")),
    array("data" => t("nodes of this type"))
    );

  $rows = array();
  foreach($tables as $tablename => $record)
  {

    if (!isset($record["rowcount"]))
      $rows[]  = array($tablename, "not present", "not present") ;
    else
    {
      $num++ ;
      $rows[] = array($tablename, $record["rowcount"], $record["nodecount"]);
    }
  }
  $out = theme("table", $header, $rows);

  return $out ;
}

function _db_table_exists($table) {
  return db_result(db_query("SHOW TABLES LIKE '{" . db_escape_table($table) . "}'"));
}

/**
 * This may only work with mysql
 *
 * @param an array of table names
 * @return an array of key pairs that contain the name and row count.
 */


function _check_tables_present($tablename)
{
    $sql = "show tables";
    $table2 = array();
    $num = 0 ;
    foreach ($tablename as $name)
    {
        if (_db_table_exists($name))
        {
            $db_query2 = db_query("select count(*) as count from ".$name);
            $result2 = db_fetch_object($db_query2);
            $table2[$name]["rowcount"] = $result2->count ;

            $db_query3 = db_query("select count(*) as count from node where type = '".$name."'");
            $result3 = db_fetch_object($db_query3);
            $table2[$name]["nodecount"] = $result3->count ;
        }
        else
            $table2[$name]["rowcount"] = $table2[$name]["nodecount"] = "" ;
    }
    return $table2 ;
}

function _install_carbon_sources()
{
  carbon_source_install_default();
}


function _check_ownership()
{
  $sql = "select fn.title, fu.name footprint_owner, su.name stamp_owner, sn.status as published, count(*) number " .
      "from carbon_stamp_account sf " .
      "join node fn on fn.nid = sf.fid " .
      "join node sn on sn.nid = sf.sid join users fu on fn.uid=fu.uid " .
      "join users su on sn.uid = su.uid " .
      "where fn.uid != sn.uid group by fn.title, fu.name, su.name, sn.status ; ";

  $db_query = db_query($sql);
  $rows = array();
  $num = 0 ;
  while  ($record = db_fetch_object($db_query))
  {
    $rows[] = array($record->title, $record->footprint_owner,
                    $record->stamp_owner,$record->published, $record->number);
    $num++ ;
  }
  $header = array(
    array("data" => t("title")),
    array("data" => t("footprint_owner")),
    array("data" => t("stamp_owner")),
    array("data" => t("published")),
    array("data" => t("number"))
   );
  return $num > 0 ? theme("table", $header, $rows) : "no stamps found with incorrect or invalid ownership" ;
}


function _repair_ownership()
{
  $sql = "update carbon_stamp_account sf " .
      "join node fn on fn.nid = sf.fid join node sn on sn.nid = sf.sid " .
      "join users fu on fn.uid=fu.uid join users su on sn.uid = su.uid " .
      "set sn.uid = fn.uid " .
      "where fn.uid != sn.uid ";
  $db_query = db_query($sql);
  return $db_query ? "done" : "failed" ;
}


/**
 * take care if you change this sql or you might delete all your data
 * needs mysql subqueries
 */


function _purge_nodes()
{
  $db_query1 = db_query(
  "delete from node where node.type = 'carbon_account' " .
  "and not exists (select nid from carbon_account where carbon_account.nid = node.nid); "
  );

  $db_query2 = db_query(
  "delete from node where node.type = 'carbon_stamp' " .
  "and not exists (select nid from carbon_stamp where carbon_stamp.nid = node.nid); "
  );
  $db_query3 = db_query(
  "delete from node where node.type = 'carbon_transfer' " .
  "and not exists (select nid from carbon_transfer where carbon_transfer.nid = node.nid); "
  );

  $db_query4 = db_query(
  "delete from node where node.type = 'carbon_source' " .
  "and not exists (select nid from carbon_source where carbon_source.nid = node.nid); "
  );

  return t("done") ;
}


function _purge_rows()
{
  return t("not implemented") ;
}


function _show_orphans()
{
  return t("not implemented") ;
}


function _remove_module()
{
  $db_query1 = db_query(
  "delete from system where name = 'carbon'"
  );
  return "module removed" ;
}


function _drop_carbon_tables()
{
  $res = _drop_tables();
  return "drop tables " . $res == 0 ? "failed" : "ok" ;
}


function _check_orphans()
{
	 return 42 ;
}



?>
